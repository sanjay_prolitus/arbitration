import React from "react"; 
import { BrowserRouter as Router, Route, Switch} from 'react-router-dom';
 
import Home from './components/Home/Home';
import Claim from './components/Claim/Claim';
import ClaimantInfoWrapper from './components/Claim/ClaimantInfo/ClaimantInfoWrapper';
import NewClaimantInfoWrapper from './components/Claim/ClaimantInfo/NewClaimantInfoWrapper';
import CmsPage from './components/CmsPage/CmsPage';
import ClaimantInfo from './components/Claim/ClaimantInfo/ClaimantInfo';
import ClaimInfo from './components/Claim/ClaimInfo/ClaimInfoWrapper';
import DocumentUpload from './components/Claim/DocumentUpload/DocumentUploadWrapper';
import RespondentInfo from './components/Claim/RespondentInfo/RespondentInfoWrapper';
import ClaimSummary from './components/Claim/Summary/SummaryWrapper';
import ClaimDetail from './components/Claim/ClaimDetails/ClaimDetails';
import InvoiceList from './components/Claim/ClaimDetails/Invoice/InvoiceList';
import InvoiceDetail from './components/Dashboard/Invoice/InvoiceDetails/InvoiceDetails';
import Dashboard from './components/Dashboard/Dashboard';
import InjectedCheckoutForm from './components/Payment/StripePayment';
import ClaimFileDetails from './components/Claim/ClaimFileDetails/ClaimFileDetails';
import NotFound from './components/NotFound/404';
import Timesheet from './components/Timesheet/Timesheet';
import Expense from './components/Expense/Expense';
import ExpenseDetail from './components/Expense/ExpenseDetail';
 
export default function Routes() {
  return (
      <Switch>
        <Route path="/dashboard" component={Dashboard} exact />
        <Route path="/cms-page/:slug" component={CmsPage} />
        <Route path="/" component={Home} exact />
        <Route path="/new-claimant-info/" component={NewClaimantInfoWrapper} /> 
        <Route path="/claimant-info/:id" component={ClaimantInfoWrapper} /> 
        <Route path="/claim-info" component={ClaimInfo} /> 
        <Route path="/document-upload" component={DocumentUpload} /> 
        <Route path="/respondent-info" component={RespondentInfo} /> 
        <Route path="/claim-summary" component={ClaimSummary} /> 
        <Route path="/claim-detail/:id" component={ClaimDetail} />
        <Route path="/invoice-detail/:id" component={InvoiceDetail} />
        <Route path="/invoice-claim-list/:id" component={InvoiceList} />
        <Route path="/payment" component={InjectedCheckoutForm} /> 
        <Route path="/claim-file-detail/:id" component={ClaimFileDetails} />
        <Route path="/timesheet/:id" component={Timesheet} />
        <Route path="/expenses/:id" component={Expense} />
        <Route path="/expense-detail/:claim_id/:payment_id" component={ExpenseDetail} />
        <Route component={NotFound} />
      </Switch>
  )
}