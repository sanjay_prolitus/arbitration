import React from "react";
import './index.scss';
import './common.scss';
import Routes from './Routes';
import HeaderTop from './components/HeaderTop/HeaderTop';
import HeaderBottom from './components/HeaderBottom/HeaderBottom';
import Header from './components/Header/Header';
import Footer from './components/Footer/Footer';
 
function App() {
    return (
      <>
        <HeaderTop/>
        <Header /> 
        <HeaderBottom/>
        <Routes />
        <Footer/>
      </>
    );
}
 
export default App;