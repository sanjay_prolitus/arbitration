import React, { Component } from 'react'
import Layout from '../../hoc/layout/layout';
import axios from 'axios';
class about extends Component {

	render() {
		return (
			<Layout>
				<section className="about">
				<div className="container">
				<div className="about-text">
				<h3>About Us</h3>
				<h4>TAG LINE</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>

				<p> Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div>
				<div className="brand-value">
				<h2>BRAND VALUES</h2>
				<div className="container">
				<div className="row">
				<div className="col">
				<figure>
				<img src={offerimg} alt="About Images" />
				</figure>
				<h4>Lorem Ipsum</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div>
				<div className="col">
				<figure>
				<img src={offerimg} alt="About Images" />
				</figure>
				<h4>Lorem Ipsum</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div>
				<div className="col">
				<figure>
				<img src={offerimg} alt="About Images" />
				</figure>
				<h4>Lorem Ipsum</h4>
				<p>Lorem Ipsum has been the industry’s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.</p>
				</div>
				<div className="col">
				<figure>
				<img src={offerimg} alt="About Images" />
				</figure>
				<h4>Lorem Ipsum</h4>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua.</p>
				</div>
				</div>
				</div>
				</div>
				<div className="journey">
				<div className="row">
				<div className="col">
				<figure>
				<img src={aboutimg} alt="About Images" />
				</figure>
				</div>
				<div className="col">
				<h3>OUR JOURNEY</h3>
				<p>Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.</p>
				</div>
				</div>
				</div>
				</div>
				</section>
			</Layout>
		)
	}
}


export default about;
