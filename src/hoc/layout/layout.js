import React from 'react';
import Header from '../../component/header/header'
import Footer from '../../component/footer/footer'

const Layout = (props) => {
	return (
		<>
			<Header />
			{props.children}
			<Footer />
		</>
	)
}

export default Layout;
