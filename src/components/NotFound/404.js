import React from 'react';
import { Link } from "react-router-dom";

const Pagenotfound = () =>{
    return(
        <div className="container">
               <div className="row">
                   <div className="col-12">
                      <section className='home-section home--module--one'>
                        <div className="container">
                            <div className="row">
                                <div className="col-12">
                                <div className="file-details" style={{ textAlign:"center" }}>
                                    <h4>404</h4>
                                    <br/>
                                    <h4>Page Not Found!</h4>
                                    <br/>
                                    <p>Oops! Looks Like Something Going Wrong</p>
                                    <br/>
                                    <div className="button-group">
                                      <Link to="/" className="green-btn">Back to homepage</Link>
                                    </div>
                                </div>
                                </div>
                            </div>
                        </div>
                    </section>
                </div>
           </div>
       </div>
    )
}
export default Pagenotfound;