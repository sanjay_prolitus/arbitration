import React, { Component } from 'react';

class NoDataTable extends Component {
    constructor(props) {
        super(props);

    }
    
    componentDidMount = () => {
        
        //console.log("Props Data: ", this.props.title)
    }

    render() {
        
        let columns = this.props.columns ? this.props.columns : [];
        let columnCount = columns.length;

        return(
            <div>
            <table className="noData table table-bordered">
                <thead>
                    <tr>
                        {columns && columns.map((header) =>
                            <th>{header.name}</th>
                        )}
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td colSpan={columnCount}>{this.props.title}</td>
                    </tr>
                </tbody>
            </table>
            </div>
        )
    }
}

export default NoDataTable;