import React, {Component} from 'react';
import { Link } from "react-router-dom";
import Loader from 'react-loader'
import { Redirect } from 'react-router';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import './timesheet.scss';
import NoDataTable from '../Common/NoDataTable';
import Breadcrumb from 'react-bootstrap/Breadcrumb'

let api_url = process.env.REACT_APP_API_URL1;

class Timesheet extends Component{
    constructor(props) {
        super(props);
        this.state = {
            claim_id : '',
            timesheet_list : '',
            loaded: false
        };
    }

    componentDidMount(){
        
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        let claim_id        = this.props.match.params.id;

        this.setState({
            claim_id: claim_id
        });

        if(token !== '' && user_id !== ''){
            this.getTimesheet(user_id, token, claim_id)
        }
    }

    // get Timesheet list
    getTimesheet = (user_id, token, claim_id) => {
        if(user_id){
            let timsheet_request    = { 
                'user_id': parseInt(user_id), 
                'access_token': token, 
                'claim_id': parseInt(claim_id), 
            }

            axios.post(api_url+'/api/v1/claim/timesheet', timsheet_request, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then(res => {
                if(res.data.success === true){

                    this.setState({
                        timesheet_list: res.data,
                        loaded: true
                    })
                }
            })
        }
    }

    render() {

        if (!localStorage.getItem('token')) {
            return (
                <Redirect to="/" />
            )
        }

        let timesheet_list_data = this.state.timesheet_list.timesheet;

        let columns = [
            {
                name: 'Name',
                selector: row => row.name,
                sortable: true
            },
            {
                name: 'Date',
                selector: row => row.date,
                sortable: true
            },
            {
                name: 'Status',
                selector: row => row.status,
                sortable: true
            },
            {
                name: 'Hours',
                selector: row => row.hours,
                sortable: true
            }         
        ];

        let list_data = [];
        if(timesheet_list_data !== undefined){
            let timesheet_list = Object.keys(timesheet_list_data); 
            timesheet_list.forEach(function(key, i) {
                list_data.push({ 
                    name: timesheet_list_data[key].name, 
                    date: timesheet_list_data[key].date,
                    status: timesheet_list_data[key].status,
                    hours: timesheet_list_data[key].hours
                })

            });
        }

        return(
            <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} radius={12} color="#201e53">
                
                <Breadcrumb>
                    <div className="container">
                        <div className="row">
                            <Breadcrumb.Item href="/dashboard">My Claims</Breadcrumb.Item>
                            <Breadcrumb.Item href="#">Timesheet</Breadcrumb.Item>
                        </div>
                    </div>
                </Breadcrumb>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                           <div className="claim-list claim-list-table ">
                                <Link to={`/claim-detail/${ this.state.claim_id }`}><h5>Claim#: {this.state.timesheet_list.claim_name}</h5></Link>
                                   
                                {list_data.length > 0 &&
                                    <DataTable
                                    columns={columns}
                                    data={list_data}
                                    fixedHeader
                                    />
                                }

                                { list_data.length == 0 &&
                                    <div className="table-responsive">
                                        <NoDataTable columns={columns} title="No Data Available" />
                                    </div>
                                }  
                                
                                
                           </div>
                       </div>
                   </div>
               </div>
           </Loader>
        )
    }
}
export default Timesheet;