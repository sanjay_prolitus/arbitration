import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import axios from 'axios';
import Alert from '../Alert/Alert';
import Mail from './../../assets/images/mail.png';
import Lock from './../../assets/images/lock.png';
import Google from './../../assets/images/google.png';
import './register.scss';

const Register = (props) =>{  


    const [succmessage, setSuccmessage] = useState('');
    const [errormessage, setErrormessage] = useState('');
    const [openAlert,setOpenAlert] = useState(false);

    let first_name = React.createRef();
    let last_name = React.createRef();
    let email = React.createRef();
    let password = React.createRef();
    let conf_password = React.createRef();


    function submitForm(e) {
        e.preventDefault();

        if(password.current.value == conf_password.current.value){

            let registrationDetail = { 'email': email.current.value, 'first_name': first_name.current.value, 'last_name': last_name.current.value, 'password': password.current.value }
            axios.post('/api/v1/user/registration', registrationDetail, {
                headers: { 
                    'Content-Type': 'text/plain'
                }
            })
            .then( response => {
                let result = response.data;
                console.log(result);
                if(result.success === true){
                    setSuccmessage(result.message);

                }else{
                    setErrormessage(result.message);
                }
                
            })
            .catch(err => {
                console.log(err);
            })
        }else{
            console.log("Error Message");
            setErrormessage("Password and Conf Password should be same.");
            console.log(errormessage);
        }
    }


    return(  
    <>      
        <Modal className="register--modal" show={props.openRegister} onHide={props.closeRegister}>
            <Modal.Body>
                <Button className="close" onClick={props.closeRegister}>&times;</Button>
                <h3 className="text-center">Create New Account</h3>
                <InputGroup className="mb-4">
                    <InputGroup.Text id="fname"><img src={Mail} alt='name'/></InputGroup.Text>
                    <FormControl
                        placeholder="First Name"
                        aria-label="Enter First Name"
                        aria-describedby="fname"
                        ref={first_name}
                    />
                </InputGroup>
                <InputGroup className="mb-4">
                    <InputGroup.Text id="lname"><img src={Lock} alt='name'/></InputGroup.Text>
                    <FormControl
                        placeholder="Last Name"
                        aria-label="Enter Last Name"
                        aria-describedby="lname"
                        ref={last_name}
                    />
                </InputGroup>
                <InputGroup className="mb-4">
                    <InputGroup.Text id="email"><img src={Mail} alt='email'/></InputGroup.Text>
                    <FormControl
                        placeholder="E-mail"
                        aria-label="Enter email"
                        aria-describedby="email"
                        ref={email}
                    />
                </InputGroup>
                <InputGroup className="mb-4">
                    <InputGroup.Text id="password"><img src={Lock} alt='password'/></InputGroup.Text>
                    <FormControl
                        placeholder="Password"
                        aria-label="Enter password"
                        aria-describedby="password"
                        ref={password}
                    />
                </InputGroup>
                <InputGroup className="mb-4">
                    <InputGroup.Text id="cPassword"><img src={Lock} alt='password'/></InputGroup.Text>
                    <FormControl
                        placeholder="Confirm Password"
                        aria-label="Enter Confirm password"
                        aria-describedby="cPassword"
                        ref={conf_password}
                    />
                </InputGroup>
                <label className='checkbox'>I Accept the Term Of Use And Privacy Policy
                    <input type="checkbox" required/>
                    <span className="checkmark"></span>
                </label>
                <button onClick={submitForm} type="submit" className="btn-signin">Sign Up</button>
                <div className="text-center">
                    <div className="or">OR</div>
                    <h4 className="text-center text-uppercase">Sign up with</h4>
                    <button type="button" className="google-btn google-btn__2 mt-4"><span><img src={Google} alt='password'/></span>Sign in with Google</button>
                </div>
            </Modal.Body>
        </Modal>
        <Alert registerSucc={succmessage} registerSucc={succmessage} />
        </>
    )
}

export default Register;