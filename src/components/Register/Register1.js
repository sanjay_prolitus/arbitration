import React,{useState, Component} from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import axios from 'axios';
import { Redirect } from 'react-router';
import RegisterAlert from '../Alert/RegisterAlert';
import Mail from './../../assets/images/mail.png';
import User from './../../assets/images/user.png';
import Lock from './../../assets/images/lock.png';
import Google from './../../assets/images/google.png';
import OTP from './../../assets/images/otp.png';
import './register.scss';
import Mobile from './../../assets/images/mobile.png';
import Recaptcha from 'react-recaptcha';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { facebookProvider, googleProvider } from "../../config/authMethods";
import socialMediaAuth from "../../services/auth";

let api_url = process.env.REACT_APP_API_URL1;
let recaptcha_site_key = process.env.REACT_APP_RECAPTCHA_SITE_KEY;

class Register extends Component {

    constructor(props) {
        super(props);
        this.state = {
            openRegister: this.props.openRegister,
            succmessage: false,
            errormessage: false,
            first_name: '',
            last_name: '',
            email: '',
            password: '',
            conf_password: '',
            userLoggedIn: false,
            password_match_err: false,
            first_name_err: false,
            last_name_err: false,
            email_err: false,
            password_err: false,
            conf_password_err: false,
            formOtpErr: false,
            loginemailMsg: false,
            loginemailMsgErr: false,
            openOtpVerify: false,
            closeOtpVerify: false,
            regmobile: '',
            regemail: '',
            regname: '',
            regmobileErr: false,
            regemailErr: false,
            regnameErr: false,
            registerOtpVerify: false,
            reg_message: false,
            user_id: '',
            user_name: '',
            user_email: '',
            user_password: '',
            user_otp: '',
            checkbox_yes: '',
            checkbox_error: false,
            loading: false,
            format_error: false,
            closeModel: this.props.closeRegister, 
            isVerified: false,
            recaptch_err: false
        };
        this.otpRegister          = this.otpRegister.bind(this);
        this.closeOtpRegister     = this.closeOtpRegister.bind(this);
        this.recaptchaLoaded = this.recaptchaLoaded.bind(this);
        this.verifyCallback = this.verifyCallback.bind(this);

        this.nameInput = React.createRef();

    }

    componentDidMount() {
        this.nameInput.current.focus();
    }

    inputHandler = (event) =>{
        this.setState ({ [event.target.name] :event.target.value  })
    }

    checkedHandler = (event) =>{
        this.setState ({ [event.target.name] :event.target.checked  })
    }

    submitForm = (e) => {
        e.preventDefault();

        let first_name      = this.state.first_name ? this.state.first_name : ''; 
        first_name          = first_name.trim(); 
        let last_name       = this.state.last_name ? this.state.last_name : '';
        last_name           = last_name.trim();
        let email           = this.state.email ? this.state.email : '';
        let password        = this.state.password ? this.state.password : '';
        let conf_password   = this.state.conf_password ? this.state.conf_password : '';
        let checkbox_yes    = this.state.checkbox_yes ? this.state.checkbox_yes : '';
        if(first_name && email && password && conf_password && checkbox_yes ){
            if(password == conf_password){
                var regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/
                if(password.match(regex)){
                    if (this.state.isVerified) {
                        this.setState({
                            loading: true,
                            openRegister: false,
                            registerOtpVerify: true
                        });
                        let registrationDetail = { 
                            'email': email, 
                            'first_name': first_name, 
                            'confirm_password': password
                        }
                        axios.post(api_url+'/api/v1/user/registration', registrationDetail, {
                            headers: { 
                                'Content-Type': 'text/plain'
                            }
                        })
                        .then( response => {
                            let result = response.data;

                            if(result.success === true){

                                this.setState({ 
                                    reg_message: result.message, 
                                    user_id: result.id, 
                                    user_name: result.name, 
                                    user_email: result.email,
                                    user_password: result.password,
                                    loading: false
                                }, () => {
                                    this.sendOtp(email);
                                })

                            }else{

                                this.setState({ errormessage: result.message, loading: false }, () => {
                                    setTimeout(() => this.setState({ errormessage: false }), 5000);
                                })
                            }
                            
                        })
                        .catch(err => {
                            console.log(err);
                        })
                    }else{
                        this.setState({ recaptch_err: "Please verify that you are a human!" }, () => {
                            setTimeout(() => this.setState({ recaptch_err: false }), 5000);
                        })
                    }
                } else {
                    this.setState({ format_error: "Password should be minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character" }, () => {
                        setTimeout(() => this.setState({ format_error: false }), 5000);
                    })
                }
            }else{
                this.setState({ password_match_err: "Password and Conf Password should be same." }, () => {
                    setTimeout(() => this.setState({ password_match_err: false }), 5000);
                })
            }    
        }else{
            if(first_name === ''){
                this.setState({ first_name_err: "This field is required." }, () => {
                    setTimeout(() => this.setState({ first_name_err: false }), 5000);
                })
            }
            if(email === ''){
                this.setState({ email_err: "This field is required." }, () => {
                    setTimeout(() => this.setState({ email_err: false }), 5000);
                })
            }
            if(password === ''){
                this.setState({ password_err: "This field is required." }, () => {
                    setTimeout(() => this.setState({ password_err: false }), 5000);
                })
            }
            if(conf_password === ''){
                this.setState({ conf_password_err: "This field is required." }, () => {
                    setTimeout(() => this.setState({ conf_password_err: false }), 5000);
                })
            }
            if(checkbox_yes === ''){
                this.setState({ checkbox_error: "This field is required." }, () => {
                    setTimeout(() => this.setState({ checkbox_error: false }), 5000);
                })
            }
        }
    }

    verifyRegisteredUserOtp = (e) => {
        e.preventDefault();

        let user_email      = this.state.user_email ? this.state.user_email : '';
        let password        = this.state.user_password ? this.state.user_password : '';
        let conf_password   = this.state.conf_password ? this.state.conf_password : '';
        let user_otp        = this.state.user_otp ? this.state.user_otp : '';

        if(user_email ){
            let request_data = { 
                'email': user_email,
                'confirm_password': password,
                'otp': user_otp
            }
            axios.post(api_url+'/api/v1/set_password', request_data, {
                headers: { 
                    'Content-Type': 'text/plain'
                }
            })
            .then( response => {
                
                let result = response.data;

                if(result.success === true){

                    localStorage.setItem('token', result.access_token);
                    localStorage.setItem('user_id', result.user_id);
                    localStorage.setItem('user_name', result.user_name);
                    localStorage.setItem('user_email', result.user_email);

                    this.setState({ 
                        succmessage: result.successfull_message, 
                        user_id: result.user_id, 
                        user_name: result.user_name, 
                        user_email: result.user_email, 
                        openRegister: false, 
                        registerOtpVerify: false, 
                        openOtpRegister: false 
                    })
                    this.setState({ succmessage: result.successfull_message }, () => {
                        setTimeout(() => this.setState({ succmessage: false }), 5000);
                    })
                    toast.success(result.successfull_message, {
                        position: "top-right",
                        autoClose: 3000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    //window.location.href = '/dashboard';
                    window.location.reload(true);
                }else{

                    this.setState({ errormessage: result.message }, () => {
                        setTimeout(() => this.setState({ errormessage: false }), 5000);
                    })
                }
                
            })
            .catch(err => {
                console.log(err);
            }) 
        }else{
            this.setState({ email_err: "This field is required." }, () => {
                setTimeout(() => this.setState({ email_err: false }), 5000);
            })
            
        }
    }

    sendOtp = (email) =>{
        if( email ){
            let emailDetail = { 'email': email };

            axios.post(api_url+'/api/v1/generate/otp', emailDetail, {
                headers: { 
                    'Content-Type': 'text/plain'
                }
            })
            .then( response => {
                debugger;
                let result = response.data;
                
                if(result.success === true){
                    this.setState({
                        reg_message: result.message,
                        registerOtpVerify: true
                    }, () => {
                        setTimeout(() => this.setState({ reg_message: false }), 5000);
                    })
                }else{
                    this.setState({ errormessage: result.message }, () => {
                        setTimeout(() => this.setState({ errormessage: false }), 5000);
                    })
                }
                
            })
            .catch(err => {
                console.log(err);
            })
        }
    }

    socialLogin = async (provider) => {
        const res       = await socialMediaAuth(provider);

        if(res.credential){
            let id_token    = res.credential.idToken;
            let accessToken = res.credential.accessToken;
            let provider    = res.credential.providerId;
            let displayName;
            let email;
            let phone;
            if(res.user){
                displayName = res.user.displayName;
                email       = res.user.email;
                phone       = res.user.phoneNumber;
            }else{
                displayName = '';
                email       = res.email;
                phone       = '';
            }
            
            let socialData = {
                "id_token": id_token,
                "accessToken": accessToken,
                "provider": provider,
                "name": displayName,
                "mobile": phone,
                "email": email,
            }

            axios.post(api_url+'/api/v1/social/media/login', socialData, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then( res => {
                let result = res.data

                if(result.status !== 0){
                    var access_token    = result.access_token;
                    var user_id         = result.user_id;
                    var user_name       = result.name;
                    var user_email      = result.email;

                    localStorage.setItem('token', access_token);
                    localStorage.setItem('user_id', user_id);
                    localStorage.setItem('user_name', user_name);
                    localStorage.setItem('user_email', user_email);

                    this.setState({ succmessage: result.message }, () => {
                        setTimeout(() => this.setState({ succmessage: false }), 5000);
                    })
                    toast.success(result.message, {
                        position: "top-right",
                        autoClose: 3000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    // this.getProfileData(user_id, access_token)
                    window.location.reload(true);
                    
                }else{
                    this.setState({ errormessage: result.message }, () => {
                        setTimeout(() => this.setState({ errormessage: false }), 3000);
                    })
                }
                
            })
            .catch(err => {
                console.log(err);
            })
        }
    }

    otpRegister = (e) => {
        e.preventDefault();
        this.setState({
            openOtpRegister: true,
            openRegister: false
        })
    }

    closeOtpRegister = (e) => {
        e.preventDefault();
        this.setState({
            openOtpRegister: false
        })
    }
    closeOtpVerify = (e) => {
        e.preventDefault();
        this.setState({
            openOtpVerify: false
        })
    }

    registerOtpCreation = (event) => {
        event.preventDefault();
        let registerDetail = { 
            'mobile': this.state.regmobile ? this.state.regmobile : '', 
            'email': this.state.regemail ? this.state.regemail : '', 
            'name': this.state.regname ? this.state.regname : '' 
        }
        if(this.state.regemail && this.state.regname){
            axios.post(api_url+'/api/v1/registration/otp/creation', registerDetail, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then( res => {
                let result = res.data
                if(result.success == false){
                    this.setState({
                        loginemailMsgErr: result.message
                    })
                }else{

                    this.setState({
                        loginemailMsg: result.email.message,
                        login_email: result.email.email,
                        otp_time: result.email.otp_time,
                        openOtpVerify: true
                    }, () => {
                        setTimeout(() => this.setState({ loginemailMsg: false }), 5000);
                    })
                }
                
            })
            .catch(err => {
                console.log(err);
            })
        }else{
            if(this.state.regemail === ''){
                this.setState({ regemailErr: "This field is required." }, () => {
                    setTimeout(() => this.setState({ regemailErr: false }), 5000);
                })
            }
            if(this.state.regname === ''){
                this.setState({ regnameErr: "This field is required." }, () => {
                    setTimeout(() => this.setState({ regnameErr: false }), 5000);
                })
            }
        }
    }

    resendOtp = (login_email) => {

        let loginDetail = { 'email': login_email }
        axios.post(api_url+'/api/v1/login/otp/creation', loginDetail, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data
            if(result.success == false){
                this.setState({
                    loginemailMsgErr: result.message
                })
            }else{
                this.setState({
                    loginemailMsg: result.email.message,
                    login_email: result.email.email,
                    otp_time: result.email.otp_time,
                    openOtpVerify: true
                })
            }
            
        })
        .catch(err => {
            console.log(err);
        })
    }

    verifyRegisterOTP = (event) => {
        event.preventDefault();
        let registerDetail = { "email": this.state.login_email, "otp": this.state.login_otp }
        if(this.state.login_email && this.state.login_otp){
            axios.post(api_url+'/api/v1/registration/otp/submit', registerDetail, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then( res => {
                let result = res.data
                
                if(result.success === true){
                    var access_token    = result.access_token;
                    var user_id         = result.user_id;
                    var user_name       = result.user_name;
                    var user_email      = result.user_email;

                    localStorage.setItem('token', access_token);
                    localStorage.setItem('user_id', user_id);
                    localStorage.setItem('user_name', user_name);
                    localStorage.setItem('user_email', user_email);

                    this.setState({ 
                        userLoggedIn: true,
                        succmessage: result.message, 
                        openOtpVerify: false,
                        openOtpRegister: false 
                    }, () => {
                        setTimeout(() => this.setState({ succmessage: false }), 5000);
                    })
                    toast.success(result.message, {
                        position: "top-right",
                        autoClose: 3000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                    });
                    window.location.reload(true);
                }else{
                    this.setState({ loginVarifyOtpErr: result.message }, () => {
                        setTimeout(() => this.setState({ loginVarifyOtpErr: false }), 5000);
                    })
                }
                
            })
            .catch(err => {
                console.log(err)
            })
        } else {
            if(this.state.login_email === ''){
                this.setState({ formEmailErr2: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formEmailErr2: false }), 5000);
                })
            }
            if(this.state.login_otp === ''){
                this.setState({ formOtpErr: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formOtpErr: false }), 5000);
                })    
            }
        }
    }

    verifyCallback(response) {
        if (response) {
            this.setState({
                isVerified: true
            })
        }
    }

    recaptchaLoaded() {
        console.log('capcha successfully loaded');
    }

    render() {

        if ( localStorage.getItem('user_id') ) {
            return (
                <Redirect to="/dashboard" />
            )
        }

        let openOtpRegister     = this.state.openOtpRegister;
        let closeOtpRegister    = this.state.closeOtpRegister;
        let openRegister        = this.state.openRegister;
        let closeModel          = this.state.closeModel;
        let openOtpVerify       = this.props.openOtpVerify;
        /*let showModel;
        let closeModel;
        if(openOtpRegister){
            showModel       = false;
        }else{
            showModel   = openRegister;
            closeModel  = closeRegister;
        }*/

        return(  
            <>      
                <Modal className="register--modal" show={openRegister}>
                    <Modal.Body>
                        <form onSubmit={this.submitForm}>
                            <Button className="close" onClick={closeModel}>&times;</Button>
                            <h3 className="text-center">Sign Up</h3>
                            {this.state.succmessage &&
                                <div className="success-message" style={{ color:"green"}}>{this.state.succmessage}</div>
                            }
                            {this.state.errormessage &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.errormessage}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="fname"><img src={User} alt='name'/></InputGroup.Text>
                                <FormControl
                                    placeholder="Name"
                                    aria-label="Enter Name"
                                    aria-describedby="fname"
                                    value={this.state.first_name}
                                    name="first_name"
                                    ref={this.nameInput}
                                    maxLength="30"
                                    type="text"
                                    onChange={this.inputHandler}
                                    required="required"
                                />
                            </InputGroup>
                            {this.state.first_name_err &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.first_name_err}</div>
                            }
                            {/*<InputGroup className="mb-4">
                                <InputGroup.Text id="lname"><img src={User} alt='name'/></InputGroup.Text>
                                <FormControl
                                    placeholder="Last Name"
                                    aria-label="Enter Last Name"
                                    aria-describedby="lname"
                                    name="last_name"
                                    value={this.state.last_name}
                                    maxLength="30"
                                    onChange={this.inputHandler}
                                />
                            </InputGroup>*/}
                            {this.state.last_name_err &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.last_name_err}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="email"><img src={Mail} alt='email'/></InputGroup.Text>
                                <FormControl
                                    placeholder="E-mail"
                                    aria-label="Enter email"
                                    aria-describedby="email"
                                    name="email"
                                    type="email"
                                    value={this.state.email}
                                    onChange={this.inputHandler}
                                    required="required"
                                />
                            </InputGroup>
                            {this.state.email_err &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.email_err}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="password"><img src={Lock} alt='password'/></InputGroup.Text>
                                <FormControl
                                    placeholder="Password"
                                    aria-label="Enter password"
                                    aria-describedby="password"
                                    type="password"
                                    name="password"
                                    value={this.state.password}
                                    onChange={this.inputHandler}
                                    required="required"
                                />
                            </InputGroup>
                            {this.state.password_err &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.password_err}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="cPassword"><img src={Lock} alt='password'/></InputGroup.Text>
                                <FormControl
                                    placeholder="Confirm Password"
                                    aria-label="Enter Confirm password"
                                    aria-describedby="cPassword"
                                    type="password"
                                    name="conf_password"
                                    value={this.state.conf_password}
                                    onChange={this.inputHandler}
                                    required="required"
                                />
                            </InputGroup>
                            {this.state.conf_password_err &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.conf_password_err}</div>
                            }
                            {this.state.password_match_err &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.password_match_err}</div>
                            }
                            {this.state.format_error &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.format_error}</div>
                            }
                            <label className='checkbox'>I Accept the Term Of Use And Privacy Policy
                                <input type="checkbox" name="checkbox_yes" value={this.state.checkbox_yes} onChange={this.checkedHandler} required="required" />
                                <span className="checkmark"></span>
                            </label>
                            {this.state.checkbox_error &&
                                <div className="form-validate-error checkbox" style={{ color:"red"}}>{this.state.checkbox_error}</div>
                            }
                            <Recaptcha
                                sitekey={recaptcha_site_key}
                                render="explicit"
                                onloadCallback={this.recaptchaLoaded}
                                verifyCallback={this.verifyCallback}
                            />
                            {this.state.recaptch_err &&
                                <div className="form-validate-error checkbox" style={{ color:"red"}}>{this.state.recaptch_err}</div>
                            }
                            <button type="submit" className="btn-signin">{this.state.loading ? "Loading..." : "Get OTP"} </button>

                            {/*<div className="text-center">
                                <button type="button" className="google-btn" onClick={this.otpRegister}><span><img src={Mobile} alt='OTP' /></span>Sign Up Using OTP</button>
                            </div>*/}

                            <div className="text-center">
                                <div className="or">OR</div>
                                <button type="button" className="google-btn google-btn__2 mt-0" onClick={() => this.socialLogin(googleProvider)}><span><img src={Google} alt='password'/></span>Sign up with Google</button>
                            </div>
                        </form>
                    </Modal.Body>
                </Modal>

                <Modal className="register--modal" show={this.state.registerOtpVerify}>
                    <Modal.Body>
                        <form onSubmit={this.verifyRegisteredUserOtp}>
                            <Button className="close" onClick={closeModel}>&times;</Button>
                            <h3 className="text-center">Verify Account</h3>
                            {this.state.reg_message &&
                                <div className="success-message" style={{ color:"green"}}>{this.state.reg_message}</div>
                            }
                            {this.state.errormessage &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.errormessage}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="email"><img src={Mail} alt='email'/></InputGroup.Text>
                                <FormControl
                                    placeholder="E-mail"
                                    aria-label="Enter email"
                                    aria-describedby="email"
                                    readonly="readonly"
                                    name="user_email"
                                    value={this.state.user_email}
                                    onChange={this.inputHandler}
                                    required="required"
                                />
                            </InputGroup>
                            {this.state.email_err &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.email_err}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="OTP"><img src={OTP} alt='OTP'/></InputGroup.Text>
                                <FormControl
                                    placeholder="Enter OTP"
                                    aria-label="Enter OTP"
                                    aria-describedby="otp"
                                    name="user_otp"
                                    value={this.state.user_otp}
                                    onChange={this.inputHandler}
                                    required="required"
                                />
                            </InputGroup>
                            {this.state.formOtpErr &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.formOtpErr}</div>
                            }

                            <button type="submit" className="btn-signin">Verify OTP & Submit</button>
                        </form>
                    </Modal.Body>
                </Modal>

                <Modal className="register--modal" show={this.state.openOtpRegister}>
                    <Modal.Body>
                        <form onSubmit={this.registerOtpCreation}>
                            <Button className="close" onClick={closeModel}>&times;</Button>
                            <h3 className="text-center">Sign Up</h3>
                            {this.state.loginemailMsgErr &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.loginemailMsgErr}</div>
                            }
                            {/*<InputGroup className="mb-4">
                                <InputGroup.Text id="regmobile"><img src={Mail} alt='regmobile'/></InputGroup.Text>
                                <FormControl
                                    placeholder="Mobile"
                                    aria-label="Enter Mobile"
                                    aria-describedby="mobile"
                                    name="regmobile"
                                    value={this.state.regmobile}
                                    onChange={this.inputHandler}
                                />
                            </InputGroup>
                            {this.state.regmobileErr &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.regmobileErr}</div>
                            }*/}
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="regname"><img src={User} alt='regname'/></InputGroup.Text>
                                <FormControl
                                    placeholder="Name"
                                    aria-label="Enter name"
                                    aria-describedby="name"
                                    name="regname"
                                    maxLength="30"
                                    value={this.state.regname}
                                    onChange={this.inputHandler}
                                    required="required"
                                />
                            </InputGroup>
                            {this.state.regnameErr &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.regnameErr}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="regemail"><img src={Mail} alt='regemail'/></InputGroup.Text>
                                <FormControl
                                    placeholder="E-mail"
                                    aria-label="Enter email"
                                    aria-describedby="email"
                                    name="regemail"
                                    value={this.state.regemail}
                                    onChange={this.inputHandler}
                                    required="required"
                                />
                            </InputGroup>
                            {this.state.regemailErr &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.regemailErr}</div>
                            }

                            <div className="text-center otp-button-group">
                                <button type="submit" className="btn-signin">Get OTP</button>
                            </div>

                            <div className="text-center">
                                <div className="or">OR</div>                            
                                <button type="button" className="google-btn google-btn__2 mt-0" onClick={() => this.socialLogin(googleProvider)}><span><img src={Google} alt='password'/></span>Sign up with Google</button>
                            </div>
                        </form>
                    </Modal.Body>
                </Modal>

                <Modal className="register--modal" show={this.state.openOtpVerify}>
                    <Modal.Body>
                        <form onSubmit={this.verifyRegisterOTP}>
                            <Button className="close" onClick={closeModel}>&times;</Button>
                            <h3 className="text-center">Sign Up</h3>                        
                            {this.state.loginemailMsg &&
                                <div className="success-message" style={{ color:"green"}}>{this.state.loginemailMsg}</div>
                            }
                            {this.state.loginVarifyOtpErr &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.loginVarifyOtpErr}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="email"><img src={Mail} alt='email'/></InputGroup.Text>
                                <FormControl
                                    placeholder="E-mail"
                                    aria-label="Enter email"
                                    aria-describedby="email"
                                    name="login_email"
                                    value={this.state.login_email}
                                    onChange={this.inputHandler}
                                    readonly
                                    required="required"
                                />
                            </InputGroup>
                            {this.state.formEmailErr2 &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.formEmailErr2}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="OTP"><img src={OTP} alt='OTP'/></InputGroup.Text>
                                <FormControl
                                    placeholder="Enter OTP"
                                    aria-label="Enter OTP"
                                    aria-describedby="otp"
                                    name="login_otp"
                                    value={this.state.login_otp}
                                    onChange={this.inputHandler}
                                    required="required"
                                />
                            </InputGroup>
                            {this.state.formOtpErr &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.formOtpErr}</div>
                            }
                            <div className="row">
                                <div className="col-12 text-right">
                                    <a href="#" className="forgot-link">Forget Password</a>
                                </div>
                            </div>
                            <div className="text-center otp-button-group">
                                <button type="submit" className="btn-signin" >Verify OTP</button>
                                <button type="button" className="btn-signin" onClick={() => this.resendOtp(this.state.login_email)}>Resend OTP</button>
                            </div>

                            <div className="text-center">
                                <div className="or">OR</div>
                                <button type="button" className="google-btn google-btn__2 mt-0" onClick={() => this.socialLogin(googleProvider)}><span><img src={Google} alt='password'/></span>Sign up with Google</button>
                            </div>
                        </form>
                    </Modal.Body>
                </Modal>

                {this.state.succmessage &&
                    <RegisterAlert registerSucc={this.state.succmessage} />
                }

                <ToastContainer />
            </>
        )

    }
}

export default Register;