import React, { Component } from 'react'
import IdleTimer from 'react-idle-timer'
import { Redirect } from 'react-router'
import axios from 'axios';

let api_url = process.env.REACT_APP_API_URL1;

class IdleTimerContainer extends Component {
  constructor(props) {
    super(props)
    this.idleTimer      = null
    this.handleOnIdle   = this.handleOnIdle.bind(this)
  }

  handleOnIdle (event) {
    console.log('user is idle', event)
    //console.log('last active', this.idleTimer.getLastActiveTime())
    let user_id         = localStorage.getItem('user_id');
    let token           = localStorage.getItem('token');

    let request    = { 
        'user_id': parseInt(user_id), 
        'access_token': token 
    }
    axios.post(api_url+'/api/v1/auth/logout', request, {
        headers: {
            'Content-Type': 'text/plain'
        }
    })
    .then(res => {

        if(res.data.success === true){

        }
    })

    localStorage.removeItem('token');
    localStorage.removeItem('user_id');
    localStorage.removeItem('user_name');
    localStorage.removeItem('user_email');
    localStorage.removeItem('partner_id');
    localStorage.removeItem('payment_line_id');
    localStorage.removeItem('claim_id');
    localStorage.removeItem('fee_amount');
    localStorage.removeItem('respondent_list');
    localStorage.removeItem('attorney_list');
    localStorage.removeItem('claimant_name');

    window.location.reload(true);
  }

  render() {
    if (!localStorage.getItem('token')) {
        return (
            <Redirect to="/" />
        )
    }
    return (
      <>
        <IdleTimer
          ref={ref => { this.idleTimer = ref }}
          timeout={5 * 1000}
          onIdle={this.handleOnIdle}
        />
      </>
    )
  }
}

export default IdleTimerContainer;