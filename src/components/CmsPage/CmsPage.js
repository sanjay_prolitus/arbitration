import React, { Component } from 'react';
import './cmspage.scss';
import Loader from 'react-loader'

let api_url = process.env.REACT_APP_API_URL1;

class CmsPage extends Component{

     constructor(props) {
        super(props);
        this.state = {
          page_slug: '',
          page_title: '',
          page_content: '',
          result: '',
          loaded: false
      };

    }

    componentDidMount(){

        let page_slug        = this.props.match.params.slug;
        this.getPageDesc(page_slug);

    }

    componentDidUpdate(prevProps){
        let curr_url =  this.props.match.params.slug;
        let page_url =  prevProps.match.params.slug;
        
        if (curr_url !== page_url) {
            this.setState({
                loaded: false
            })
            this.getPageDesc(curr_url);
        }

    }

    getPageDesc = (page_slug) => {
        if( page_slug !== '' ){
            fetch(api_url+'/api/v1/cms/pages/'+page_slug)
            .then(response => response.json())
            .then(data => 
                    this.setState({
                        page_title: data.data.title,
                        page_content: data.data.description,
                        loaded: true
                    })
            );
        }
    }

    render(){

        let page_title = this.state.page_title;
        let page_content = this.state.page_content;

        /*if( page_title === '' && page_content === '' ){
            page_title = "Page not Found";
            page_content = "404 - Page not found.";
        }*/

        return(
            <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} radius={12} color="#201e53">
                <section className='home-section home--module--one'>
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <h2 dangerouslySetInnerHTML={{ __html: page_title}} />
                                <div dangerouslySetInnerHTML={{ __html: page_content }} />
                            </div>
                        </div>
                    </div>
                </section>
            </Loader>
        )
    }
}
export default CmsPage;