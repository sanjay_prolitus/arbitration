import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import File from './../../assets/images/otp.png';
import './add-document.scss';

const AddDocument = () =>{  
    const [openAddDocument,setOpenAddDocument] = useState(false);
    const handleCloseAddDocument = () =>{
        setOpenAddDocument(false);
    }
    return(        
        <Modal className="add-document--modal white-background-modal" show={openAddDocument} onHide={openAddDocument}>
            <Modal.Body>
                <Button className="close" onClick={handleCloseAddDocument}>&times;</Button>
                <h3 className="text-center">Add Document</h3>
                <div className="d-none">
                    <h3 className="text-center mb-2">Upload Document</h3>
                    <i className="text-center d-block">Primary Document</i>
                </div>
                <div className="row align-items-center mb-4">
                    <div className="col-md-4 col-12">
                        <label>Document<sup>*</sup></label>
                    </div>
                    <div className="col-md-8 col-12">
                        <select className="form-control">
                            <option value="abc">ABC</option>
                        </select>
                    </div>
                </div>
                <div className="row align-items-center mb-4">
                    <div className="col-md-4 col-12">
                        <label>Document Type<sup>*</sup></label>
                    </div>
                    <div className="col-md-8 col-12">
                        <select className="form-control">
                            <option value="abc">ABC</option>
                        </select>
                    </div>
                </div>
                <div className="row mb-4">
                    <div className="col-md-4 col-12">
                        <label>Description</label>
                    </div>
                    <div className="col-md-8 col-12">
                        <textarea className="form-control" cols="4" rows="4">
                        </textarea>
                    </div>
                </div>
                <div className="row align-items-center mb-4">
                    <div className="col-md-4 col-12">
                        <label>Upload Document</label>
                    </div>
                    <div className="col-md-8 col-12">
                       <label className="file-attach" for='file'>
                           <input type="file" id="file"/>
                           <p className="text-uppercase">Choose file<img src={File} alt="Upload"/></p>
                       </label>
                    </div>
                </div>
                <button type="submit" className="btn-signin">Submit</button>
            </Modal.Body>
        </Modal>
    )
}

export default AddDocument;