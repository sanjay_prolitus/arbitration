import React, {Component} from 'react';
import { Link } from "react-router-dom";
import Loader from 'react-loader'
import { Redirect } from 'react-router';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import './expense.scss';
import NoDataTable from '../Common/NoDataTable';
import Breadcrumb from 'react-bootstrap/Breadcrumb'

let api_url = process.env.REACT_APP_API_URL1;
let payment_method = process.env.REACT_APP_PAYMENT_STRIPE_METHOD;

class Expense extends Component{
    constructor(props) {
        super(props);
        this.state = {
            claim_id : '',
            expense_list : '',
            loaded: false,
            goToPayment: false,
            goToStripePayment: false
        };
    }

    componentDidMount(){
        
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        let claim_id        = this.props.match.params.id;
        
        this.setState({
            claim_id: claim_id
        });

        if(token !== '' && user_id !== ''){
            this.getExpenses(user_id, token, claim_id)
        }
    }

    // get Expense list
    getExpenses = (user_id, token, claim_id) => {
        if(user_id){
            let expense_request    = { 
                'user_id': parseInt(user_id), 
                'access_token': token, 
                'claim_id': parseInt(claim_id), 
            }

            axios.post(api_url+'/api/v1/pending/order/payment/list', expense_request, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then(res => {
                if(res.data.success === true){

                    this.setState({
                        expense_list: res.data,
                        loaded: true
                    })
                }
            })
        }
    }

    // Download Docs 
    payEfile = (claim_id, payment_line_id) => {
        localStorage.setItem('claim_id', claim_id)
        localStorage.setItem('payment_line_id', payment_line_id)
        
        if(payment_method === "ON"){
            this.setState({
                goToStripePayment: true
            });
        }else{
            this.setState({
                goToPayment: true
            });
        }
    }

    render() {

        if (!localStorage.getItem('token')) {
            return (
                <Redirect to="/" />
            )
        }

        if(this.state.goToPayment){
            return(
                <Redirect to="/payment" />
            )
        }
        if(this.state.goToStripePayment){
            return(
                <Redirect to="/payment" />
            )
        }

        let expense_list_data = this.state.expense_list.pending_list;

        let columns = [
            {
                name: 'Order Name',
                selector: row => row.order_name,
                sortable: true
            },
            {
                name: 'Claimant / Respondent',
                selector: row => row.claimant_respodent,
                sortable: true
            },
            {
                name: 'Amount',
                selector: row => row.amount,
                sortable: true
            },
            {
                name: 'Status',
                selector: row => row.status,
                sortable: true
            } ,
            {
                name: 'Action',
                selector: row => row.action
            }         
        ];

        let list_data = [];
        if(expense_list_data !== undefined){
            let expense_list = Object.keys(expense_list_data); 
            let $this = this;
            expense_list.forEach(function(key, i) {

                //let action = <Link className="add-primary" onClick={() => $this.payEfile(expense_list_data[key].claim_id, expense_list_data[key].payment_line_id)}>View</Link>;
                let action = <Link className="add-primary" to={`/expense-detail/${ expense_list_data[key].claim_id }/${ expense_list_data[key].payment_line_id }`}>View</Link>;
                list_data.push({ 
                    order_name: expense_list_data[key].order_name, 
                    claimant_respodent: expense_list_data[key].claimant_respodent,
                    amount: "$ "+ expense_list_data[key].amount,
                    status: expense_list_data[key].status,
                    action: action
                })

            });
        }

        return(
            <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} radius={12} color="#201e53">
                <Breadcrumb>
                    <div className="container">
                        <div className="row">
                            <Breadcrumb.Item href="/dashboard">My Claims</Breadcrumb.Item>
                            <Breadcrumb.Item href="#">Expense</Breadcrumb.Item>
                        </div>
                    </div>
                </Breadcrumb>
                <div className="container">
                   <div className="row">
                       <div className="col-12">
                           <div className="claim-list claim-list-table ">
                                <Link to={`/claim-detail/${ this.state.claim_id }`}><h5>Claim#: {this.state.expense_list.claim_name}</h5></Link>
                                    
                                {list_data.length > 0 &&
                                    <DataTable
                                    columns={columns}
                                    data={list_data}
                                    fixedHeader
                                    />
                                }

                                { list_data.length == 0 &&
                                    <div className="table-responsive">
                                        <NoDataTable columns={columns} title="No Data Available" />
                                    </div>
                                }  
                                
                                
                           </div>
                       </div>
                   </div>
               </div>
           </Loader>
        )
    }
}
export default Expense;