import React, {Component} from 'react';
import './expense-detail.scss';
import axios from 'axios';
import { Redirect } from 'react-router';
import Loader from 'react-loader'
import { Link } from "react-router-dom";
import DataTable from 'react-data-table-component';
import NoDataTable from '../Common/NoDataTable';
import Breadcrumb from 'react-bootstrap/Breadcrumb'
import StripePayment from '../Payment/StripePayment';

let api_url = process.env.REACT_APP_API_URL1;
let payment_method = process.env.REACT_APP_PAYMENT_STRIPE_METHOD;

class ExpenseDetail extends Component {
    
    constructor(props) {
        super(props);
        this.state = {
            expense_detail : '',
            loaded: false,
            goToPayment: false,
            goToStripePayment: false
        };

    }

    componentDidMount(){

        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        localStorage.removeItem('payment_from')
        let claim_id        = this.props.match.params.claim_id; 
        let payment_line_id = this.props.match.params.payment_id; 
        
        if(token !== '' && user_id !== ''){
            this.getExpensesDetail(user_id, token, claim_id, payment_line_id)
        }
    }

    // get Expense Detail
    getExpensesDetail = (user_id, token, claim_id, payment_line_id) => {
        
        localStorage.setItem('claim_id', claim_id)
        localStorage.setItem('payment_line_id', payment_line_id)

        let expense_request    = { 
            'user_id': parseInt(user_id), 
            'access_token': token, 
            'claim_id': parseInt(claim_id), 
            'payment_line_id': parseInt(payment_line_id), 
        }

        axios.post(api_url+'/api/v1/order/detail', expense_request, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then(res => {
            if(res.data.success === true){

                this.setState({
                    expense_detail: res.data,
                    loaded: true
                })
            }
        })
        
    }

    // Download Docs 
    payEfile = (event) => {
        event.preventDefault();
        localStorage.removeItem('payment_from')
        localStorage.setItem('payment_from', "expense_detail")
        if(payment_method === "ON"){
            this.setState({
                goToStripePayment: true
            });
        }else{
            this.setState({
                goToPayment: true
            });
        }
    }

    render() {

        if (!localStorage.getItem('token')) {
            return (
                <Redirect to="/" />
            )
        }

        if(this.state.goToPayment){
            return(
                <Redirect to="/payment" />
            )
        }
        /*if(this.state.goToStripePayment){
            return(
                <Redirect to="/payment" />
            )
        }*/

        let expense_detail = this.state.expense_detail;
        let product_details = this.state.expense_detail.product_details;

        let columns = [
            {
                name: 'Description',
                selector: row => row.description,
                sortable: true
            },
            {
                name: 'Amount',
                selector: row => row.amount,
                sortable: true
            }            
        ];

        let list_data = [];
        if(product_details !== undefined){
            let product_list = Object.keys(product_details); 
            product_list.forEach(function(key, i) {

                list_data.push({ 
                    description: <Link onClick={e => e.preventDefault()}> {product_details[key].description} </Link>, 
                    amount: <Link onClick={e => e.preventDefault()}> {product_details[key].amount.toLocaleString(undefined, {minimumFractionDigits:2})} USD </Link>,
                })

            });
        }

        return(
            <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} radius={12} color="#201e53">
                <Breadcrumb>
                    <div className="container">
                        <div className="row">                            
                            <Breadcrumb.Item href="/dashboard">My Claims</Breadcrumb.Item>
                            <Breadcrumb.Item href="#">Expense Detail</Breadcrumb.Item>
                        </div>
                    </div>
                </Breadcrumb>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="file-details">
                            <Link to={`/claim-detail/${ expense_detail.claim_id }`}><h5>Claim#: {expense_detail.claim_name}</h5></Link>
                                
                                <form onSubmit={this.payEfile}>
                                    <div className="invoice">
                                        <div className="row">
                                            <div className="col-md-6 col-12">
                                                <ul>
                                                    <li>
                                                        <label>Order Name</label>
                                                        <p>{expense_detail.order_name}</p>
                                                    </li>
                                                    <li>
                                                        <label>Amount</label>
                                                        <p>{expense_detail.amount} USD</p>                                                        
                                                    </li>
                                                    <li>
                                                        <label>Status</label>
                                                        <p>{expense_detail.status}</p>                                                        
                                                    </li>
                                                </ul>
                                            </div>
                                            <div className="col-md-6 col-12">
                                                <ul>
                                                    <li>
                                                        <label>Claimant / Respondent</label>
                                                        <p>{expense_detail.claimant_respodent}</p>
                                                    </li>
                                                    <li>
                                                        <label>Date</label>
                                                        <p>{expense_detail.date}</p>                                                        
                                                    </li>
                                                </ul>
                                            </div>
                                            {product_details !== undefined && product_details.length ? (
                                            <>
                                                {list_data.length > 0 &&
                                                    <DataTable
                                                        columns={columns}
                                                        data={list_data}
                                                        fixedHeader
                                                    />
                                                }

                                                { list_data.length == 0 &&
                                                    <div className="claim-list-table table-responsive">
                                                        <NoDataTable columns={columns} title="No Data Available" />
                                                    </div>
                                                }     
                                            </>
                                        ) : null}
                                        </div>
                                    </div>

                                    <div className="text-right mb-4 invoice">
                                        <button type="submit" className="print-btn"> Pay Now </button>
                                    </div>
                                    
                                </form>
                            </div>
                       </div>
                   </div>
                </div>

                {this.state.goToStripePayment &&
                    <StripePayment />
                }

            </Loader> 
        )   
    }
}
export default ExpenseDetail;