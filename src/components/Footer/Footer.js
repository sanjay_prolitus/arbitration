import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import './footer.scss';
import axios from 'axios';

let api_url = process.env.REACT_APP_API_URL1;

class Footer extends Component{
    constructor(props) {
        super(props);
        this.state = {
            footer_section_list: ''
        };
    }

    componentDidMount(){
        
       this.getFooterdata()
        
    }

    // get Footerdata
    getFooterdata = () => {
        
        axios.get(api_url+'/api/v1/footer', {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then(res => {

            if(res.data.success === true){
                let footer_section_list = res.data.footer_section_list;
                this.setState({
                    footer_section_list: footer_section_list
                })

            }else{

            }
        })        
    }

    render() {

        let footer_section_list = this.state.footer_section_list;

        return(
            <>
                <div className="page-length"></div>
                <footer>
                    <div className="container">
                        <div className="row text-center text-md-left">
                            <div className="col-md-5 col-12">
                                <p>Copyright @mcarbitration.org</p>
                            </div>
                            <div className="col-md-7 col-12 text-md-right">
                                <ul>
                                    { footer_section_list ? footer_section_list.map((footer_section_list, footer_key) =>
                                    <li key={footer_key}><Link to={`/cms-page/${ footer_section_list.page_type_name }`} >{footer_section_list.display_name}</Link></li>
                                    ) : null}
                                </ul>
                            </div>
                        </div>
                    </div>
                </footer>
            </>
        )
    }
}
export default Footer;