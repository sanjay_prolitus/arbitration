import React, { useState } from 'react';
import Dropdown from 'react-bootstrap/Dropdown';
import { Link } from 'react-router-dom';
import './header-bottom.scss';
import axios from 'axios';

let api_url = process.env.REACT_APP_API_URL1;

const HeaderBottom = () =>{
    const [toggle,setToggle]        = React.useState(false);
    const [megaMenu, setMegaMenu]   = React.useState('');
    const toogleHandle = () =>{
        setToggle(!toggle)
    }       

    const getMegaMenu = React.useCallback(() => {
        
        axios.get(api_url+'/api/v1/megamenu', {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then((response) => {
            setMegaMenu(response.data.menu_list)
        })
        .catch((error) => {
            console.log(error)
        })
        
    }, [])

    React.useEffect(() => {
        getMegaMenu()
    }, [getMegaMenu])

    return(
        <section className='header--bottom'>
            <div className='container'>
                <div className='row'>
                    <div className='col-12'>
                        <div onClick={toogleHandle} className={`${toggle === true ? 'open-menu toogle':'toogle'}`}>
                            <span></span>
                        </div>
                        <nav className={`${toggle === true ? 'open-menu navbar':'navbar'}`}>   
                            <ul className="navbar-nav">
                                {megaMenu ? megaMenu.map((megamenu, index) =>
                                    <>
                                    {megamenu.column != '' ? (
                                        <Dropdown className="nav-item dropdown" key={index}>
                                            <Dropdown.Toggle id="dropdown-basic" className="nav-link dropdown-toggle">{megamenu.name}</Dropdown.Toggle>
                                            <div className="dropdown-menu">
                                                <div className="container">
                                                    <div className="row">
                                                        {megamenu.content_type === "csm_page" ? (
                                                            <>
                                                                {megamenu.column ? megamenu.column.map((columns, j) =>
                                                                    <div className="col-md-3 col-12" key={j}>
                                                                        <a href={`/cms-page/${ columns.page_type_name }`} className="dropdown-item">{columns.name}</a>
                                                                    </div>
                                                                ) : null}
                                                            </>
                                                        ) : (
                                                            <>
                                                                {megamenu.column ? megamenu.column.map((columns, j) =>
                                                                    <div className="col-md-3 col-12" key={j} >
                                                                        <a href={api_url+columns.document} className="dropdown-item">{columns.name}</a>
                                                                    </div>
                                                                ) : null}
                                                            </>
                                                        )}      
                                                    </div>
                                                </div>
                                            </div>
                                        </Dropdown>
                                    ) : (
                                        <>
                                        {megamenu.content_type === "csm_page" ? (
                                            <>
                                                <li className="nav-item"><Link to='#' className="nav-link">{megamenu.name}</Link></li>
                                            </>
                                        ) : (
                                            <>
                                                <li className="nav-item"><Link to='#' className="nav-link">{megamenu.name}</Link></li>
                                            </>
                                        )}   
                                        </>  
                                    )}    
                                    </>
                                ) : null}
                                
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </section>
    )
}
export default HeaderBottom;