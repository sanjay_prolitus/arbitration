import React, {Component, useEffect, useState} from 'react';
import axios from "axios"
import { Redirect } from 'react-router';

import StripeContainer from './StripeContainer';
import StripePayment from './StripePayment';
import './makepayment.scss';

class MakePayment extends Component{

    constructor(props) {
        super(props);
        this.state = {
            showItem: false
        };
    }

    componentDidMount(){
        
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        
        
    }

    render(){

        if(!localStorage.getItem('token')){
            <Redirect to="/" />
        }
        
        return (
            <>
                <StripePayment />
            </>
        )
    };

}
export default MakePayment;