import React from 'react';
import { Redirect } from 'react-router';
import {loadStripe} from '@stripe/stripe-js';
import {CardElement, Elements, ElementsConsumer} from "@stripe/react-stripe-js"
import axios from "axios";
import './makepayment.scss';

import PaymentSuccessAlert from '../Alert/PaymentSuccessAlert';
import './paymentalert.scss';
import Loader from 'react-loader'

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import Check from './../../assets/images/check.png';
import Danger from './../../assets/images/danger.png';


let api_url = process.env.REACT_APP_API_URL1;
let payment_method = process.env.REACT_APP_PAYMENT_STRIPE_METHOD;

class CheckoutForm extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      stripe_publishable_key : '',
      stripe_secret_key : '',
      acquirer_id : '',
      payment_reference : '',
      client_secret : '',
      strip_customer_id : '',
      amount : '',
      claim_id : '',
      claim_referance : '',
      message : '',
      errorMsg : '',
      paymentIntent : '',
      acquirer_name : '',
      loaded : false,
      loading : false,
      payment_status : false,
      cancel_message : ''
    };

  }

  componentDidMount(){

    this.setState({
      loaded: true
    });

    let user_id     = localStorage.getItem('user_id')
    let claim_id    = localStorage.getItem('claim_id')
    let token       = localStorage.getItem('token')
    let payment_line_id       = localStorage.getItem('payment_line_id')

    let reference   = (new URLSearchParams(window.location.search)).get("reference")
    let current_url = window.location.href.split('?')[0];
    let split_url   = current_url.split('/stripe/');
    let payment_status   = split_url[1];
    //console.log("payment_method: ", payment_method)
    //console.log("reference: ", reference)
    if(payment_method === "ON" && reference === null){
      this.payment_acquirer_new()
    }
    
    this.setState({
      payment_status: payment_status
    });

    if(reference && payment_status === "success"){
      this.orderComplete();
    }

    if(reference && payment_status === "cancel"){
      this.orderCancel();
    }

  }

  payment_acquirer_new = async () => {
    //event.preventDefault();

    let user_id     = localStorage.getItem('user_id')
    let claim_id    = localStorage.getItem('claim_id')
    let token       = localStorage.getItem('token')
    let payment_line_id       = localStorage.getItem('payment_line_id')

    let acquirer_request = { 
      'user_id': parseInt(user_id), 
      'access_token': token 
    }

    axios.post(api_url+'/api/v1/get/payment_acquirer', acquirer_request, {
      headers: {
        'Content-Type': 'text/plain'
      }
    })
    .then(res => {

        if(res.data.success === true){
          
          let acquirer_id   = res.data.payment_methods[0]['acquirer_id'];
          let acquirer_name = res.data.payment_methods[0]['acquirer_name'];

            this.setState({
                acquirer_id: acquirer_id,
                acquirer_name : acquirer_name,
                provider: res.data.payment_methods[0]['provider'],
                stripe_publishable_key: res.data.payment_methods[0]['stripe_publishable_key'],
                stripe_secret_key: res.data.payment_methods[0]['stripe_secret_key'],
                loading: true
            }, () => {
                this.stripeCheckout(user_id, token, claim_id, acquirer_id, acquirer_name, payment_line_id)
            })
        }else{
            this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                setTimeout(() => this.setState({ errorMsg: false }), 5000);
            })
        }
    })
    .catch(err => {
        console.log(err)
    })

  };

  stripeCheckout = (user_id, token, claim_id, acquirer_id, acquirer_name, payment_line_id) => {
      
      this.setState({
        loaded: false
      });

      let transaction_request = { 
        'user_id': parseInt(user_id), 
        'access_token': token, 
        'claim_id': parseInt(claim_id), 
        'acquirer_id': parseInt(acquirer_id), 
        'payment_line_id': parseInt(payment_line_id) 
      }
      
      axios.post(api_url+'/get/checkout/transaction_id', transaction_request, {
          headers: {
              'Content-Type': 'text/plain'
          }
      })
      .then(res => {
        
          if(res.data.success === true){
              let checkout_url            = res.data.checkout_url;
              let payment_reference       = res.data.payment_reference;
              let pi_reference            = res.data.pi_reference;
              let payment_transaction_id  = res.data.payment_transaction_id;
              this.setState({
                  claim_id: res.data.claim_id,
                  payment_line_id: res.data.payment_line_id,
                  payment_reference: payment_reference,
                  payment_transaction_id: payment_transaction_id,
                  amount: res.data.amount,
                  session_id: res.data.session_id,
                  checkout_url: res.data.checkout_url,
                  pi_reference: pi_reference,
                  cancel_url: res.data.cancel_url,
                  success_url: res.data.success_url,
                  loaded: true
                })
                localStorage.setItem('payment_reference', payment_reference)
                localStorage.setItem('pi_reference', pi_reference)
                localStorage.setItem('acquirer_id', acquirer_id)
                localStorage.setItem('acquirer_reference', acquirer_name)
                localStorage.setItem('payment_transaction_id', payment_transaction_id)
                
                window.location.href=checkout_url;

          }else{
            this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                  setTimeout(() => this.setState({ errorMsg: false }), 5000);
              })
          }
      })

      
  }

  handleSubmit = async (event) => {
    // Block native form submission.
    event.preventDefault();

    const {stripe, elements} = this.props;

    if (!stripe || !elements) {
      // Stripe.js has not loaded yet. Make sure to disable
      // form submission until Stripe.js has loaded.
      return;
    }

    this.setState({
      loading: false
    });

    let user_id     = localStorage.getItem('user_id')
    let partner_id  = localStorage.getItem('partner_id')
    let claim_id    = localStorage.getItem('claim_id')
    let token       = localStorage.getItem('token')
    let payment_line_id       = localStorage.getItem('payment_line_id')

    let acquirer_request = { 
      'user_id': parseInt(user_id), 
      'access_token': token 
    }

    axios.post(api_url+'/api/v1/get/payment_acquirer', acquirer_request, {
      headers: {
        'Content-Type': 'text/plain'
      }
    })
    .then(res => {

        if(res.data.success === true){
          
          let acquirer_id = res.data.payment_methods[0]['acquirer_id'];

            this.setState({
                acquirer_id: acquirer_id,
                acquirer_name : res.data.payment_methods[0]['acquirer_name'],
                provider: res.data.payment_methods[0]['provider'],
                stripe_publishable_key: res.data.payment_methods[0]['stripe_publishable_key'],
                stripe_secret_key: res.data.payment_methods[0]['stripe_secret_key'],
                loading: true
            }, () => {
                this.getTransactionID(user_id, token, claim_id, acquirer_id, payment_line_id)
            })
        }else{
            this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                setTimeout(() => this.setState({ errorMsg: false }), 5000);
            })
        }
    })
    .catch(err => {
        console.log(err)
    })

    // Get a reference to a mounted CardElement. Elements knows how
    // to find your CardElement because there can only ever be one of
    // each type of element.
    const cardElement = elements.getElement(CardElement);

    const {error, paymentMethod} = await stripe.createPaymentMethod({
      type: 'card',
      card: cardElement,
    });

    /*if (error) {
      console.log('[error]', error);
    } else {
      console.log('[PaymentMethod]', paymentMethod);
    }*/

    

  };

  //  Claimant Information (Fileclaim Master)
  getTransactionID = (user_id, token, claim_id, acquirer_id, payment_line_id) => {
      
      if(user_id){ 
          let transaction_request = { 
            'user_id': parseInt(user_id), 
            'access_token': token, 
            'claim_id': parseInt(claim_id), 
            'acquirer_id': parseInt(acquirer_id), 
            'payment_line_id': parseInt(payment_line_id) 
          }

          axios.post(api_url+'/get/payment/transaction_id', transaction_request, {
              headers: {
                  'Content-Type': 'text/plain'
              }
          })
          .then(res => {

              if(res.data.success === true){
                  this.setState({
                      payment_transaction_id: res.data.payment_transaction_id,
                      amount: res.data.amount,
                      payment_reference: res.data.payment_reference,
                      client_secret: res.data.client_secret,
                      strip_customer_id: res.data.strip_customer_id
                    }, (e) => {
                      this.confirmPayment(e)
                    })
              }else{
                this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                      setTimeout(() => this.setState({ errorMsg: false }), 5000);
                  })
              }
          })

          /*axios.get('https://arbitration.mywwwserver.in/api/payment.php')
          .then(res => {

            if(res.data.status === 'requires_payment_method'){
              this.setState({
                client_secret: res.data.client_secret,
                payment_transaction_id : 2,
                amount: 100,
                payment_reference: "S00070-2",
                paymentCompleted: true
              }, () => {
                this.confirmPayment()
              })
            }else{

            }
          })
          .catch(err => {
            console.log(err)
          })*/
      }
  }

  confirmPayment = async () => {

      const {stripe, elements} = this.props;
      
      const cardElement = elements.getElement(CardElement);
      const {error, paymentMethod} = await stripe.createPaymentMethod({
        type: 'card',
        card: cardElement,
      });

      /*if (error) {
        console.log('[error]', error);
      } else {
        console.log('[PaymentMethod]', paymentMethod);
      }*/

      stripe.confirmCardPayment(this.state.client_secret, {
          payment_method: {
            card: cardElement
          }
      })

      .then(result => {

          if (result.error) {
              // Show error to your customer
              this.setState({ errorMsg: result.error.message, loaded: true }, () => {
                setTimeout(() => this.setState({ errorMsg: false }), 5000);
              })
          } else {
              // The payment succeeded!
              this.setState({ 
                paymentIntent: result.paymentIntent.id
              }, () => {
                this.orderComplete();
              })
          }
      })
  }

  // Shows a success message when the payment is complete
  orderComplete = async () => {

    this.setState({
        loaded: false
      });

    let user_id     = localStorage.getItem('user_id')
    let claim_id    = localStorage.getItem('claim_id')
    let token       = localStorage.getItem('token')
    let payment_line_id       = localStorage.getItem('payment_line_id');
    let payment_reference;
    let pi_reference;
    let acquirer_id;
    let acquirer_reference;

    if(payment_method === "ON"){
      payment_reference   = localStorage.getItem('payment_reference');
      pi_reference        = localStorage.getItem('pi_reference');
      acquirer_id         = localStorage.getItem('acquirer_id');
      acquirer_reference  = localStorage.getItem('acquirer_reference');
    } else {
      payment_reference   = this.state.payment_reference;
      pi_reference        = this.state.paymentIntent;
      acquirer_id         = this.state.acquirer_id;
      acquirer_reference  = this.state.acquirer_name;
    }

      let create_payment_request = { 
        'user_id': parseInt(user_id), 
        'access_token': token, 
        'claim_id': parseInt(claim_id), 
        'payment_reference': payment_reference, 
        'payment_line_id': parseInt(payment_line_id),
        'pi_reference': pi_reference,
        'acquirer_reference': acquirer_reference,
        'acquirer_id': acquirer_id
        /*'strip_customer_id': this.state.strip_customer_id,
        'client_secret': this.state.client_secret,
        'paid_amount': this.state.amount */
      }

      axios.post(api_url+'/api/v1/payment/create', create_payment_request, {
          headers: {
              'Content-Type': 'text/plain'
          }
      })
      .then(res => {

          if(res.data.success === true){
              this.setState({
                  claim_id: res.data.claim_id,
                  claim_referance: res.data.claim_referance,
                  payment_reference: res.data.payment_reference,
                  message: res.data.message,
                  loaded: true
                })

                localStorage.removeItem('respondent_list');
                localStorage.removeItem('attorney_list');
                localStorage.removeItem('fee_amount');
                localStorage.removeItem('claimant_name');
                localStorage.removeItem('claim_id');
                localStorage.removeItem('payment_line_id');
                localStorage.removeItem('partner_id');

                localStorage.removeItem('payment_reference');
                localStorage.removeItem('pi_reference');
                localStorage.removeItem('acquirer_id');
                localStorage.removeItem('acquirer_reference');
                localStorage.removeItem('payment_transaction_id');
                localStorage.removeItem('payment_from')
          }else{
            this.setState({ errorMsg: "Somthing went wrong, try again!!", loaded: true }, () => {
              setTimeout(() => this.setState({ errorMsg: false }), 5000);
            })
          }
      })
  }
  // Shows a cancel message when the payment is cancelled
  orderCancel = async () => {

    this.setState({
        loaded: false
      });

      let user_id                 = localStorage.getItem('user_id')
      let token                   = localStorage.getItem('token')
      let pi_reference            = localStorage.getItem('pi_reference')
      let payment_transaction_id  = localStorage.getItem('payment_transaction_id')


      let cancel_payment_request = { 
        'user_id': parseInt(user_id), 
        'access_token': token, 
        'payment_transaction_id': payment_transaction_id, 
        'pi_reference': pi_reference
      }

      axios.post(api_url+'/api/v1/strip/payment_cancel', cancel_payment_request, {
          headers: {
              'Content-Type': 'text/plain'
          }
      })
      .then(res => {
        //console.log("Cancel: ", res)
        if(res.data.success === true){
              this.setState({
                  cancel_message: res.data.message,
                  loaded: true
                })

                /*localStorage.removeItem('respondent_list');
                localStorage.removeItem('attorney_list');
                localStorage.removeItem('fee_amount');
                localStorage.removeItem('claimant_name');
                localStorage.removeItem('claim_id');
                localStorage.removeItem('payment_line_id');
                localStorage.removeItem('partner_id');*/

                localStorage.removeItem('payment_reference');
                localStorage.removeItem('pi_reference');
                localStorage.removeItem('acquirer_id');
                localStorage.removeItem('acquirer_reference');
                localStorage.removeItem('payment_transaction_id');

          }else{
            this.setState({ errorMsg: "Somthing went wrong, try again!!", loaded: true }, () => {
              setTimeout(() => this.setState({ errorMsg: false }), 5000);
            })
          }
      })
  }

  // Show the customer the error from Stripe if their card fails to charge
  showError (errorMsgText) {
      this.loading(false);
      var errorMsg = document.querySelector("#card-error");
      errorMsg.textContent = errorMsgText;
      setTimeout(function() {
          errorMsg.textContent = "";
      }, 4000);
  };

  // Show a spinner on payment submission
  loading(isLoading) {
      if (isLoading) {
          // Disable the button and show a spinner
          document.querySelector("button").disabled = true;
          document.querySelector("#spinner").classList.remove("hidden");
          document.querySelector("#button-text").classList.add("hidden");
      } else {
          document.querySelector("button").disabled = false;
          document.querySelector("#spinner").classList.add("hidden");
          document.querySelector("#button-text").classList.remove("hidden");
      }
  }

  render() {
    if (!localStorage.getItem('token')) {
        return (
            <Redirect to="/" />
        )
    }

    let claim_id          = localStorage.getItem('claim_id');
    let payment_line_id   = localStorage.getItem('payment_line_id');
    if (this.state.payment_status === "cancel" ) {
        if (localStorage.getItem('payment_from') === "file_document" ) {
          return (
              <Redirect to={`/claim-file-detail/${claim_id}`} />
          )
        } else if (localStorage.getItem('payment_from') === "expense_detail" ) {
          return (
              <Redirect to={`/expense-detail/${claim_id}/${payment_line_id}`} />
          )
        } else {
          return (
              <Redirect to="/claim-summary" />
          )
        }
    }

    const {stripe} = this.props;
    return (
      <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} radius={12} top="50%" color="#201e53">
         
         {payment_method !== "ON" &&
          <form onSubmit={this.handleSubmit}>
            {this.state.errorMsg &&
                <div className="form-validate-error" style={{ color:"red"}}>{this.state.errorMsg}</div>
            }
            <h5>Claimant Name: {localStorage.getItem('claimant_name')}</h5>
            <h6>Fee Amount: {parseFloat(localStorage.getItem('fee_amount')).toFixed(2)} USD</h6>
            <br/>
            <CardElement
              options={{
                style: {
                  base: {
                    fontSize: '16px',
                    color: '#424770',
                    '::placeholder': {
                      color: '#aab7c4',
                    },
                  },
                  invalid: {
                    color: '#9e2146',
                  },
                },
              }}
            />
            <button type="submit" disabled={!stripe}>
              {this.state.loading ? "Loading..." : "Pay"}
            </button>
          </form>
        }

          {/*this.state.message && this.state.payment_status === "success" ? (
            <div className="alert--modal ">
               <div className="modal-dialog">
                  <div className="modal-content">
                     <div role="alert" className="fade alert alert-success show" style={{ marginBottom: 0 }}>
                        <button type="button" className="close btn btn-primary">×</button>
                        <div className="d-flex align-items-start">
                           <img className="mr-3" src={Check} alt="alert" />
                           <div>
                              <p>{this.state.message}</p>
                           </div>
                        </div>
                     </div>
                  </div>
               </div>
            </div>
          ):null*/}

          {/*this.state.payment_status === "cancel" &&
          <div className="alert--modal ">
             <div className="modal-dialog">
                <div className="modal-content">
                   <div role="alert" className="fade alert alert-danger show" style={{ marginBottom: 0 }}>
                      <button type="button" className="close btn btn-primary">×</button>
                      <div className="d-flex align-items-start">
                         <img className="mr-3" src={Danger} alt="alert" />
                         <div>
                            <p>{this.state.cancel_message}</p>
                         </div>
                      </div>
                   </div>
                </div>
             </div>
          </div>
          */}

        {this.state.message &&
          <PaymentSuccessAlert paymentsuccess={this.state.message} />
        }
      </Loader>
    );
  }
}

const InjectedCheckoutForm = () => {
  return (
    <ElementsConsumer>
    
      {({elements, stripe}) => (
        <div className="paymentForm">
        <CheckoutForm elements={elements} stripe={stripe} />
      </div>
      )}
    </ElementsConsumer>
  );
};

// Make sure to call `loadStripe` outside of a component’s render to avoid
// recreating the `Stripe` object on every render.
// const stripePromise = loadStripe('pk_test_6pRNASCoBOKtIshFeQd4XMUh');
const PUBLIC_KEY = "pk_test_51Hn7xiJrPHxdZCLAGxnB9HWbec3H31TzVH8T2tB54Og8wB8YmF7sbWm5Ln2kJ6NYlE8hVplx3zEk1MTokvlXcLXM001HwXQ6Yu"
//const PUBLIC_KEY = process.env.REACT_APP_STRIPE_PUBLICKEY
//const stripeAccount = "acct_1Hn7xiJrPHxdZCLA"

const stripeTestPromise = loadStripe(PUBLIC_KEY)

const App = () => {
  return (
    <Elements stripe={stripeTestPromise}>
      <InjectedCheckoutForm />
    </Elements>
  );
};

export default App;