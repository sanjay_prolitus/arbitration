import React, { Component } from 'react';
import HomeImage from './../../assets/images/home-image.png';
import './home.scss';
import axios from 'axios';
import Loader from 'react-loader'

let api_url = process.env.REACT_APP_API_URL1;

class Home extends Component{
    constructor(props) {
        super(props);
        this.state = {
            banners: '',
            loaded: false
        };
    }

    componentDidMount(){
        
       this.getHomepagedata()
        
    }

    // get homepage data
    getHomepagedata = () => {
        
        axios.get(api_url+'/api/v1/homepage', {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then(res => {
            
            if(res.data.success === true){

                this.setState({
                    banners: res.data.banner,
                    loaded: true
                })
            }else{

            }
        })        
    }

    render() {
        let homepage_data = this.state.banners;
        
        return(
            <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} radius={12} color="#201e53">
                {homepage_data !== undefined && homepage_data.length &&
                    <>
                    {homepage_data ? homepage_data.map((homepage, index) =>
                        <>
                        {homepage.banner_type === "text" &&
                            <section className='home-section home--module--one'>
                                <div className="container">
                                    <div className="row">
                                        <div className="col-12">
                                            <div dangerouslySetInnerHTML={{ __html: homepage.banner_text }} />
                                        </div>
                                    </div>
                                </div>
                            </section>
                        }
                        {homepage.banner_type === "i_w_c" &&
                        <section className='home-section home--module--two' style={{ backgroundImage: `url(${api_url+homepage.image_url})` }}>
                            <div className="container">
                                <div className="row">
                                    {homepage.banner_text &&
                                        <div className="col-md-5 col-12">
                                            <div dangerouslySetInnerHTML={{ __html: homepage.banner_text }} />
                                        </div>
                                    }
                                </div>
                            </div>
                        </section>
                        }
                        {homepage.banner_type === "il_cr" &&
                        <section className='home-section home--module--three'>
                            <div className="container">
                                <div className="row">
                                    <div className="col-md-7 col-12">
                                        <img src={api_url+homepage.image_url} alt={homepage.name} className='img-fluid'/>
                                    </div>
                                    <div className="col-md-5 col-12 d-flex flex-column justify-content-center">
                                        <div dangerouslySetInnerHTML={{ __html: homepage.banner_text }} />
                                    </div>
                                </div>
                            </div>
                        </section>
                        }
                        </>
                    ) : null}
                    </>
                } 
            </Loader>
        )
    }
}
export default Home;