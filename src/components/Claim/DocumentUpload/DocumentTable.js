import React, { Component } from 'react';
import DataTable from 'react-data-table-component';
import Edit from './../../../assets/images/edit.png';
import Delete from './../../../assets/images/delete.png';
import Excel from './../../../assets/images/excel.png';
import Pdf from './../../../assets/images/pdf.png';
import File from './../../../assets/images/otp.png';
import './document-table.scss';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import { Link } from "react-router-dom";
import Loader from 'react-loader';
import NoDataTable from '../../Common/NoDataTable';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileUpload } from '@fortawesome/free-solid-svg-icons'

import axios from 'axios';

let api_url = process.env.REACT_APP_API_URL1;

class DocumentTable extends Component {

    constructor(props) {
        super(props);
        this.state = {
            claim_id:'',
            document_id: '',
            payment_line_id: '',
            parent_id: '',
            document_config_id: '',
            description: '',
            document: '',
            doc_type: '',
            document_name: '',
            doc_attachment_type: '',
            success_message: false,
            error_message: '',
            loading: false,
            showEditModel: false,
            showSecModel: false,
            edit_docs: false,
            master_data: false,
            loaded: false,
            fileName: '',
            isDisabled: false
        };

        this.handleCloseEditDocument    = this.handleCloseEditDocument.bind(this);
        this.handleCloseSecDocument     = this.handleCloseSecDocument.bind(this);
    }

    componentDidMount(){

        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');

        if(token !== '' && user_id !== ''){
            this.fileclaimMaster(user_id, token)
        }

        this.setState({
            loaded: true
        })
    }

    handleCloseEditDocument(e) {
        e.preventDefault();
        this.setState({
            showEditModel: false
        })
    }

    handleCloseSecDocument(e) {
        e.preventDefault();
        this.setState({
            showSecModel: false,
            description: '',
            document_config_id: '',
            document: '',
            fileName: '',
        })
    }

    inputHandler = (event) =>{

        this.setState ({ [event.target.name] : event.target.value })
    }

    inputSelectHandler = (event) =>{

        var el          = document.getElementById("document_config_id");
        var document_config_id    = el.options[el.selectedIndex].getAttribute("data-id");
        var document_name    = el.options[el.selectedIndex].getAttribute("data-name");
        
        this.setState({ 
            [event.target.name] : event.target.value, 
            document_config_id : document_config_id,
            document_name : document_name
        });

    }

    onFileChange = event => {
        this.setState({ 
            document: event.target.files[0],
            fileName: event.target.files[0].name 
        });
    };

    // Download Docs 
    downloadDocs = (claim_id, document_id, payment_line_id, doc_attachment_type ) => {
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');

        let get_file_detail = doc_attachment_type.split('/');
        let type = get_file_detail[0];
        let ext  = get_file_detail[1];

        let file_request    = { 
            'user_id': parseInt(user_id), 
            'access_token': token, 
            'claim_id': claim_id, 
            'document_id': document_id, 
            'payment_line_id': payment_line_id
        }

        axios.post(api_url+'/api/v1/document/download', file_request, {
            headers: {
                'Content-Type': 'application/'+type+'/*'
            }
        })
        .then(res => {       
            
            if(res.data.success === true){
                let document_url = res.data.document_url;
                window.open(api_url+document_url, '_blank');
            }
        })
        
    }

    // Download Docs 
    removeDocs = (claim_id, document_id, payment_line_id) => {
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');

        document_id = [document_id]
        let remove_request    = { 
            'user_id': parseInt(user_id), 
            'access_token': token,
            'claim_id': claim_id,
            'payment_line_id': payment_line_id,
            'documents': document_id,
        }
                
        axios.post(api_url+'/api/v1/fileclaim/remove-document', remove_request, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then(res => {       
            let result = res.data

            if(result.success === true){
                /*this.setState({ success_message: "Document removed successfully." }, () => {
                    setTimeout(() => this.setState({ success_message: false }), 3000);
                })*/

                toast.success('Document removed successfully.', {
                    position: "top-right",
                    autoClose: 3000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });

                this.props.documentUploadcallback()
                
            }else{
                /*this.setState({ error_messgae: "Somthing went wrong!!" }, () => {
                    setTimeout(() => this.setState({ error_messgae: false }), 3000);
                })*/
                toast.error('Somthing went wrong!!', {
                    position: "top-right",
                    autoClose: 3000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }
        })
        
    }

    // Edit Docs 
    editDocs = (document_id, payment_line_id, index) => {

        let documents_info  = this.props.table_data;
        let documents       = documents_info.documents;

        if(documents   == '' && typeof documents == 'undefined'){
            return false;
        }
        
        let documents_data    = documents[index];
        let document_config_id  = documents_data.document_configuration_id[0];

        this.setState({
            document_id: document_id,
            payment_line_id: payment_line_id,
            showEditModel: true,
            document_name: documents_data.doc_type,
            doc_type: documents_data.doc_type.toString().toLowerCase(),
            description: documents_data.description,
            document_config_id: document_config_id
        })

    }

    // Submit Edit Docs 
    submitEditDocs = (e) => {
        e.preventDefault();
        
        this.setState({
            showEditModel: true,
            edit_docs: true
        }, () => {
            this.docsUpload(e)
        })

    }

    // Add Secondary Docs 
    addSecondaryDocs = (document_id, payment_line_id) => {
        this.setState({
            parent_id: document_id,
            payment_line_id: payment_line_id,
            showSecModel: true,
            description: ''
        })
    }

    // Submit Secondary Docs 
    submitSecondaryDocs = (e) => {
        e.preventDefault();
        this.setState({
            edit_docs: false
        }, () => {
            this.getDocumentByID(e)
        })
    }

    // get documents with id
    getDocumentByID = (e) => {
        e.preventDefault();
        let access_token        = localStorage.getItem('token');
        let user_id             = localStorage.getItem('user_id');

        let claim_id            =  localStorage.getItem("claim_id"); 
        let payment_line_id     =  this.state.payment_line_id ? this.state.payment_line_id : 0; 
        let partner_id          =  0;

        let document_config_id  =  this.state.document_config_id ? this.state.document_config_id : ''; 
        let documents           =  this.state.document ? this.state.document : '';  
        let document_name       =  this.state.document_name ? this.state.document_name : '';  
        let doc_type            =  this.state.document_name ? this.state.document_name : '';

        let request = { 
            "user_id": parseInt(user_id),
            "access_token": access_token,
            "claim_id": parseInt(claim_id),
            "partner_id": parseInt(partner_id),
            "payment_line_id": parseInt(payment_line_id)
        }

        if(document_name && doc_type && document_config_id && documents){

            this.setState({
                loaded: false,
                isDisabled: true
            })

            axios.post(api_url+'/api/v1/get/document/id', request, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then( res => {
                let result = res.data

                if(result.success === true){
                    this.setState({ 
                        document_id: res.data.document_id,
                        payment_line_id: res.data.payment_line_id,
                        partner_id: res.data.partner_id
                    }, () => {
                        this.docsUpload(e);
                    })
                    
                }else{
                    this.setState({ errorMsg: "Somthing went wrong!!", isDisabled: false, loaded:true }, () => {
                        setTimeout(() => this.setState({ errorMsg: false }), 5000);
                    })
                }
            })
            .catch(err => {
                console.log(err)
            })
        }
        else {
            if(document_name == ''){
                this.setState({ formValidateError0: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formValidateError0: false }), 5000);
                })
            }
            if(doc_type == '' || document_config_id == ''){
                this.setState({ formValidateError1: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formValidateError1: false }), 5000);
                })
            }
            if(documents == ''){
                this.setState({ formValidateError2: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formValidateError2: false }), 5000);
                })
            }
        }
        
    }

    // upload documents
    docsUpload = (event) => {
        event.preventDefault();

        let access_token        = localStorage.getItem('token');
        let user_id             = localStorage.getItem('user_id');
        
        let claim_id            =  localStorage.getItem("claim_id"); 
        let payment_line_id     =  this.state.payment_line_id ? this.state.payment_line_id : 0; 
        let parent_id           =  this.state.parent_id ? this.state.parent_id : 0;
        let document_config_id  =  this.state.document_config_id ? this.state.document_config_id : '';  
        let description         =  this.state.description ? this.state.description : '';
        let documents           =  this.state.document ? this.state.document : '';  
        let document_name       =  this.state.document_name ? this.state.document_name : '';  
        let doc_type            =  this.state.document_name ? this.state.document_name : '';  
        let document_id         =  this.state.document_id ? this.state.document_id : 0;
        let edit_docs           =  this.state.edit_docs ? this.state.edit_docs : '';

        if(document_name && doc_type && documents){
            
            if (documents.name.match(/\.(jpg|jpeg|png|pdf|JPG|JPEG|PNG|PDF)$/)) {

                this.setState({
                    loaded: false
                })

                const formData = new FormData();
                formData.append('user_id', parseInt(user_id))
                formData.append('access_token', access_token)
                formData.append('claim_id', claim_id)
                formData.append('payment_line_id', payment_line_id)
                formData.append('document_id', document_id)
                formData.append('parent_id', parent_id)
                formData.append('document_configuration_id', document_config_id)
                formData.append('description', description)
                formData.append('document', documents)
                formData.append('document_name', document_name)
                formData.append('doc_type', doc_type.toString().toLowerCase())
            
                axios.post(api_url+'/api/v1/upload/document', formData, {
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                })
                .then( res => {
                    let result = res.data
                    
                    if(result.success === true){
                        this.setState({
                            claim_id: res.data.claim_id,
                            payment_line_id: res.data.payment_line_id,
                            document_id: res.data.document_id,
                            showModel: false,
                            showSecModel: false,
                            showEditModel: false,
                            loaded: true,
                            description: '',
                            document_config_id: '',
                            fileName: '',
                            isDisabled: false
                        })

                        /*this.setState({ success_message: "successfully added." }, () => {
                            setTimeout(() => this.setState({ success_message: false }), 5000);
                        })*/

                        toast.success('Successfully added.', {
                            position: "top-right",
                            autoClose: 3000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });

                        this.props.documentUploadcallback()

                    }else{
                        /*this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                            setTimeout(() => this.setState({ errorMsg: false }), 5000);
                        })*/
                        this.setState({
                            loaded: true,
                            isDisabled: false
                        })

                        toast.error('Somthing went wrong!!', {
                            position: "top-right",
                            autoClose: 3000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }
                })
                .catch(err => {
                    console.log(err)
                })

            }else{
                this.setState({ formValidateError3: "Only jpg, jpeg, png, pdf files are allowed!" }, () => {
                    setTimeout(() => this.setState({ formValidateError3: false }), 5000);
                })
            }
        }
        else {
            if(document_name == ''){
                this.setState({ formValidateError0: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formValidateError0: false }), 5000);
                })
            }
            if(doc_type == ''){
                this.setState({ formValidateError1: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formValidateError1: false }), 5000);
                })
            }
            if(documents == ''){
                this.setState({ formValidateError2: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formValidateError2: false }), 5000);
                })
            }
            
        }
            
    }

    // get fileclaim Master
    fileclaimMaster = (user_id, token) => {

        let fileclaim_request = { 
            "user_id": parseInt(user_id),
            "access_token": token
        }
        
        axios.post(api_url+'/api/v1/fileclaim/master', fileclaim_request, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data
            if(result.success === true){

                let document_name;                
                let doc_type;                
                let document_config_id;                
                let documents     = res.data.documents;
                if(documents && documents.length){
                    document_name   = documents[0]['name']
                    doc_type        = documents[0]['doc_type']
                    document_config_id        = documents[0]['id']
                }
                this.setState({ 
                    master_data: res.data,
                    /*document_name: document_name,
                    doc_type: doc_type,
                    document_config_id: document_config_id,*/
                    documents_data: documents
                })
                
            }else{
                this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                    setTimeout(() => this.setState({ errorMsg: false }), 5000);
                })
            }
        })
        .catch(err => {
            console.log(err)
        })
        
    }

    render() {

        let master_data     = this.state.master_data.documents;

        let documents_info  = this.props.table_data;
        let documents       = documents_info.documents; 
        let claim_id;
        if(documents_info){
           claim_id        = documents_info.claim_detail.id;  
        }

        let columns = [
            {
                name: 'Document',
                selector: row => row.document,
                sortable: true
            },
            {
                name: 'Document Type',
                selector: row => row.document_type,
                sortable: true
            },
            {
                name: 'Description',
                selector: row => row.description,
            },
            {
                name: 'Download',
                selector: row => row.download,
            },
            {
                name: 'Action',
                selector: row => row.action,
                minWidth: "245px"
            }           
        ];

        let list_data = [];
        if(documents !== undefined){
            let document_list = Object.keys(documents); 
            let $this = this;
            document_list.forEach(function(key, i) {

                let document_name = documents[key]['document_configuration_id'][1];
                let download_url;
                let action_col;
                if(document_name){
                    download_url = 
                    <>
                        <span className="filter-field" style={{ backgroundColor: "unset",boxShadow: "none" }}>
                            <Link onClick={e => e.preventDefault()} className="clear-btn" onClick={() => $this.downloadDocs(claim_id, documents[key].id, documents[key].payment_line_id, documents[key].doc_attachment_type)}>Download</Link>
                        </span>
                    </>
                }
                if(document_name){

                    action_col = 
                    <>
                        <img src={Edit} alt='Edit' onClick={() => $this.editDocs(documents[key].id, documents[key].payment_line_id, i)}/>
                        <img src={Delete} alt='delete' onClick={() => {if(window.confirm('Are you sure to delete this record?')) $this.removeDocs(claim_id, documents[key].id, documents[key].payment_line_id) }}/>
                        {documents[key].doc_type !== "Supporting" &&
                            <a className="add-secondary" onClick={() => $this.addSecondaryDocs(documents[key].id, documents[key].payment_line_id)}>Add Secondary</a>
                        }
                    </>
                }

                list_data.push({ 
                    document: documents[key].doc_type, 
                    document_type: documents[key].document_configuration_id[1],
                    description: documents[key].description,
                    download: download_url,
                    action: action_col
                })

            });
        }

        
        return(
            <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} radius={12} color="#201e53">
                {this.state.success_message &&
                  <div className="success-message" style={{ color:"green"}}>{this.state.success_message}</div>
                }
                {this.state.errorMsg &&
                  <div className="form-validate-error" style={{ color:"red"}}>{this.state.errorMsg}</div>
                }
                <>

                    {list_data.length > 0 &&
                     
                        <DataTable
                            columns={columns}
                            data={list_data}
                            fixedHeader
                        />
                    
                    }

                    { list_data.length == 0 &&
                        <div className="document-table table-responsive">
                            <NoDataTable columns={columns} title="No Data Available" />
                        </div>
                    }
                </>
                
                <Modal className="add-document--modal white-background-modal" show={this.state.showEditModel} >
                    <Modal.Body>
                        <Button className="close" onClick={this.handleCloseEditDocument}>&times;</Button>
                        <div className="">
                        <h3 className="text-center mb-2">Edit Document</h3>
                            <i className="text-center d-block">{this.state.document_name} Document</i>
                        </div>
                        {this.state.errorMsg &&
                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.errorMsg}</div>
                        }
                        
                            <div className="row align-items-center mb-4">
                                <div className="col-md-4 col-12">
                                    <label>Document type<sup className="required">*</sup></label>
                                </div>
                                <div className="col-md-8 col-12">
                                    <select className="form-control" name="document_config_id" id="document_config_id" value={this.state.document_config_id} onChange={this.inputSelectHandler}>
                                        {master_data ? master_data.map((document, i) =>
                                            <>
                                            {document.doc_type === this.state.doc_type &&
                                                <option value={document.id} data-id={document.id} data-name={document.doc_type} key={i} >{document.name}</option>
                                            }
                                            </>
                                        ) : null}
                                    </select>
                                    {this.state.formValidateError0 &&
                                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError0}</div>
                                    }
                                </div>
                            </div>
                            <div className="row mb-4">
                                <div className="col-md-4 col-12">
                                    <label>Description</label>
                                </div>
                                <div className="col-md-8 col-12">
                                    <textarea className="form-control" cols="4" rows="4" name="description" value={this.state.description} onChange={this.inputHandler}></textarea>
                                </div>
                            </div>
                            <div className="row align-items-center mb-4">
                                <div className="col-md-4 col-12">
                                    <label>Upload Document</label>
                                </div>
                                <div className="col-md-8 col-12">
                                   <label className="file-attach" htmlFor='file'>
                                       <input type="file" id="file" name="document" onChange={this.onFileChange}/>
                                       <p className="text-uppercase">Choose file <FontAwesomeIcon icon={faFileUpload} /></p>
                                   </label>
                                   {this.state.fileName &&
                                        <p className="uploading-file-name">{this.state.fileName}</p>
                                    }
                                   {this.state.formValidateError2 &&
                                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError2}</div>
                                    }
                                    {this.state.formValidateError3 &&
                                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError3}</div>
                                    }
                                </div>
                            </div>
                            <button type="submit" className="btn-signin" onClick={this.submitEditDocs} disabled={this.state.isDisabled}>Submit </button>
                        
                    </Modal.Body>
                </Modal>

                <Modal className="add-document--modal white-background-modal" show={this.state.showSecModel} >
                    <Modal.Body>
                        <Button className="close" onClick={this.handleCloseSecDocument}>&times;</Button>
                        {this.state.errorMsg &&
                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.errorMsg}</div>
                        }
                        <div className="">
                            <h3 className="text-center mb-2">Add Document</h3>
                            <i className="text-center d-block">Secondary Document</i>
                        </div>
                        
                            <div className="row align-items-center mb-4">
                                <div className="col-md-4 col-12">
                                    <label>Document Type<sup className="required">*</sup></label>
                                </div>
                                <div className="col-md-8 col-12">
                                    <select className="form-control" name="document_config_id" id="document_config_id" value={this.state.document_config_id} onChange={this.inputSelectHandler}>
                                        <option value="">Please select</option>
                                        {master_data ? master_data.map((document, i) =>
                                            <>
                                            {document.doc_type === "supporting" &&
                                                <option value={document.id} key={i} data-id={document.id} data-name={document.doc_type}>{document.name}</option>
                                            }
                                            </>
                                        ) : null}
                                    </select>
                                    {this.state.formValidateError0 &&
                                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError0}</div>
                                    }
                                </div>
                            </div>
                            <div className="row mb-4">
                                <div className="col-md-4 col-12">
                                    <label>Description</label>
                                </div>
                                <div className="col-md-8 col-12">
                                    <textarea className="form-control" cols="4" rows="4" name="description" value={this.state.description} onChange={this.inputHandler}></textarea>
                                </div>
                            </div>
                            <div className="row align-items-center mb-4">
                                <div className="col-md-4 col-12">
                                    <label>Upload Document</label>
                                </div>
                                <div className="col-md-8 col-12">
                                   <label className="file-attach" htmlFor='file'>
                                       <input type="file" id="file" name="document" onChange={this.onFileChange}/>
                                       <p className="text-uppercase">Choose file <FontAwesomeIcon icon={faFileUpload} /></p>
                                   </label>
                                   {this.state.fileName &&
                                        <p className="uploading-file-name">{this.state.fileName}</p>
                                    }
                                   {this.state.formValidateError2 &&
                                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError2}</div>
                                    }
                                    {this.state.formValidateError3 &&
                                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError3}</div>
                                    }
                                </div>
                            </div>
                            <button type="submit" className="btn-signin" onClick={this.submitSecondaryDocs} disabled={this.state.isDisabled}>Submit</button>
                        
                    </Modal.Body>
                </Modal>
                <ToastContainer />
           </Loader>
        )
    }
}
export default DocumentTable;