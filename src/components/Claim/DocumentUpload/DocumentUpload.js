import React, { Component } from 'react';
import DocumentTable from './DocumentTable';
import AddDocument from './AddDocument';
import Arrow from './../../../assets/images/arrow.svg';
import './document-upload.scss';
import './add-document.scss';
import axios from 'axios';
import { Redirect } from 'react-router';
import { Link } from "react-router-dom";

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import File from './../../../assets/images/otp.png';
import './add-document.scss';
import Loader from 'react-loader'
import ReactTooltip from 'react-tooltip';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faFileUpload } from '@fortawesome/free-solid-svg-icons'

let api_url = process.env.REACT_APP_API_URL1;

class DocumentUpload extends Component {
    intervalID;
    constructor(props) {
        super(props);
        this.state = {
            showModel: false,
            errorMsg: '',
            goBack: false,
            goNext: false,
            master_data: '',
            myclaim_detail_data: '',
            claim_id: '',
            payment_line_id: '',
            document_id: '',
            parent_id: '',
            document_config_id: '',
            description: '',
            document: '',
            doc_type: '',
            document_name: '',
            agreement_type_details: '',
            agreement_type_id: '',
            formValidateError0: '',
            formValidateError1: '',
            formValidateError2: '',
            formValidateError3: '',
            partner_id: '',
            agreement_errorMsg: false,
            document_errorMsg: false,
            loaded: false,
            agreement_type_ids: false,
            success_message: false,
            help: '',
            fileName: '',
            isDisabled: false
        };
        
        this.openAddDocument            = this.openAddDocument.bind(this);
        this.handleCloseAddDocument     = this.handleCloseAddDocument.bind(this);

    }

    componentDidMount(){

        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');

        if(token !== '' && user_id !== ''){

            let claim_id = '';
            if(localStorage.getItem("claim_id")){
                claim_id = localStorage.getItem("claim_id");
            }

            this.fileclaimMaster(user_id, token)
            this.myclaimDetail(user_id, token, claim_id)
        }

    }

    /*componentDidUpdate(){

        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        let claim_id
        if(localStorage.getItem("claim_id")){
            claim_id = localStorage.getItem("claim_id");
        }

        if(token !== '' && user_id !== '' && claim_id !== ''){
            this.myclaimDetail(user_id, token, claim_id)
        }
    }*/

    documentUploaded = () => {
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        let claim_id
        if(localStorage.getItem("claim_id")){
            claim_id = localStorage.getItem("claim_id");
        }

        if(token !== '' && user_id !== '' && claim_id !== ''){
            this.myclaimDetail(user_id, token, claim_id)
        }
    }

    componentWillUnmount() {
        /*
          stop myclaimDetail() from continuing to run even
          after unmounting this component
        */
        clearTimeout(this.intervalID);
    }

    openAddDocument(e) {
        this.setState({
            showModel: true
        })
    }

    handleCloseAddDocument(e) {
        this.setState({
            showModel: false,
            description: '',
            document_config_id: '',
            document: '',
            fileName: '',
        })
    }

    inputHandler = (event) =>{

        this.setState ({ [event.target.name] : event.target.value })
    }

    inputSelectHandler = (event) =>{

        var el          = document.getElementById("document_config_id");
        var document_config_id    = el.options[el.selectedIndex].getAttribute("data-id");
        var document_name    = el.options[el.selectedIndex].getAttribute("data-name");
        
        this.setState({ 
            [event.target.name] : event.target.value, 
            document_config_id : document_config_id,
            document_name : document_name
        });

    }

    checkboxHandler = (event) =>{

        this.setState ({ [event.target.name] : event.target.value })

    }

    onFileChange = event => {
        this.setState({ 
            document: event.target.files[0],
            fileName: event.target.files[0].name
        });
    };

    // get fileclaim Master
    fileclaimMaster = (user_id, token, claim_id) => {

        let fileclaim_request = { 
            "user_id": parseInt(user_id),
            "access_token": token
        }
        
        axios.post(api_url+'/api/v1/fileclaim/master', fileclaim_request, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data

            if(result.success === true){

                let document_name;                
                let doc_type;                
                let agreement_type_id;                
                let document_config_id;                
                let documents              = res.data.documents;
                let agreement_type_details = res.data.agreement_type_details;
                if(documents && documents.length){
                    document_name   = documents[0]['name']
                    doc_type        = documents[0]['doc_type']
                    document_config_id        = documents[0]['id']
                }
                if(agreement_type_details && agreement_type_details.length){
                    agreement_type_id  = agreement_type_details[0]['id']
                }
                this.setState({ 
                    master_data: res.data,
                    agreement_type_details: agreement_type_details,
                    help: res.data.help,
                    loaded: true
                    /*agreement_type_id: agreement_type_id
                    document_name: document_name,
                    doc_type: doc_type,
                    document_config_id: document_config_id*/
                })
                
            }else{
                /*this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                    setTimeout(() => this.setState({ errorMsg: false }), 5000);
                })*/
                toast.error('Somthing went wrong!!', {
                    position: "top-right",
                    autoClose: 3000,
                    hideProgressBar: false,
                    closeOnClick: true,
                    pauseOnHover: true,
                    draggable: true,
                    progress: undefined,
                });
            }
        })
        .catch(err => {
            console.log(err)
        })
        
    }

    // submit asset details
    docsUpload = (event) => {
        event.preventDefault();
        let access_token        = localStorage.getItem('token');
        let user_id             = localStorage.getItem('user_id');
        
        let claim_id            =  localStorage.getItem("claim_id"); 
        let payment_line_id     =  this.state.payment_line_id ? this.state.payment_line_id : ''; 
        let parent_id           =  this.state.parent_id ? this.state.parent_id : 0;
        let document_config_id  =  this.state.document_config_id ? this.state.document_config_id : '';  
        let description         =  this.state.description ? this.state.description : '';
        let documents           =  this.state.document ? this.state.document : '';  
        let document_name       =  this.state.document_name ? this.state.document_name : '';  
        let doc_type            =  this.state.document_name ? this.state.document_name : '';  
        let partner_id          =  localStorage.getItem("partner_id");  
        
        let document_id         =  this.state.document_id ? this.state.document_id : '';  
        
        if(document_name && doc_type && documents){
            if (documents.name.match(/\.(jpg|jpeg|png|pdf|JPG|JPEG|PNG|PDF)$/)) {

                this.setState({
                    loaded: false
                })

                const formdata = new FormData();
                formdata.append('user_id', parseInt(user_id))
                formdata.append('access_token', access_token)
                formdata.append('claim_id', claim_id)
                formdata.append('payment_line_id', payment_line_id)
                formdata.append('document_id', document_id)
                formdata.append('parent_id', parent_id)
                formdata.append('document_configuration_id', document_config_id)
                formdata.append('description', description)
                formdata.append('document', documents)
                formdata.append('document_name', document_name)
                formdata.append('doc_type', doc_type.toString().toLowerCase())
                
                axios.post(api_url+'/api/v1/upload/document', formdata, {
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                })
                .then( res => {
                    let result = res.data
                    if(result.success === true){
                        this.setState({
                            claim_id: res.data.claim_id,
                            payment_line_id: res.data.payment_line_id,
                            document_id: res.data.document_id,
                            showModel: false,
                            description: '',
                            document_config_id: '',
                            fileName: '',
                            loaded: true,
                            isDisabled: false
                        })

                        /*this.setState({ success_message: "successfully added." }, () => {
                            setTimeout(() => this.setState({ success_message: false }), 5000);
                        })*/
                        toast.success('Successfully added.', {
                            position: "top-right",
                            autoClose: 3000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                        this.myclaimDetail(user_id, access_token, claim_id);
                    }else{
                        /*this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                            setTimeout(() => this.setState({ errorMsg: false }), 5000);
                        })*/
                        toast.error('Somthing went wrong!!', {
                            position: "top-right",
                            autoClose: 3000,
                            hideProgressBar: false,
                            closeOnClick: true,
                            pauseOnHover: true,
                            draggable: true,
                            progress: undefined,
                        });
                    }
                })
                .catch(err => {
                    console.log(err)
                })

            }else{
                this.setState({ formValidateError3: "Only jpg, jpeg, png, pdf files are allowed!" }, () => {
                    setTimeout(() => this.setState({ formValidateError3: false }), 5000);
                })
            }
        }
        else {
            if(document_name == ''){
                this.setState({ formValidateError0: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formValidateError0: false }), 5000);
                })
            }
            if(doc_type == ''){
                this.setState({ formValidateError1: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formValidateError1: false }), 5000);
                })
            }
            if(documents == ''){
                this.setState({ formValidateError2: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formValidateError2: false }), 5000);
                })
            }
        }
            
    }

    // get documents with id
    getDocumentByID = (e) => {
        e.preventDefault();
        let access_token        = localStorage.getItem('token');
        let user_id             = localStorage.getItem('user_id');

        let claim_id            =  localStorage.getItem("claim_id"); 
        let payment_line_id     =  this.state.payment_line_id ? this.state.payment_line_id : 0; 
        let partner_id          =  0;

        let document_config_id  = this.state.document_config_id ? this.state.document_config_id : ''; 
        let documents           = this.state.document ? this.state.document : '';  
        let document_name       = this.state.document_name ? this.state.document_name : '';  
        let doc_type            = this.state.document_name ? this.state.document_name : '';

        let request = { 
            "user_id": parseInt(user_id),
            "access_token": access_token,
            "claim_id": parseInt(claim_id),
            "partner_id": parseInt(partner_id),
            "payment_line_id": parseInt(payment_line_id)
        }
        if(document_name && doc_type && document_config_id && documents){
            this.setState({
                isDisabled: true
            });

            axios.post(api_url+'/api/v1/get/document/id', request, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then( res => {
                let result = res.data

                if(result.success === true){
                    this.setState({ 
                        document_id: res.data.document_id,
                        payment_line_id: res.data.payment_line_id,
                        partner_id: res.data.partner_id
                    }, () => {
                        this.docsUpload(e);
                    })
                    
                }else{
                    this.setState({ errorMsg: "Somthing went wrong!!", isDisabled: false }, () => {
                        setTimeout(() => this.setState({ errorMsg: false }), 5000);
                    })
                }
            })
            .catch(err => {
                console.log(err)
            })
        }
        else {
            if(document_name == ''){
                this.setState({ formValidateError0: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formValidateError0: false }), 5000);
                })
            }
            if(doc_type == ''){
                this.setState({ formValidateError1: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formValidateError1: false }), 5000);
                })
            }
            if(documents == ''){
                this.setState({ formValidateError2: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formValidateError2: false }), 5000);
                })
            }
        }    
        
    }

    // Go to previous screen
    goBack = (e) => {
        e.preventDefault();
        this.setState({
            goBack: true
        });
    }

    // fileclaim step 3 submit
    fileclaimStep3Submit = (event) => {
        event.preventDefault();

        let access_token        = localStorage.getItem('token');
        let user_id             = localStorage.getItem('user_id');
        
        let claim_id            =  localStorage.getItem('claim_id'); 
        let partner_id          =  localStorage.getItem('partner_id'); 
        //let payment_line_id     =  localStorage.getItem('payment_line_id'); 
        let agreement_type_id   =  this.state.agreement_type_id ? this.state.agreement_type_id : '';  
        let payment_line_id     =  this.state.payment_line_id ? this.state.payment_line_id : 0;
        
        let fileclaim_step3 = {
            "user_id": parseInt(user_id),
            "access_token": access_token,
            "claim_id": parseInt(claim_id),
            "partner_id": parseInt(partner_id),
            "agreement_type_id": parseInt(agreement_type_id),
            "payment_line_id": parseInt(payment_line_id)
        }
        //let check_documents = this.state.myclaim_detail_data.documents.length;
        if(agreement_type_id){

            this.setState({
                loaded: false
            })
            
            axios.post(api_url+'/api/v1/fileclaim/step3', fileclaim_step3, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then( res => {
                let result = res.data
                
                if(result.success === true){
                    this.setState({
                        goNext: true,
                        claim_id: result.claim_id,
                        payment_line_id: result.payment_line_id,
                        //document_id: 12,
                        loaded: true
                    })

                    localStorage.setItem('payment_line_id', this.state.payment_line_id);

                }else{
                    this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                        setTimeout(() => this.setState({ errorMsg: false }), 3000);
                    })
                }
            })
            .catch(err => {
                console.log(err)
            })
        }else{
            /*if(agreement_type_id === ''){
                this.setState({ agreement_errorMsg: "Please select agreement." }, () => {
                    setTimeout(() => this.setState({ agreement_errorMsg: false }), 3000);
                })

                window.scrollTo(0, 0)
            }*/

            toast.error('Please select agreement.', {
                position: "top-right",
                autoClose: 2000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });

            /*if(this.state.document_id === ''){
                this.setState({ document_errorMsg: "Please add documents." }, () => {
                    setTimeout(() => this.setState({ document_errorMsg: false }), 3000);
                })
            }*/
        }

    }

    // get document list with claim id
    myclaimDetail = (user_id, token, claim_id) => {

        let fileclaim_request = { 
            "user_id": parseInt(user_id),
            "access_token": token,
            "claim_id": parseInt(claim_id)
        }

        axios.post(api_url+'/api/v1/myclaim/detail', fileclaim_request, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data
            
            if(result.success === true){
                //debugger
                this.setState({ 
                    myclaim_detail_data: res.data,
                    agreement_type_ids: res.data.claim_detail,
                    loaded: true,
                    agreement_type_id: res.data.claim_detail.agreement_type_id
                })
                
            }else{
                this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                    setTimeout(() => this.setState({ errorMsg: false }), 5000);
                })
            }

            //this.intervalID = setTimeout(this.myclaimDetail.bind(this), 1000);
        })
        .catch(err => {
            console.log(err)
        })
        
    }

    render() {

        if(this.state.goBack){
            return(
                <Redirect to="/claim-info" />
            )
        }

        if(this.state.goNext){
            return(
                <Redirect to="/respondent-info" />
            )
        }

        if(!localStorage.getItem('token')){
            return(
                <Redirect to="/" />
            )
        }

        let documents           = this.state.master_data.documents;
        let documents_info      = this.state.myclaim_detail_data;
        let agreement_type_data = this.state.agreement_type_details;
        let agreement_type_id   = this.state.agreement_type_ids.agreement_type_id;
     
        //debugger
        let checked;
        /*if(agreement_type_id){
            checked = "checked"
        }else{
            checked = ""
        }*/
        return(
            <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} radius={12} top="100%" color="#201e53">
               <div className="document-upload">
                    {this.state.agreement_errorMsg &&
                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.agreement_errorMsg}</div>
                    }
                    {this.state.document_errorMsg &&
                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.document_errorMsg}</div>
                    }
                    <h1 data-tip data-for='agreement'>Select Agreement</h1>
                    {this.state.help.agreement &&
                        <ReactTooltip id='agreement' >
                            <span>{this.state.help.agreement}</span>
                        </ReactTooltip>
                    }
                    <ul>
                    {
                        agreement_type_data !== undefined && agreement_type_data.length ? (
                            <>{
                                agreement_type_data ? agreement_type_data.map((agreement_type_item, index) =>
                                    
                                    <li key={index}>
                                        <label className='checkbox radiobox'>{agreement_type_item.name}
                                            <input type="radio" name='agreement_type_id' value={agreement_type_item.id} onChange={this.inputHandler} />
                                            <span className={"checkmark "+(this.state.agreement_type_id === agreement_type_item.id ? "checked" : '')}></span>
                                        </label>
                                    </li>
                                ) : null
                            }</>
                        ) : (
                            <li>No Data found.</li>
                        )
                    }
                    </ul>
                    
                    <div className='row'>
                        <div className="col-md-6 col-6">
                            <h2 data-tip data-for='upload_documents'>Document Upload</h2>
                            {this.state.help.upload_documents &&
                                <ReactTooltip id='upload_documents' >
                                    <span>{this.state.help.upload_documents}</span>
                                </ReactTooltip>
                            }
                        </div>
                        <div className="col-md-6 col-6 text-right">
                            <Link onClick={e => e.preventDefault()} className="add-primary" onClick={this.openAddDocument}>Add Primary</Link>
                        </div>
                    </div>
                    {this.state.success_message &&
                      <div className="success-message" style={{ color:"green"}}>{this.state.success_message}</div>
                    }
                    <DocumentTable table_data={documents_info} documentUploadcallback={this.documentUploaded} />

                   <div className="text-center">
                        <button type="button" className="back-btn" onClick={this.goBack}><img src={Arrow} alt='arrow'/>Back</button>
                        <button type="button" className="next-btn" onClick={this.fileclaimStep3Submit}>Next <img src={Arrow} alt='arrow' /></button>
                    </div>

                    <Modal className="add-document--modal white-background-modal" show={this.state.showModel} >
                        <Modal.Body>
                            <Button className="close" onClick={this.handleCloseAddDocument}>&times;</Button>
                            <div className="">
                                <h3 className="text-center mb-2">Add Document</h3>
                                <i className="text-center d-block">Primary Document</i>
                            </div>
                            {this.state.errorMsg &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.errorMsg}</div>
                            }
                            <form onSubmit = {this.getDocumentByID}>
                                <div className="row align-items-center mb-4">
                                    <div className="col-md-4 col-12">
                                        <label>Document Type<sup className="required">*</sup></label>
                                    </div>
                                    <div className="col-md-8 col-12">
                                        <select className="form-control" name="document_config_id" id="document_config_id" value={this.state.document_config_id} onChange={this.inputSelectHandler}>
                                            <option value="">Please select</option>
                                            {documents ? documents.map((document, j) =>
                                                <>
                                                {document.doc_type === "primary" &&
                                                    <option value={document.id} key={j} data-id={document.id} data-name={document.doc_type}>{document.name}</option>
                                                }
                                                </>
                                            ) : null}
                                        </select>
                                        {this.state.formValidateError1 &&
                                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError1}</div>
                                        }
                                    </div>
                                </div>
                                <div className="row mb-4">
                                    <div className="col-md-4 col-12">
                                        <label>Description</label>
                                    </div>
                                    <div className="col-md-8 col-12">
                                        <textarea className="form-control" cols="4" rows="4" name="description" value={this.state.description} onChange={this.inputHandler}></textarea>
                                    </div>
                                </div>
                                <div className="row align-items-center mb-4">
                                    <div className="col-md-4 col-12">
                                        <label>Upload Document</label>
                                    </div>
                                    <div className="col-md-8 col-12">
                                       <label className="file-attach" htmlFor='file'>
                                           <input type="file" id="file" name="document" onChange={this.onFileChange}/>
                                           <p className="text-uppercase">Choose file <FontAwesomeIcon icon={faFileUpload} /></p>
                                       </label>                                       
                                       {this.state.fileName &&
                                            <p className="uploading-file-name">{this.state.fileName}</p>
                                        }
                                       {this.state.formValidateError2 &&
                                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError2}</div>
                                        }
                                        {this.state.formValidateError3 &&
                                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError3}</div>
                                        }
                                    </div>
                                </div>
                                <button type="submit" className="btn-signin" disabled={this.state.isDisabled}>Submit</button>
                            </form>
                        </Modal.Body>
                    </Modal>

               </div>
               <ToastContainer />
            </Loader>
        )
    }
}
export default DocumentUpload;