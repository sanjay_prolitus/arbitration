import React from 'react';
import Edit from './../../../assets/images/edit.png';
import Delete from './../../../assets/images/delete.png';
import './attorney-table.scss';

const AttorneyTable = () =>{
    return(
       <div className="document-table table-responsive">
            <table className="table table-bordered">
                <thead>
                    <tr>
                        <th><span>Respondent Name</span></th>
                        <th><span>Email</span></th>
                        <th><span>Action</span></th>
                    </tr>
                </thead>
                <tbody>
                    <tr>
                        <td>Ron</td>
                        <td>ron@gmail.com</td>
                        <td>
                            <img src={Edit} alt='Edit'/>
                            <img src={Delete} alt='delete'/>
                        </td>
                    </tr>
                    <tr>
                        <td>Andrew</td>
                        <td>andrew@gmail.com</td>
                        <td>
                            <img src={Edit} alt='Edit'/>
                            <img src={Delete} alt='delete'/>
                        </td>
                    </tr>
                </tbody>
            </table>
       </div>
    )
}
export default AttorneyTable;