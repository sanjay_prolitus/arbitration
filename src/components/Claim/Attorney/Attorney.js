import React from 'react';
import AttorneyTable from './AttorneyTable';
import Arrow from './../../../assets/images/arrow.svg';
import UserPlus from './../../../assets/images/user-blue.png';
import './attorney.scss';

const Attorney = () =>{
    return(
       <div className="attorney">            
            <div className='row'>
                <div className="col-12 text-right">
                    <a href="javascript" className="add-respondent"><img src={UserPlus} alt='user'/>Add Attorney</a>
                </div>
            </div>
            <AttorneyTable/>
           <div className="text-center">
                <button type="button" className="back-btn"><img src={Arrow} alt='arrow'/>Back</button>
                <button type="button" className="next-btn">Next <img src={Arrow} alt='arrow'/></button>
            </div>
       </div>
    )
}
export default Attorney;