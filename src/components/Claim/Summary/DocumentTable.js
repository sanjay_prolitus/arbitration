import React, { Component } from 'react';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import { Redirect } from 'react-router';
import Excel from './../../../assets/images/excel.png';
import Pdf from './../../../assets/images/pdf.png';
import './document-table.scss';
import { Link } from "react-router-dom";
import NoDataTable from '../../Common/NoDataTable';

let api_url = process.env.REACT_APP_API_URL1;

class DocumentTable extends Component{

    constructor(props) {
        super(props);
        this.state = {
            documentList : '',
            document_list : ''
        };

    }

    componentDidMount(){
        
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        
        if(token !== '' && user_id !== ''){
            this.getDocumentList(user_id, token)
        }
    }

    /*getDocumentData = (event) => {
        event.preventDefault();

        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');

        if( user_id && token ){
            let document_data_request = { 'user_id': parseInt(user_id), 'access_token': token, 'claim_id': parseInt(localStorage.getItem('claim_id')), 'document_id': 4, 'payment_line_id' : parseInt(localStorage.getItem('payment_line_id')) };

            axios.post(api_url+'/api/v1/document/download', document_data_request, {
                headers: { 
                    'Content-Type': 'text/plain'
                }
            })
            .then( response => {
                if(response){
                    alert("Successfull");
                }else{
                    alert("Failed");
                }
            })
        }
    }*/

    // Download Docs 
    downloadDocs = (document_id, payment_line_id, doc_attachment_type ) => {
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        let claim_id        = localStorage.getItem('claim_id');

        let get_file_detail = doc_attachment_type.split('/');
        let type = get_file_detail[0];
        let ext  = get_file_detail[1];

        let file_request    = {
            'user_id': parseInt(user_id), 
            'access_token': token, 
            'claim_id': parseInt(claim_id), 
            'document_id': document_id, 
            'payment_line_id': payment_line_id
        }

        axios.post(api_url+'/api/v1/document/download', file_request, {
            //responseType: 'blob',
            headers: {
                'Content-Type': 'application/'+type+'/*'
            }
        })
        .then(res => {
            if(res.data.success === true){
                let document_url = res.data.document_url;
                window.open(api_url+document_url, '_blank');
            }
        })
        
    }

    // get asset details
    getDocumentList = (user_id, token) => {
        if(user_id){
            let document_request    = { 
                'user_id': parseInt(user_id), 
                'access_token': token, 
                'claim_id': parseInt(localStorage.getItem('claim_id')) 
            }
            
            axios.post(api_url+'/api/v1/myclaim/detail', document_request, {
                headers: { 
                    'Content-Type': 'text/plain'
                }
            })
            .then(res => {
                if(res.data.status !== 'true'){
                    this.setState({
                        document_list: res.data,
                        loading: true
                    })
                }else{

                }
            })
        }
    }


    render(){

        if(!localStorage.getItem('token')){
            <Redirect to="/" />
        }

        //let documentList = this.state.document_list.documents;
        let documentList = this.props.documentsData;

        let columns = [
            {
                name: 'Document',
                selector: row => row.document,
                sortable: true
            },
            {
                name: 'Document Type',
                selector: row => row.document_type,
                sortable: true
            },
            {
                name: 'Description',
                selector: row => row.description,
            },
            {
                name: 'Download',
                selector: row => row.download,
            }           
        ];

        let list_data = [];
        if(documentList !== undefined){
            let document_list = Object.keys(documentList); 
            let $this = this;
            document_list.forEach(function(key, i) {

                let document_name = documentList[key]['document_configuration_id'][1];
                let download_url;
                let action_col;
                if(document_name){
                    download_url = 
                    <>
                        <span className="filter-field" style={{ backgroundColor: "unset",boxShadow: "none" }}>
                            <Link onClick={e => e.preventDefault()} className="clear-btn" onClick={() => $this.downloadDocs(documentList[key].id, documentList[key].payment_line_id, documentList[key].doc_attachment_type)}>Download</Link>
                        </span>
                    </>
                }

                list_data.push({ 
                    document: documentList[key].doc_type, 
                    document_type: documentList[key].document_configuration_id[1],
                    description: documentList[key].description,
                    download: download_url
                })

            });
        }

        return(
           <>
                {list_data.length > 0 &&
                    <DataTable
                        columns={columns}
                        data={list_data}
                        fixedHeader
                    />
                }

                { list_data.length == 0 &&
                    <div className="document-table table-responsive">
                        <NoDataTable columns={columns} title="No Data Available" />
                    </div>
                }  
           </>
        )
    }
}
export default DocumentTable;