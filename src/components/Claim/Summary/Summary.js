import React, {Component, useEffect, useState} from 'react';
import DocumentTable from './DocumentTable';
import RespondentTable from './RespondentTable';
import axios from 'axios';
import { Redirect } from 'react-router';
import Arrow from './../../../assets/images/arrow.svg';
import Info from './../../../assets/images/info.png';
import './summary.scss';
import Loader from 'react-loader'

import PaymentSuccessAlert from '../../Alert/PaymentSuccessAlert';
import './paymentalert.scss';

import StripePayment from '../../Payment/StripePayment';

let api_url = process.env.REACT_APP_API_URL1;
let payment_method = process.env.REACT_APP_PAYMENT_STRIPE_METHOD;

class Summary extends Component{

    constructor(props) {
        super(props);
        this.state = {
            claimant_name: '',
            claimDetails: '',
            payment_acknowledgement: '',
            claimantName: '',
            claimantAmount: 0,
            fillingFee: 0,
            claimMaster: '',
            claimantInfoMaster: '',
            respondentData: '',
            documentsData: '',
            claimSummary: '',
            goBack: false,
            client_secret: '',
            payment_transaction_id: '',
            amount: '',
            payment_reference: '',
            goToPayment: false,
            goToStripePayment: false,
            errormessage: '',
            loaded: false
        };
    }

    componentDidMount(){
        
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        
        if(token !== '' && user_id !== ''){
            let claim_id = '';
            if(localStorage.getItem("claim_id")){
                claim_id = localStorage.getItem("claim_id");
            }
            this.getClaimSummary(user_id, token, claim_id)
        }

    }

    // Go to previous screen
    goBack = (e) => {
        e.preventDefault();
        this.setState({
            goBack: true
        });
    }

    getClaimSummary = (user_id, token, claim_id) => {
        if(user_id){

            let claimant_info = { 'user_id': parseInt(user_id), 'access_token': token }
            
            axios.post(api_url+'/api/v1/fileclaim/master', claimant_info, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then(res => {
                if(res.data.status !== 'true'){
                    this.setState({
                        claimantInfoMaster: res.data,
                        payment_acknowledgement: res.data.help.payment_acknowledgement,
                        loading: true
                    }, () => {
                        setTimeout(() => this.setState({ loaded: true }), 10000);
                    })
                }else{

                }
            });


            let claim_summary = { 'user_id': parseInt(user_id), 'access_token': token, 'claim_id' : parseInt(claim_id) };
            
            axios.post(api_url+'/api/v1/myclaim/detail', claim_summary, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then(res => {
                
                if(res.data.success === true){

                    this.setState({
                        claimSummary: res.data,
                        claimantName: res.data.claim_detail.claimant_name,
                        claimantAmount: res.data.claim_detail.claim_amount,
                        fillingFee: res.data.claim_detail.filling_fee,
                        respondentData: res.data.respondent,
                        documentsData: res.data.documents,
                        loading: true,
                        loaded: true
                    })
                }else{

                }
            })
        }
    }

    payEFile = (event) => {
        event.preventDefault();
        localStorage.removeItem('payment_from')
        if(payment_method === "ON"){
            this.setState({
                goToStripePayment: true,
                loaded: false
            });

        }else{
            this.setState({
                goToPayment: true
            });
        }
        
    } 

    render() {

        if(this.state.goToPayment){
            return(
                <Redirect to="/payment" />
            )
        }
        /*if(this.state.goToStripePayment){
            return(
                <Redirect to="/payment" />
            )
        }*/
        if(this.state.goBack){
            return(
                <Redirect to="/respondent-info" />
            )
        }

        if(!localStorage.getItem('token')){
            <Redirect to="/" />
        }

        if (this.state.payment_status === "cancel" ) {
            return (
                <Redirect to="/claim-summary" />
            )
        }

        let ans = '';
        let claimDetails = this.state.claimSummary;
        let payment_Acknowledgement = this.state.payment_acknowledgement;
        let claimant_name   = this.state.claimantName;
        let claim_amount    = this.state.claimantAmount;
        let filling_fee     = this.state.fillingFee;
        let respondentData  = this.state.respondentData;
        let documentsData   = this.state.documentsData;

        let fee_amount      = localStorage.getItem('fee_amount');
        localStorage.setItem('claimant_name', claimant_name);

        return(
            <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} radius={12} top="100%" color="#201e53">
               <div className="summary">
                    <div className='row'>
                        <div className="col-md-6 col-12">
                            <ul className='summary-list'>
                                <li>
                                    <h6>Claimant</h6>
                                    <p>{claimant_name}</p>
                                </li>
                                <li>
                                    <h6>Amount of Claim</h6>
                                    <p>{claim_amount + " USD"}</p>
                                </li>
                                <li>
                                    <h6>Filling Fee</h6>
                                    <p>{fee_amount + " USD"}</p>
                                </li>
                            </ul>
                        </div>
                        <div className="col-md-6 col-12">
                            <div className="acknowledgement">
                                <img src={Info} alt="Info"/>
                                <div>
                                    <h5>Payment Acknowledgement</h5>
                                    <p>{ payment_Acknowledgement }</p>
                                </div>
                            </div>
                        </div>
                    </div>
                    <h1>Respondent</h1>
                    <div className='row'>
                        <div className="col-md-6 col-12">
                            <RespondentTable respondentData={respondentData}/>
                        </div>
                        <div className="col-md-6 col-12"></div>
                    </div>
                    <h2>Document Upload</h2>
                    <DocumentTable documentsData={documentsData}/>
                    <form onSubmit = {this.payEFile} >
                        <div className="text-center">
                            <a href="" className="back-btn" onClick={this.goBack}><img src={Arrow} alt='arrow'/>Back</a>
                            <button type="submit" className="next-btn">Pay and E-file Now</button>
                        </div>
                    </form>
               </div>

                {this.state.message &&
                    <PaymentSuccessAlert paymentsuccess={this.state.message} />
                }

                {this.state.goToStripePayment &&
                    <StripePayment />
                }
            </Loader>
        )
    }
}
export default Summary;