import React, { Component, useEffect, useState } from 'react';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import { Redirect } from 'react-router';
import './document-table.scss';
import NoDataTable from '../../Common/NoDataTable';

let api_url = process.env.REACT_APP_API_URL1;

class RespondentTable extends Component{

    constructor(props){
        super(props);
        this.state = {
            respondentDataResponse: ''
        };
    }

    componentDidMount(){
        
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        
        if(token !== '' && user_id !== ''){
            this.getRespondentData(user_id, token)
        }
    }

    getRespondentData = (user_id, token) => {
        if(user_id){
            let respondent_data = { 'user_id': parseInt(user_id), 'access_token': token, 'claim_id' : parseInt(localStorage.getItem('claim_id')) };
            
            axios.post(api_url+'/api/v1/myclaim/detail', respondent_data, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then(res => {
                if(res.data.status !== 'true'){
                    this.setState({
                        respondentDataResponse: res.data,
                        loading: true
                    })
                }else{

                }
            })
        }
    }

    render(){

        if(!localStorage.getItem('token')){
            <Redirect to="/" />
        }

        //let respondentData = this.state.respondentDataResponse.respondent;
        let respondentData = this.props.respondentData;

        let columns = [
            {
                name: 'Respondent Name',
                selector: row => row.name,
                sortable: true
            },
            {
                name: 'Email',
                selector: row => row.email,
                sortable: true
            }         
        ];

        let list_data = [];
        if(respondentData !== undefined){
            let respondent_list = Object.keys(respondentData); 
            respondent_list.forEach(function(key, i) {

                let respondent_name = respondentData[key].name;

                list_data.push({ 
                    name: respondentData[key].name, 
                    email: respondentData[key].email
                })

            });
        }

        return(
           <>
                {list_data.length > 0 &&
                    <DataTable
                        columns={columns}
                        data={list_data}
                        fixedHeader
                    />
                }

                { list_data.length == 0 &&
                    <div className="document-table table-responsive">
                        <NoDataTable columns={columns} title="No Data Available" />
                    </div>
                }
           </>
        )
    }
}
export default RespondentTable;