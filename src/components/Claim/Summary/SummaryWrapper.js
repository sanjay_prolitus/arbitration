import React, { useState } from 'react';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Accordion from 'react-bootstrap/Accordion';
import '../claim.scss';
import Summary from './Summary';


const SummaryWrapper = () =>{

    const [activeButton,setActiveButton] = useState(false);

    const handleAccordion = (e) =>{
        if(e.target.value === "Claim Summary"){         
            setActiveButton(!activeButton)
        }
    }

    return(
       <div className="container claim-container">
           <div className="row">
               <div className="col-12 d-md-block d-none">
                   <Tabs className='claim-tabs' defaultActiveKey="Claim Summary">
                        <Tab eventKey="claimant Information" title="Claimant Information" disabled="disabled">
                            
                        </Tab>
                        <Tab eventKey="Claim Information" title="Claim Information" disabled="disabled">
                            
                        </Tab>
                        <Tab eventKey="Document Upload" title="Document Upload" disabled="disabled">
                            
                        </Tab>
                        <Tab eventKey="Respondent information" title="Respondent Information" disabled="disabled">
                            
                        </Tab>
                        <Tab eventKey="Claim Summary" title="Claim Summary">
                            <Summary/>
                        </Tab>
                    </Tabs>
               </div>
               <div className="col-12 d-md-none d-block">
                    <Accordion defaultActiveKey="0" className='claim-accordion'>
                        <Accordion.Toggle value="Claim Summary" className={activeButton === true ? 'accordion-button active-button' : 'accordion-button'} disabled="disabled" eventKey="1">Claim Summary</Accordion.Toggle>
                        <Accordion.Collapse eventKey="0">
                            <div className="tab-content">
                                <Summary/>
                            </div>
                        </Accordion.Collapse>
                    </Accordion>
               </div>
           </div>
       </div>
    )
}
export default SummaryWrapper;