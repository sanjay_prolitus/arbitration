import React, {Component, useEffect, useState} from 'react';
import axios from 'axios';
import { Redirect } from 'react-router';
import Arrow from './../../../assets/images/arrow.svg';
import './claim-info.scss';
import Loader from 'react-loader'
import ReactTooltip from 'react-tooltip';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

let api_url = process.env.REACT_APP_API_URL1;

class ClaimInfo extends Component{

    constructor(props) {
        super(props);
        this.state = {
            claimInfoMaster : '',
            goBack : false,
            goNext : false,
            claim_amount: '',
            equitable_relief_action: '',
            equitable_relief_action_description: '',
            formal_hearing: 'no',
            hearing_type_id: '',
            availability_of_hearing: '',
            claim_description: '',
            claim_id: '',
            errormessage: '',
            loaded: false,
            claim_info_error: false,
            help: ''
        };
    }

    componentDidMount(){
        
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        
        if(token !== '' && user_id !== ''){
            let claim_id = '';
            if(localStorage.getItem("claim_id")){
                claim_id = localStorage.getItem("claim_id");
            }
            this.getClaimInfo(user_id, token, claim_id)
        }
    }

    //  Claimant Information (Fileclaim Master)
    getClaimInfo = (user_id, token, claim_id) => {
        
        let claim_info_master = { 'user_id': parseInt(user_id), 'access_token': token }
        
        axios.post(api_url+'/api/v1/fileclaim/master', claim_info_master, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then(res => {
            if(res.data.status !== 'true'){

                this.setState({
                    claimInfoMaster: res.data,
                    help: res.data.help,
                    loading: true,
                    loaded: true
                })
            }else{

            }
        })

        if(claim_id){ 
            let claim_info = { 'user_id': parseInt(user_id), 'access_token': token, 'claim_id': parseInt(claim_id) }
            
            axios.post(api_url+'/api/v1/myclaim/detail', claim_info, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then(res => {

                if(res.data.success === true){
                    this.setState({
                        claim_amount: res.data.claim_detail.claim_amount,
                        equitable_relief_action: res.data.claim_detail.equitable_relief_action_required,
                        formal_hearing: res.data.claim_detail.formal_hearing,
                        hearing_type_id: res.data.claim_detail.hearing_type_id,
                        availability_of_hearing: res.data.claim_detail.availability_of_hearing,
                        claim_description: res.data.claim_detail.claim_description,
                        equitable_relief_action_description: res.data.claim_detail.equitable_relief_action_description,
                        claim_id: claim_id,
                        loading: true
                    }, () => {
                        setTimeout(() => this.setState({ loaded: true }), 10000);
                    })
                }else{

                }
            })   
        }
    }

    toggleDesc = (event) =>{
        var checkBox = document.getElementById("equitable_relief_action");
        var text = document.getElementById("equitable_relief_action_description");

        if (checkBox.checked == true){
            text.style.display = "flex";
        } else {
            text.style.display = "none";
        }
    }

    checkboxHandler = (event) =>{
       this.setState ({ [event.target.name] :event.target.checked  })
    }

    inputHandler = (event) =>{
        this.setState ({ [event.target.name] :event.target.value  })
    }

    goBack = (e) => {
        e.preventDefault();
        this.setState({
            goBack: true
        });
    }

    addClaimInfo = (event) => {
        event.preventDefault();

        let hearing_status              = this.state.help.hearing_status;

        let hearing_type_id             = this.state.hearing_type_id ? this.state.hearing_type_id : '';
        let formal_hearing              = this.state.formal_hearing ? this.state.formal_hearing : 'no';
        let equitable_relief_action     = this.state.equitable_relief_action ? this.state.equitable_relief_action : '';
        let equitable_relief_action_description = this.state.equitable_relief_action_description ? this.state.equitable_relief_action_description : '';
        let availability_of_hearing     = this.state.availability_of_hearing ? this.state.availability_of_hearing : '';
        let claim_description           = this.state.claim_description ? this.state.claim_description : '';
        let claim_amount                = this.state.claim_amount ? this.state.claim_amount : 0;
       
        if(equitable_relief_action === ""){
            equitable_relief_action_description = '';
        }
        if(hearing_status !== "on"){
            equitable_relief_action = '';
        }
        if(formal_hearing === "no"){
            hearing_type_id = 0;
            availability_of_hearing = '';
        }
        let state_claim_id  = this.state.claim_id;
        let get_claim_id    = localStorage.getItem('claim_id');
        let claim_id;
        if(get_claim_id){
            claim_id = get_claim_id;
        }else{
            claim_id = state_claim_id;
        }
        let claimInfo = { 
            'user_id': parseInt(localStorage.getItem('user_id')), 
            'claim_id': parseInt(claim_id), 
            'access_token': localStorage.getItem('token'), 
            'claim_amount': claim_amount,
            'equitable_relief_action_required' : equitable_relief_action,
            'equitable_relief_action_description' : equitable_relief_action_description,
            'formal_hearing' : formal_hearing,
            'hearing_type_id' : parseInt(hearing_type_id),
            'availability_of_hearing' : availability_of_hearing,
            'claim_description': claim_description
        }          
        if(claim_amount || equitable_relief_action){

            this.setState({
                loaded: false
            })

            axios.post(api_url+'/api/v1/fileclaim/step2', claimInfo, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then( res => {
                let result = res.data

                if(result.success === true){
                    var claim_id    = result.claim_id;
                    var payment_line_id         = result.payment_line_id;
                    this.setState({
                        goNext: true,
                        loaded: true
                    });
                }else{
                    this.setState({
                        errormessage: result.message,
                        loaded: true
                    });
                }
                
            })
            .catch(err => {
                console.log(err);
            })
        } else {
            
            /*this.setState({ claim_info_error: "Please fill claim amount or Equitable relief action." }, () => {
                setTimeout(() => this.setState({ claim_info_error: false }), 5000);
            })*/

            toast.error('Please fill claim amount or Equitable relief action.', {
                position: "top-right",
                autoClose: 3000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
            });
            
        }
    } 

    render(){
        
        if(this.state.goBack){
            return(
                <Redirect to={`/claimant-info/${ localStorage.getItem('claim_id') }`} />
            )
        }

        if(this.state.goNext){
            return(
                <Redirect to="/document-upload" />
            )
        }
        
        if(!localStorage.getItem('token')){
            return(
                <Redirect to="/" />
            )
        }

        let hearing_types   = this.state.claimInfoMaster.hearing_types;
        let hearing_status  = this.state.help.hearing_status;
        if(hearing_status){
            hearing_status = hearing_status;
        }
        let equitable_relief_action = this.state.equitable_relief_action;
        let relief_action;
        if(equitable_relief_action === true){
            relief_action = 'flex';
        }else{
            relief_action = 'none';
        }


        return(
            <Loader loaded={this.state.loaded} lines={13} length={12} width={6} radius={12} top="100%" color="#201e53" className="loader">
                
                <div className="claim-info" show={this.props.claimId}>
                    {this.state.claim_info_error &&
                      <div className="form-validate-error" style={{ color:"red"}}>{this.state.claim_info_error}</div>
                    }
                    <form onSubmit = {this.addClaimInfo}>
                        <div className="row align-items-center mb-5">
                            <div className="col-md-4 col-12">
                                <label>Amount of Claim<sup className="required">*</sup></label>
                            </div>
                            <div className="col-md-4 col-10">
                                <input type="text" className="form-control transparent" name="claim_amount" onChange={this.inputHandler} value={this.state.claim_amount > 0 ? this.state.claim_amount : ''}/>
                            </div>
                            <div className="col-md-4 col-2">
                                <label>USD</label>
                            </div>
                        </div>
                        <div className="row align-items-center mb-5">
                            <div className="col-md-4 col-10">
                                <label data-tip data-for='equitable_relief_action_required'>Equitable Relief Action?</label>
                                {this.state.help.equitable_relief_action_required &&
                                    <ReactTooltip id='equitable_relief_action_required' >
                                        <span>{this.state.help.equitable_relief_action_required}</span>
                                    </ReactTooltip>
                                }
                            </div>
                            <div className="col-md-8 col-2">
                                <label className='checkbox'>
                                    <input type="checkbox" name="equitable_relief_action" id="equitable_relief_action" onClick={this.toggleDesc} onChange={this.checkboxHandler} checked={this.state.equitable_relief_action} />
                                    <span className={"checkmark " +(this.state.equitable_relief_action === true  ? "checked" : '')}></span>
                                </label>
                            </div>
                        </div>
                        <div className="row align-items-center mb-5" id="equitable_relief_action_description" style={{ display: relief_action }} >
                            <div className="col-md-4 col-12">
                                <label data-tip data-for='equitable_relief_action_description'>Equitable Relief Action</label>
                                {this.state.help.equitable_relief_action_description &&
                                    <ReactTooltip id='equitable_relief_action_description' >
                                        <span>{this.state.help.equitable_relief_action_description}</span>
                                    </ReactTooltip>
                                }
                            </div>
                            <div className="col-md-8 col-12">
                                <input type="text" className="form-control transparent" name="equitable_relief_action_description" value={this.state.equitable_relief_action_description ? this.state.equitable_relief_action_description : ''} onChange={this.inputHandler} required={this.state.equitable_relief_action ? "required" : ''}/>
                            </div>
                        </div>
                        {hearing_status === "on" &&
                            <>
                                <div className="row align-items-center mb-5">
                                    <div className="col-md-4 col-12">
                                        <label data-tip data-for='formal_hearing'>Hearing:Would you like a formal hearing?</label>
                                        {this.state.help.formal_hearing &&
                                            <ReactTooltip id='formal_hearing' >
                                                <span>{this.state.help.formal_hearing}</span>
                                            </ReactTooltip>
                                        }
                                    </div>
                                    <div className="col-md-8 col-12">
                                        <select className="form-control transparent" name="formal_hearing" onChange={this.inputHandler} value={this.state.formal_hearing}>
                                            <option value="no">No</option>
                                            <option value="yes">Yes</option>
                                        </select>
                                    </div>
                                </div>
                                {this.state.formal_hearing === "yes" &&
                                    <>
                                        <div className="row align-items-center mb-5">
                                            <div className="col-md-4 col-12">
                                                <label data-tip data-for='hearing_types'>Hearing Type</label>
                                                {this.state.help.hearing_type &&
                                                    <ReactTooltip id='hearing_types' >
                                                        <span>{this.state.help.hearing_type}</span>
                                                    </ReactTooltip>
                                                }
                                            </div>
                                            <div className="col-md-8 col-12">
                                                <select className="form-control transparent" name="hearing_type_id" onChange={this.inputHandler} value={this.state.hearing_type_id ? this.state.hearing_type_id : ''}>
                                                    <option value="">Please Select</option>
                                                { 
                                                    hearing_types !== undefined && hearing_types.length ? (
                                                        <>
                                                            { hearing_types ? hearing_types.map((hearing_type_item, index) =>  
                                                                <option value={hearing_type_item.id} >{hearing_type_item.name}</option>
                                                            ) : null }
                                                        </>
                                                    ) : (
                                                        <option value=""> No record found</option>
                                                    )
                                                }
                                                </select>
                                            </div>
                                        </div>
                                        <div className="row align-items-center mb-5">
                                            <div className="col-md-4 col-12">
                                                <label data-tip data-for='avail_hearing'>Availability of Hearing</label>
                                                {this.state.help.availability_of_hearing &&
                                                    <ReactTooltip id='avail_hearing' >
                                                        <span>{this.state.help.availability_of_hearing}</span>
                                                    </ReactTooltip>
                                                }
                                            </div>
                                            <div className="col-md-8 col-12">
                                                <input type="text" className="form-control transparent" name="availability_of_hearing" onChange={this.inputHandler} value={this.state.availability_of_hearing ? this.state.availability_of_hearing : ''}/>
                                            </div>
                                        </div>
                                    </>
                                }
                            </>
                        }
                        <div className="row textarea-item mb-5">
                            <div className="col-md-4 col-12">
                                <label data-tip data-for='claim_description'>Claim Description</label>
                                {this.state.help.claim_description &&
                                    <ReactTooltip id='claim_description' >
                                        <span>{this.state.help.claim_description}</span>
                                    </ReactTooltip>
                                }
                            </div>
                            <div className="col-md-8 col-12">
                                <textarea className="form-control transparent" cols="4" rows="4" name="claim_description" onChange={this.inputHandler} value={this.state.claim_description ? this.state.claim_description : ''}></textarea>
                            </div>
                        </div>
                        <div className="text-center">
                            <a href="" className="back-btn" onClick={this.goBack}><img src={Arrow} alt='arrow'/>Back</a>
                            <button type="submit" className="next-btn">Next <img src={Arrow} alt='arrow'/></button>
                        </div>
                    </form>

                    <ToastContainer />
                </div>

            </Loader>
        )
    }
}
export default ClaimInfo;