import React, {Component, useEffect, useState} from 'react';
import axios from 'axios';
import { Redirect } from 'react-router';
import Arrow from './../../../assets/images/arrow.svg';
import './claim-info.scss';

class ClaimInfo extends Component{

    constructor(props) {
        super(props);
        this.state = {
            claimInfoMaster : '',
            claim_amount: '',
            equitable_relief_action: '',
            equitable_relief_action_description: '',
            formal_hearing: '',
            hearing_type_id: '',
            availability_of_hearing: '',
            claim_description: '',
            errormessage: ''
        };
    }

    componentDidMount(){
        
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        
        if(token !== '' && user_id !== ''){
            this.getClaimInfo(user_id, token)
        }
    }

    //  Claimant Information (Fileclaim Master)
    getClaimInfo = (user_id, token) => {
        if(user_id){
            let claim_info_master = { 'user_id': parseInt(user_id), 'access_token': token }
            
            axios.post('api/v1/fileclaim/master', claim_info_master, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then(res => {
                if(res.data.status !== 'true'){
                    console.log(res.data)
                    this.setState({
                        claimInfoMaster: res.data,
                        loading: true
                    })
                }else{

                }
            })
        }
    }

    toggleDesc = (event) =>{
        var checkBox = document.getElementById("equitable_relief_action");
        var text = document.getElementById("equitable_relief_action_description");

        if (checkBox.checked == true){
            text.style.display = "flex";
        } else {
            text.style.display = "none";
        }
    }

    inputHandler = (event) =>{
        this.setState ({ [event.target.name] :event.target.value  })
    }

    addClaimInfo = (event) => {
        event.preventDefault();

        let claimInfo = { 'user_id': parseInt(localStorage.getItem('user_id')), 
                            'claim_id': parseInt(localStorage.getItem('claim_id')), 
                            'access_token': localStorage.getItem('token'), 
                            'claim_amount': this.state.claim_amount,
                            'equitable_relief_action_required' : this.state.equitable_relief_action,
                            'equitable_relief_action_description' : this.state.equitable_relief_action_description,
                            'formal_hearing' : this.state.formal_hearing,
                            'hearing_type_id' : parseInt(this.state.hearing_type_id),
                            'availability_of_hearing' : this.state.availability_of_hearing,
                            'claim_description': this.state.claim_description
                        }

        axios.post('/api/v1/fileclaim/step2', claimInfo, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data
                
            if(result.success === true){
                var claim_id    = result.claim_id;
                var payment_line_id         = result.payment_line_id;
            }else{
                this.setState({
                    errormessage: result.message
                });
            }
            
        })
        .catch(err => {
            console.log(err);
        })
    } 

    render(){

        if(!localStorage.getItem('token')){
            return(
                <Redirect to="/" />
            )
        }

        let hearing_types = this.state.claimInfoMaster.hearing_types;

        return(
            <div className="claim-info" show={this.props.claimId}>
                <form onSubmit = {this.addClaimInfo}>
                    <div className="row align-items-center mb-5">
                        <div className="col-md-4 col-12">
                            <label>Amount of Claim<sup className="required">*</sup></label>
                        </div>
                        <div className="col-md-8 col-12">
                            <input type="text" className="form-control transparent" name="claim_amount" onChange={this.inputHandler} />
                        </div>
                    </div>
                    <div className="row align-items-center mb-5">
                        <div className="col-md-4 col-10">
                            <label>Equitable Relief Action?</label>
                        </div>
                        <div className="col-md-8 col-2">
                            <label className='checkbox'>
                                <input type="checkbox" name="equitable_relief_action" id="equitable_relief_action" onClick={this.toggleDesc} onChange={this.inputHandler}/>
                                <span className="checkmark"></span>
                            </label>
                        </div>
                    </div>
                    <div className="row align-items-center mb-5">
                        <div className="col-md-4 col-12">
                            <label>Hearing:Would you like a formaal hearing?</label>
                        </div>
                        <div className="col-md-8 col-12">
                            <select className="form-control transparent" name="formal_hearing" onChange={this.inputHandler} >
                                <option value="yes" selected>Yes</option>
                                <option value="no">No</option>
                            </select>
                        </div>
                    </div>
                    <div className="row align-items-center mb-5">
                        <div className="col-md-4 col-12">
                            <label>Hearing Type</label>
                        </div>
                        <div className="col-md-8 col-12">
                            <select className="form-control transparent" name="hearing_type_id" onChange={this.inputHandler}>
                                <option value="">Please Select</option>
                            { 
                                hearing_types !== undefined && hearing_types.length ? (
                                    <>
                                        { hearing_types ? hearing_types.map((hearing_type_item, index) =>  
                                            <option value={hearing_type_item.id} >{hearing_type_item.name}</option>
                                        ) : null }
                                    </>
                                ) : (
                                    <option value=""> No record found</option>
                                )
                            }
                            </select>
                        </div>
                    </div>
                    <div className="row align-items-center mb-5" id="equitable_relief_action_description" style={{ display: "none" }} >
                        <div className="col-md-4 col-12">
                            <label>Equitable Relief Action</label>
                        </div>
                        <div className="col-md-8 col-12">
                            <input type="text" className="form-control transparent" name="equitable_relief_action_description" onChange={this.inputHandler} />
                        </div>
                    </div>
                    <div className="row align-items-center mb-5">
                        <div className="col-md-4 col-12">
                            <label>Availability of Hearing</label>
                        </div>
                        <div className="col-md-8 col-12">
                            <input type="text" className="form-control transparent" name="availability_of_hearing" onChange={this.inputHandler} />
                        </div>
                    </div>
                    <div className="row mb-5">
                        <div className="col-md-4 col-12">
                            <label>Claimant Description</label>
                        </div>
                        <div className="col-md-8 col-12">
                            <textarea className="form-control transparent" cols="4" rows="4" name="claim_description" onChange={this.inputHandler}></textarea>
                        </div>
                    </div>
                    <div className="text-center">
                        <button type="button" className="back-btn"><img src={Arrow} alt='arrow'/>Back</button>
                        <button type="submit" className="next-btn">Next <img src={Arrow} alt='arrow'/></button>
                    </div>
                </form>
            </div>
        )
    }
}
export default ClaimInfo;