import React,{Component, useEffect, useState} from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import axios from 'axios';
import { Redirect } from 'react-router';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Mail from './../../../assets/images/mail.png';
import User from './../../../assets/images/user.png';
import './add-new-claim.scss';

let api_url = process.env.REACT_APP_API_URL1;

class AddNewClaim extends Component{  

    constructor(props) {
        super(props);
        this.state = {
            errormessage: ''
        };
    }

    inputHandler = (event) =>{
        this.setState ({ [event.target.name] :event.target.value  })
    }

    addClaimantContact = (event) => {
        event.preventDefault();

        let claimantContactInfo = { 
            'user_id': parseInt(localStorage.getItem('user_id')),
            'access_token': localStorage.getItem('token'), 
            'name': this.state.name,
            'email' : this.state.email
        }

        axios.post(api_url+'/api/v1/add_claimant_contact', claimantContactInfo, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data
            
            if(result.success === true){
                var contact_id    = result.contact_id;
            }else{
                this.setState({
                    errormessage: result.message
                });
            }
            
        })
        .catch(err => {
            console.log(err);
        })
    }

    render(){

        if(!localStorage.getItem('token')){
            <Redirect to="/" />
        }

        return(        
            <Modal className="new-claim--modal" show={this.props.openNewClaim} onHide={this.props.closeNewClaim}>
                <Modal.Body>
                    <Button className="close" onClick={this.props.closeNewClaim}>&times;</Button>
                    <h3 className="text-center text-white">Add Claimant</h3>
                    <form onSubmit = {this.addClaimantContact}>
                        <InputGroup className="mb-4">
                            <InputGroup.Text id="Name"><img src={User} alt='user'/></InputGroup.Text>
                            <FormControl
                                placeholder="Name"
                                aria-label="Enter Name"
                                aria-describedby="Name"
                                name="name"
                                onChange={this.inputHandler}
                                required="required"
                            />
                        </InputGroup>
                        <InputGroup className="mb-4">
                            <InputGroup.Text id="email"><img src={Mail} alt='mail'/></InputGroup.Text>
                            <FormControl
                                placeholder="E-mail"
                                aria-label="Enter Email"
                                aria-describedby="email"
                                name="email"
                                onChange={this.inputHandler}
                                required="required"
                            />
                        </InputGroup>
                        <div className="text-center">
                            <button type="submit" className="btn-signin">Submit</button>
                        </div>
                    </form>
                </Modal.Body>
            </Modal>
        )
    }
}

export default AddNewClaim;