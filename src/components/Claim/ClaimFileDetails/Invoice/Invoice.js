import React, { Component } from 'react';
import Search from './../../../../assets/images/search-plus.png';
import File from './../../../../assets/images/file.png';
import TimeSheet from './../../../../assets/images/timesheet.png';
import Expense from './../../../../assets/images/expense.png';
import './invoice.scss';
import { Link } from "react-router-dom";

class Invoice extends Component {

    constructor(props) {
        super(props);
        this.state = {
            counter_claim_amount:''
        };
    }

    inputHandler = (event) =>{

        this.setState ({ [event.target.name] : event.target.value })
    }

    render() {

        let invoice_data        = this.props.invoice_data ? this.props.invoice_data : '';
        let formValidateError   = this.props.formValidateError ? this.props.formValidateError : '';

        return(
            <div className="invoice">
                <div className="text-right mb-4">
                    <button type="button" className="print-btn"><img src={Search} alt='print'/>View Invoice</button>
                </div>
                <div className="row">
                    <div className="col-md-6 col-12">
                        <ul>
                            <li>
                                <label>Claim Number</label>
                                <p>{invoice_data.name}</p>
                            </li>
                            <li>
                                <label>Claimant Name</label>
                                <p>{invoice_data.claimant_name}</p>
                            </li>
                        </ul>
                    </div>
                    <div className="col-md-6 col-12">
                        <ul>
                            <li>
                                <label>Claimant Amount</label>
                                <p>{invoice_data.claim_amount} USD</p>
                            </li>
                            <li>
                                <label>Counter Claim Amount</label>
                                <p><input type="text" name="counter_claim_amount" value={this.state.counter_claim_amount} onChange={this.inputHandler}/> </p>
                                {this.state.formValidateError &&
                                    <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError}</div>
                                }
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}
export default Invoice;