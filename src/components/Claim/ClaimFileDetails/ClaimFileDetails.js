import React, {Component} from 'react';
import DocumentTable from './DocumentTable';
import Attorney from './Attorney/Attorney';
import AttorneyTable from './Attorney/AttorneyTable';
import Invoice from './Invoice/Invoice';
import './file-details.scss';
import axios from 'axios';
import { Redirect } from 'react-router';
import Loader from 'react-loader'
import Breadcrumb from 'react-bootstrap/Breadcrumb'

import Search from './../../../assets/images/search-plus.png';
import StripePayment from '../../Payment/StripePayment';

let api_url = process.env.REACT_APP_API_URL1;

class ClaimFileDetails extends Component {
    intervalID;
    constructor(props) {
        super(props);
        this.state = {
          claim_id: '',
          payment_line_id: '',
          tables_data: '',
          master_data: '',
          loading: false,
          formValidateError: false,
          errorMsg: false,
          loaded: false,
          goToPayment: false
      };

    }

    componentDidMount(){

        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        localStorage.removeItem('payment_from')
        let claim_id        = this.props.match.params.id;

        if(token !== '' && user_id !== '' && claim_id !== ''){
            this.claimDetailsData(user_id, token, claim_id)
        }
    }

    /*componentDidUpdate(){

        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        let claim_id        = this.props.match.params.id;

        if(token !== '' && user_id !== '' && claim_id !== ''){
            this.claimDetailsData(user_id, token, claim_id)
        }
    }*/

    documentUploaded = () => {
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        let claim_id        = this.props.match.params.id;

        if(token !== '' && user_id !== '' && claim_id !== ''){
            this.claimDetailsData(user_id, token, claim_id)
        }
    }

    componentWillUnmount() {
        /*
          stop claimDetailsData() from continuing to run even
          after unmounting this component
        */
        clearTimeout(this.intervalID);
    }

    // Get claim deatil data
    claimDetailsData = (user_id, token, claim_id) => {
        
        let request = {
            "user_id": parseInt(user_id),
            "access_token": token,
            "claim_id": parseInt(claim_id)
        }

        axios.post(api_url+'/api/v1/myclaim/detail', request, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {

            let result = res.data
            let documents = res.data.documents
            let payment_line_id;
            if(documents[0]){
                payment_line_id = res.data.documents[0].payment_line_id
            }
            
            if(result.success === true){
                this.setState({
                    tables_data: res.data,
                    payment_line_id: payment_line_id,
                    loading: true,
                    loaded: true
                })

                localStorage.setItem('claimant_name', res.data.claim_detail.claimant_name);

                //this.intervalID = setTimeout(this.claimDetailsData.bind(this), 1000);

            }else{
                this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                    setTimeout(() => this.setState({ errorMsg: false }), 5000);
                })
            }

        })
        .catch(err => {
            console.log(err)
        })
          
    }

    inputHandler = (event) =>{

        this.setState ({ [event.target.name] : event.target.value })
    }

    // generate payment line
    geneartePaymentLineFileclaim = (event) => {
        event.preventDefault();
        let access_token        = localStorage.getItem('token');
        let user_id             = localStorage.getItem('user_id');
        
        let claim_id                =  this.props.match.params.id ? this.props.match.params.id : ''; 
        let counter_claim_amount    =  this.state.counter_claim_amount ? this.state.counter_claim_amount : '';
        let payment_line_id         =  this.state.payment_line_id ? this.state.payment_line_id : 0;

        let generate_payment_line = { 
            "user_id": parseInt(user_id),
            "access_token": access_token,
            "claim_id": parseInt(claim_id),
            "payment_line_id": payment_line_id

        }

        if(payment_line_id){
            axios.post(api_url+'/api/v1/document/fileclaim', generate_payment_line, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then( res => {
                let result = res.data

                if(result.success === true){
                    this.setState({
                        claim_id: res.data.claim_id,
                        partner_id: res.data.partner_id,
                        payment_line_id: res.data.payment_line_id
                    })

                    this.submitClaimFile(event);

                }else{
                    this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                        setTimeout(() => this.setState({ errorMsg: false }), 5000);
                    })
                }
            })
            .catch(err => {
                console.log(err)
            })

        }
        else {
            
            this.setState({ errorMsg: "Please add documents first." }, () => {
                setTimeout(() => this.setState({ errorMsg: false }), 5000);
            })
            
        }
            
    }

    // submit asset details
    submitClaimFile = (event) => {
        event.preventDefault();
        let access_token        = localStorage.getItem('token');
        let user_id             = localStorage.getItem('user_id');
        let attorney_list       = JSON.parse(localStorage.getItem('attorney_list'));
        
        let claim_id            =  this.props.match.params.id ? this.props.match.params.id : ''; 
        let payment_line_id     =  this.state.payment_line_id ? this.state.payment_line_id : 0;
        let counter_claim_amount  =  this.state.counter_claim_amount ? this.state.counter_claim_amount : 0;  
        let counter_claim_applicable = this.state.tables_data.counter_claim_applicable;
        let is_counter_claim;
        if(counter_claim_applicable === "yes"){
            is_counter_claim = true;
        }else{
            is_counter_claim = false;
        }
        if(attorney_list && attorney_list !== null ){
            attorney_list = attorney_list;
        }else{
            attorney_list = "";
        }

        let counterClaimData = { 
            "user_id": parseInt(user_id),
            "user_type": "Respondent",
            "is_counter_claim": is_counter_claim,
            "access_token": access_token,
            "claim_id": parseInt(claim_id),
            "payment_line_id": parseInt(payment_line_id),
            "counter_claim_amount": parseFloat(counter_claim_amount),
            "attorney_list": attorney_list

        }
        
        if(payment_line_id){
            if(counter_claim_applicable === "yes" && counter_claim_amount === 0 ){
                this.setState({ errorMsg: "Counter claim amount should be a +ve number." }, () => {
                    setTimeout(() => this.setState({ errorMsg: false }), 5000);
                })
                window.scrollTo(0,0)
            }else{

                this.setState({
                    loaded: false
                })
                axios.post(api_url+'/api/v1/counterclaim', counterClaimData, {
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                })
                .then( res => {
                    let result = res.data

                    if(result.success === true){
                        localStorage.setItem('fee_amount', res.data.fee_amount)
                        localStorage.setItem('claim_id', res.data.claim_id);
                        localStorage.setItem('payment_line_id', res.data.payment_line_id);

                        localStorage.removeItem('payment_from')
                        localStorage.setItem('payment_from', "file_document")

                        this.setState({
                            claim_id: res.data.claim_id,
                            fee_amount: res.data.fee_amount,
                            claim_amount: res.data.claim_amount,
                            counter_claim_amount: res.data.counter_claim_amount,
                            payment_reference: res.data.payment_reference,
                            payment_line_id: res.data.payment_line_id,
                            paymeny_key: res.data.paymeny_key,
                            paymeny_token: res.data.paymeny_token,
                            loaded: true,
                            goToPayment: true
                        })

                    }else{
                        this.setState({ errorMsg: "Somthing went wrong!!", loaded: true }, () => {
                            setTimeout(() => this.setState({ errorMsg: false }), 5000);
                        })
                    }
                })
                .catch(err => {
                    console.log(err)
                })
            }

        }
        else {
            
            /*this.setState({ formValidateError: "This field is required." }, () => {
                setTimeout(() => this.setState({ formValidateError: false }), 5000);
            })*/

            this.setState({ errorMsg: "Please add documents first." }, () => {
                setTimeout(() => this.setState({ errorMsg: false }), 5000);
            })

            window.scrollTo(0,0)
            
        }
            
    }

    render() {

        if (!localStorage.getItem('token')) {
            return (
                <Redirect to="/" />
            )
        }

        /*if(this.state.goToPayment){
            return(
                <Redirect to="/payment" />
            )
        }*/

        let all_data        = this.state.tables_data;

        let invoice_data    = all_data.claim_detail ? all_data.claim_detail : '';
        let respondent_data = all_data.respondent;
        let document_data   = all_data.documents;
        let claim_id;
        let payment_line_id;
        if(invoice_data){
            claim_id = invoice_data.id;
        }
        let counter_claim_applicable = all_data.counter_claim_applicable;
        let filling_fee              = all_data.payment_line_id;

        let filling_amount = 0;
        if(filling_fee !== undefined){
            let filling_fees = Object.keys(filling_fee); 
            filling_fees.forEach(function(key, i) {

                let filling_state = filling_fee[key]['state'];
                if(filling_state === "draft"){
                    let amt = filling_fee[key]['amount'];
                    filling_amount = parseFloat(filling_amount) + parseFloat(amt);
                }
                
            });
        }

        return(
            <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} radius={12} color="#201e53">
                <Breadcrumb>
                    <div className="container">
                        <div className="row">
                            <Breadcrumb.Item href="/dashboard">My Claims</Breadcrumb.Item>
                            <Breadcrumb.Item href="#">File Document</Breadcrumb.Item>
                        </div>
                    </div>
                </Breadcrumb>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="file-details claim-file-documents">
                                <form onSubmit={this.submitClaimFile}>
                                    {this.state.errorMsg &&
                                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.errorMsg}</div>
                                    }
                                    <input type="hidden" name="payment_line_id" value={this.state.payment_line_id} onChange={this.inputHandler} />
                                    <div className="invoice">
                                        <div className="row">
                                            <div className="col-md-6 col-12">
                                                <ul>
                                                    <li>
                                                        <label>Claim#</label>
                                                        <p>{invoice_data.name}</p>
                                                    </li>
                                                    <li>
                                                        <label>Claimant Name</label>
                                                        <p>{invoice_data.claimant_name}</p>
                                                    </li>
                                                    <li>
                                                        <label>Filling Fee</label>
                                                        <p>{filling_amount} USD</p>
                                                    </li>
                                                </ul>
                                            </div>
                                            <div className="col-md-6 col-12">
                                                <ul>
                                                    <li>
                                                        <label>Claimant Amount</label>
                                                        <p>{invoice_data.claim_amount} USD</p>
                                                    </li>
                                                    {invoice_data.counter_claim_amount > 0 ? (
                                                        <li className='no-margin'>
                                                        <label>Counter Claim Amount</label>
                                                        <p>{invoice_data.counter_claim_amount} USD</p>
                                                        </li>
                                                    ): null}
                                                    {counter_claim_applicable === "yes" &&
                                                        <li>
                                                            <label>Counter Claim Amount</label>
                                                            <p>
                                                                <input type="number" min="1" name="counter_claim_amount" class="form-control" value={this.state.counter_claim_amount} onChange={this.inputHandler}/> 
                                                                {this.state.formValidateError &&
                                                                    <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError}</div>
                                                                }
                                                            </p>
                                                            
                                                        </li>
                                                    }
                                                </ul>
                                            </div>
                                        </div>
                                    </div>

                                    {/*<Invoice invoice_data={invoice_data} formValidateError={this.state.formValidateError} />*/}
                                    
                                    {counter_claim_applicable === "yes" &&
                                        <AttorneyTable respondent_data={respondent_data} loading={this.state.loading}/>
                                    }
                                    <DocumentTable document_data={document_data} documentUploadcallback={this.documentUploaded} claim_id={claim_id} loading={this.state.loading}/>
                                    
                                    
                                    <div className="text-right mb-4 invoice">
                                        <button type="submit" className="print-btn"> Submit document </button>
                                    </div>
                                    
                                </form>
                            </div>
                       </div>
                   </div>
                </div>

                {this.state.goToPayment &&
                    <StripePayment />
                }

            </Loader> 
        )   
    }
}
export default ClaimFileDetails;