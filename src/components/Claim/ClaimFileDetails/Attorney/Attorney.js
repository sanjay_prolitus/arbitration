import React from 'react';
import AttorneyTable from './AttorneyTable';
import UserPlus from './../../../../assets/images/user-blue.png';
import './attorney.scss';
import { Link } from "react-router-dom";

const Attorney = () =>{
    return(
       <div className="file-attorney">            
            <div className='row'>
                <div className="col-12">
                    <h2>Respondent</h2>
                </div>
                  <div className="col-12 text-right">
                      <Link onClick={e => e.preventDefault()} className="add-respondent" onClick={this.openRespond}><img src={UserPlus} alt='user'/>Add Respondent</Link>
                  </div>
            </div>
            <div className='row'>
                <div className="col-md-6 col-12">
                    <AttorneyTable/>
                </div>
            </div>
       </div>
    )
}
export default Attorney;