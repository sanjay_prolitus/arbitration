import React, { Component } from 'react';
import DataTable from 'react-data-table-component';
import Edit from './../../../../assets/images/edit.png';
import Delete from './../../../../assets/images/delete.png';
import './attorney-table.scss';
import { Link } from "react-router-dom";
import axios from 'axios';

import Arrow from './../../../../assets/images/arrow.svg';
import UserPlus from './../../../../assets/images/user-plus.png';
import './respondent-info.scss';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Mail from './../../../../assets/images/mail.png';
import User from './../../../../assets/images/user.png';
import './respond.scss';
import NoDataTable from '../../../Common/NoDataTable';

class RespondentTable extends Component {
    intervalID;
    constructor(props) {
        super(props);
        this.state = {
          showModel: false,
          showEditModel: false,
          claim_id: '',
          name: '',
          email: '',
          edit_name: '',
          edit_email: '',
          formValidateError0: '',
          formValidateError1: '',
          formValidateError2: '',
          success_message: false,
          attorney_lists: '',
          selectedIndex: ''
        };

        this.openAttorney               = this.openAttorney.bind(this);
        this.handleCloseAttorney        = this.handleCloseAttorney.bind(this);
        this.handleCloseEditAttorney    = this.handleCloseEditAttorney.bind(this);

    }

    componentDidMount(){
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');

        this.getAttorneyStoredata()
        
    }

    componentWillUnmount() {
        /*
          stop getAttorneyStoredata() from continuing to run even
          after unmounting this component
        */
        clearTimeout(this.intervalID);
    }

    openAttorney(e) {
        this.setState({
            showModel: true
        })
    }

    handleCloseAttorney(e) {
        this.setState({
            showModel: false
        })
    }

    handleCloseEditAttorney(e) {
        this.setState({
            showModel: false
        })
    }

    inputHandler = (event) =>{

        this.setState ({ [event.target.name] : event.target.value })
    }

    // Submit Attorney list
    submitAttorney = (event) => {
        event.preventDefault();
        
        let name    =  this.state.name ? this.state.name : '';
        let email   =  this.state.email ? this.state.email : '';
                    
        if(name && email){

            let attorney_list = JSON.parse(localStorage.getItem('attorney_list'));
            let attorneyList = {
                "name": name,
                "email": email
            };
            if(attorney_list){                
                attorney_list.push(attorneyList);
                localStorage.setItem('attorney_list', JSON.stringify(attorney_list));
            }else{
                attorneyList = [attorneyList];
                localStorage.setItem('attorney_list', JSON.stringify(attorneyList));
            }


            this.setState({ success_message: "Successfully added.", name: '', email: '' }, () => {
              setTimeout(() => this.setState({ success_message: false, showModel: false }), 2000);
          })
        }else {
          if(name == ''){
              this.setState({ formValidateError0: "This field is required." }, () => {
                  setTimeout(() => this.setState({ formValidateError0: false }), 5000);
              })
          }
          if(email == ''){
              this.setState({ formValidateError1: "This field is required." }, () => {
                  setTimeout(() => this.setState({ formValidateError1: false }), 3000);
              })
          }
        }
    }

    // Edit Attorneylist
    submitEditAttorney = (event) => {
        event.preventDefault();

        let edit_name           =  this.state.edit_name ? this.state.edit_name : '';
        let edit_email          =  this.state.edit_email ? this.state.edit_email : '';
        let selectedIndex       =  this.state.selectedIndex ? this.state.selectedIndex : 0;
                    
        if(edit_name && edit_email){

            let attorney_list = JSON.parse(localStorage.getItem('attorney_list'));
            let attorneyList = {
                "name": edit_name,
                "email": edit_email
            };
            attorney_list[selectedIndex] = attorneyList;
            localStorage.setItem('attorney_list', JSON.stringify(attorney_list));

            this.setState({ success_message: "Successfully updated." }, () => {
              setTimeout(() => this.setState({ success_message: false, showEditModel: false }), 2000);
          })
        }else {
          if(edit_name == ''){
              this.setState({ formValidateError0: "This field is required." }, () => {
                  setTimeout(() => this.setState({ formValidateError0: false }), 5000);
              })
          }
          if(edit_email == ''){
              this.setState({ formValidateError1: "This field is required." }, () => {
                  setTimeout(() => this.setState({ formValidateError1: false }), 3000);
              })
          }
        }
    }

    // Get Attorney data
    getAttorneyStoredata = () => {

        let attorney_list   = localStorage.getItem('attorney_list');
        attorney_list       = JSON.parse(attorney_list);
        
        if(attorney_list){
            this.setState({
                attorney_lists: attorney_list,
                loading: true
            })
        }

        // call getAttorneyStoredata() again in set time in seconds
        this.intervalID = setTimeout(this.getAttorneyStoredata.bind(this), 1000);
    }

    removeAttorney = (index) => {
        const getSelectedIndex = JSON.parse(localStorage.getItem("attorney_list"));
        getSelectedIndex.splice(index, 1);
        localStorage.setItem("attorney_list", JSON.stringify(getSelectedIndex));      
    }

    editAttorney = (index) => {
        let attorney_list    = JSON.parse(localStorage.getItem("attorney_list"));
        let attorney_data    = attorney_list[index];

        this.setState({
            edit_name: attorney_data.name,
            edit_email: attorney_data.email,
            showEditModel: true,
            selectedIndex: index
        })
    }

    render() {

        let attorney_lists  = this.state.attorney_lists;
        let loading         = this.props.loading;
        
        let columns = [
            {
                name: 'Name',
                selector: row => row.name,
                sortable: true
            },
            {
                name: 'Email',
                selector: row => row.email,
                sortable: true
            },
            {
                name: 'Action',
                selector: row => row.action,
                sortable: true
            }         
        ];

        let list_data = [];
        if(attorney_lists !== undefined){
            let attorney_list = Object.keys(attorney_lists); 
            attorney_list.forEach(function(key, i) {

                let name = attorney_lists[key].name;
                let action_col;
                let $this = this;
                if(name){
                    action_col = 
                    <>
                        <img src={Edit} alt='Edit' onClick={() => $this.editAttorney(key)}/>
                        <img src={Delete} alt='delete' onClick={() => $this.removeAttorney(key)}/>
                    </>
                }

                list_data.push({ 
                    name: attorney_lists[key].name, 
                    email: attorney_lists[key].email,
                    action: action_col
                })

            });
        }
        
        return(
            <>

                <div className='row'>
                    <div className="col-md-6 col-3">
                        <h2>Attorney</h2>
                    </div>
                    <div className="col-md-6 col-9 text-right">
                        <Link onClick={e => e.preventDefault()} className="add-primary" onClick={this.openAttorney}>Add Attorney</Link>
                    </div>
                </div>
                <>
                    {list_data.length > 0 &&
                        <DataTable
                            columns={columns}
                            data={list_data}
                            fixedHeader
                        />
                    }

                    { list_data.length == 0 &&
                      <div className="document-table table-responsive">
                        <NoDataTable columns={columns} title="No Data Available" />
                      </div>
                    }    
                </>

                <Modal className="respond--modal" show={this.state.showModel}>
                    <Modal.Body>
                        <Button className="close" onClick={this.handleCloseAttorney}>&times;</Button>
                        <h3 className="text-center">Add Attorney</h3>
                        {this.state.errorMsg &&
                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.errorMsg}</div>
                        }
                        {this.state.success_message &&
                            <div className="success-message" style={{ color:"green"}}>{this.state.success_message}</div>
                        }
                        
                          <InputGroup className="mb-4">
                              <InputGroup.Text id="rName"><img src={User} alt='respond'/></InputGroup.Text>
                              <FormControl
                                  placeholder="Attorney Name"
                                  aria-label="Enter Attorney"
                                  aria-describedby="rName"
                                  name="name"
                                  type="text"
                                  value={this.state.name}
                                  onChange={this.inputHandler}
                              />
                          </InputGroup>
                          {this.state.formValidateError0 &&
                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError0}</div>
                          }
                          <InputGroup className="mb-4">
                              <InputGroup.Text id="email"><img src={Mail} alt='mail'/></InputGroup.Text>
                              <FormControl
                                  placeholder="E-mail"
                                  aria-label="Enter Email"
                                  aria-describedby="email"
                                  name="email"
                                  type="email"
                                  value={this.state.email}
                                  onChange={this.inputHandler}
                              />
                          </InputGroup>
                          {this.state.formValidateError1 &&
                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError1}</div>
                          }
                          {this.state.formValidateError2 &&
                              <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError2}</div>
                          }
                          {this.state.errorMsg &&
                              <div className="form-validate-error" style={{ color:"red"}}>{this.state.errorMsg}</div>
                          }
                          <div className="text-center">
                              <button type="submit" className="btn-signin" onClick={this.submitAttorney}>Submit</button>
                          </div>
                        
                    </Modal.Body>
                </Modal>

                <Modal className="respond--modal" show={this.state.showEditModel}>
                    <Modal.Body>
                        <Button className="close" onClick={this.handleCloseEditAttorney}>&times;</Button>
                        <h3 className="text-center">Edit Attorney</h3>
                        {this.state.errorMsg &&
                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.errorMsg}</div>
                        }
                        {this.state.success_message &&
                            <div className="success-message" style={{ color:"green"}}>{this.state.success_message}</div>
                        }
                        
                          <InputGroup className="mb-4">
                              <InputGroup.Text id="rName"><img src={User} alt='respond'/></InputGroup.Text>
                              <FormControl
                                  placeholder="Attorney Name"
                                  aria-label="Enter Attorney"
                                  aria-describedby="rName"
                                  name="edit_name"
                                  type="text"
                                  value={this.state.edit_name}
                                  onChange={this.inputHandler}
                              />
                          </InputGroup>
                          {this.state.formValidateError0 &&
                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError0}</div>
                          }
                          <InputGroup className="mb-4">
                              <InputGroup.Text id="email"><img src={Mail} alt='mail'/></InputGroup.Text>
                              <FormControl
                                  placeholder="E-mail"
                                  aria-label="Enter Email"
                                  aria-describedby="email"
                                  name="edit_email"
                                  type="email"
                                  value={this.state.edit_email}
                                  onChange={this.inputHandler}
                              />
                          </InputGroup>
                          {this.state.formValidateError1 &&
                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError1}</div>
                          }
                          {this.state.formValidateError2 &&
                              <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError2}</div>
                          }
                          {this.state.errorMsg &&
                              <div className="form-validate-error" style={{ color:"red"}}>{this.state.errorMsg}</div>
                          }
                          <div className="text-center">
                              <button type="submit" className="btn-signin" onClick={this.submitEditAttorney}>Submit</button>
                          </div>
                        
                    </Modal.Body>
                </Modal>
           </>
        )
    }
}
export default RespondentTable;