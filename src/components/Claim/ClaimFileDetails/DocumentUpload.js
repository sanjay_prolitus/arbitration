import React, { Component } from 'react';
import DocumentTable from './DocumentTable';
import AddDocument from './AddDocument';
import Arrow from './../../../assets/images/arrow.svg';
import './document-upload.scss';
import './add-document.scss';
import axios from 'axios';
import { Redirect } from 'react-router';
import { Link } from "react-router-dom";

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import File from './../../../assets/images/otp.png';
import './add-document.scss';

let api_url = process.env.REACT_APP_API_URL1;

class DocumentUpload extends Component {
    intervalID;
    constructor(props) {
        super(props);
        this.state = {
            showModel: false,
            errorMsg: '',
            goBack: false,
            goNext: false,
            master_data: '',
            myclaim_detail_data: '',
            claim_id: '',
            payment_line_id: '',
            document_id: '',
            parent_id: '',
            document_configuration_id: '',
            description: '',
            document: '',
            doc_type: '',
            document_name: '',
            formValidateError0: '',
            formValidateError1: '',
            formValidateError2: '',
            formValidateError3: '',
            agreement_type_id: '',
            partner_id: ''
        };

        this.openAddDocument            = this.openAddDocument.bind(this);
        this.handleCloseAddDocument     = this.handleCloseAddDocument.bind(this);

    }

    componentDidMount(){

        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');

        if(token !== '' && user_id !== ''){
            this.fileclaimMaster(user_id, token)
            this.myclaimDetail(user_id, token)
        }

    }

    componentWillUnmount() {
        /*
          stop myclaimDetail() from continuing to run even
          after unmounting this component
        */
        clearTimeout(this.intervalID);
    }

    openAddDocument(e) {
        this.setState({
            showModel: true
        })
    }

    handleCloseAddDocument(e) {
        this.setState({
            showModel: false
        })
    }

    inputHandler = (event) =>{

        this.setState ({ [event.target.name] : event.target.value })
    }

    onFileChange = event => {
        this.setState({ document: event.target.files[0] });
    };

    // get fileclaim Master
    fileclaimMaster = (user_id, token) => {

        let fileclaim_request = { 
            "user_id": parseInt(user_id),
            "access_token": token
        }
        
        axios.post(api_url+'/api/v1/fileclaim/master', fileclaim_request, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data

            if(result.success === true){

                let document_name;                
                let doc_type;                
                let documents     = res.data.documents;
                if(documents && documents.length){
                    document_name   = documents[0]['name']
                    doc_type        = documents[0]['doc_type']
                }
                this.setState({ 
                    master_data: res.data,
                    document_name: document_name,
                    doc_type: doc_type
                })
                
            }else{
                this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                    setTimeout(() => this.setState({ errorMsg: false }), 5000);
                })
            }
        })
        .catch(err => {
            console.log(err)
        })
        
    }

    // submit asset details
    docsUpload = (event) => {
        event.preventDefault();
        let access_token        = localStorage.getItem('token');
        let user_id             = localStorage.getItem('user_id');
        
        let claim_id            =  this.state.claim_id ? this.state.claim_id : 21; 
        let payment_line_id     =  this.state.payment_line_id ? this.state.payment_line_id : 18; 
        let parent_id           =  this.state.parent_id ? this.state.parent_id : 0;
        let document_config_id  =  this.state.document_configuration_id ? this.state.document_configuration_id : 1;  
        let description         =  this.state.description ? this.state.description : '';
        let documents           =  this.state.document ? this.state.document : '';  
        let document_name       =  this.state.document_name ? this.state.document_name : '';  
        let doc_type            =  this.state.doc_type ? this.state.doc_type : '';  
        let partner_id          =  this.state.partner_id ? this.state.partner_id : 14;  

        this.getDocumentByID(user_id, access_token, claim_id, partner_id, payment_line_id)
        
        let document_id         =  this.state.document_id ? this.state.document_id : 16;  
       
        /*let case_document = {
            "document_id": 15,
            "parent_id": 0,
            "document_configuration_id": 1,
            "description": description,
            "document": documents,
            "document_name": document_name,
            "doc_type": doc_type
        }
        
        let case_documents = {
            "user_id": parseInt(user_id),
            "access_token": access_token,
            "claim_id": 21,
            "payment_line_id":18,
            "case_document": 
                {
                    "document_id": 15,
                    "parent_id": 0,
                    "document_configuration_id": 1,
                    "description": description,
                    "document": documents,
                    "document_name": document_name,
                    "doc_type": doc_type
                }
        }*/
        
        if(document_name && doc_type && documents){
            if (documents.name.match(/\.(jpg|jpeg|png|pdf|JPG|JPEG|PNG|PDF)$/)) {
                const formdata = new FormData();
                formdata.append('user_id', parseInt(user_id))
                formdata.append('access_token', access_token)
                formdata.append('claim_id', claim_id)
                formdata.append('payment_line_id', payment_line_id)
                formdata.append('document_id', document_id)
                formdata.append('parent_id', parent_id)
                formdata.append('document_configuration_id', document_config_id)
                formdata.append('description', description)
                formdata.append('document', documents)
                formdata.append('document_name', document_name)
                formdata.append('doc_type', doc_type.toString().toLowerCase())
                //formdata.append('case_document', case_document)
                
                axios.post(api_url+'/api/v1/upload/document', formdata, {
                    headers: {
                        'Content-Type': 'text/plain'
                    }
                })
                .then( res => {
                    let result = res.data
                    
                    if(result.success === "true"){
                        this.setState({
                            claim_id: res.data.claim_id,
                            payment_line_id: res.data.payment_line_id,
                            document_id: res.data.document_id,
                            showModel: false
                        })

                        this.myclaimDetail(user_id, access_token);
                    }else{
                        this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                            setTimeout(() => this.setState({ errorMsg: false }), 5000);
                        })
                    }
                })
                .catch(err => {
                    console.log(err)
                })

            }else{
                this.setState({ formValidateError3: "Only jpg, jpeg, png, pdf files are allowed!" }, () => {
                    setTimeout(() => this.setState({ formValidateError3: false }), 5000);
                })
            }
        }
        else {
            if(document_name == ''){
                this.setState({ formValidateError0: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formValidateError0: false }), 5000);
                })
            }
            if(doc_type == ''){
                this.setState({ formValidateError1: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formValidateError1: false }), 5000);
                })
            }
            if(documents == ''){
                this.setState({ formValidateError2: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formValidateError2: false }), 5000);
                })
            }
        }
            
    }

    // Go to previous screen
    goBack = (e) => {
        e.preventDefault();
        this.setState({
            goBack: true
        });
    }

    // fileclaim step 3 submit
    fileclaimStep3Submit = (event) => {
        event.preventDefault();

        let access_token        = localStorage.getItem('token');
        let user_id             = localStorage.getItem('user_id');
        
        let claim_id            =  localStorage.getItem('claim_id'); 
        let partner_id          =  localStorage.getItem('partner_id'); 
        let agreement_type_id   =  this.state.agreement_type_id ? this.state.agreement_type_id : '1';  
        let payment_line_id     =  this.state.payment_line_id ? this.state.payment_line_id : '0';
        
        let fileclaim_step3 = {
            "user_id": parseInt(user_id),
            "access_token": access_token,
            "claim_id": parseInt(claim_id),
            "partner_id": parseInt(partner_id),
            "agreement_type_id": parseInt(agreement_type_id),
            "payment_line_id": parseInt(payment_line_id)
        }

        axios.post(api_url+'/api/v1/fileclaim/step3', fileclaim_step3, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data
            if(result.success === true){
                this.setState({
                    goNext: true,
                    claim_id: result.claim_id,
                    payment_line_id: result.payment_line_id,
                    document_id: 12
                })

                localStorage.setItem('payment_line_id', this.state.payment_line_id);

            }else{
                this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                    setTimeout(() => this.setState({ errorMsg: false }), 5000);
                })
            }
        })
        .catch(err => {
            console.log(err)
        })

    }

    // get document list with claim id
    myclaimDetail = (user_id, token) => {

        let claim_id            =  this.state.claim_id ? this.state.claim_id : 21; 

        let fileclaim_request = { 
            "user_id": parseInt(user_id),
            "access_token": token,
            "claim_id": parseInt(claim_id)
        }
        
        axios.post(api_url+'/api/v1/myclaim/detail', fileclaim_request, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data
            
            if(result.success === true){
                this.setState({ 
                    myclaim_detail_data: res.data
                })
                
            }else{
                this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                    setTimeout(() => this.setState({ errorMsg: false }), 5000);
                })
            }

            //this.intervalID = setTimeout(this.myclaimDetail.bind(this), 1000);
        })
        .catch(err => {
            console.log(err)
        })
        
    }
    // get documents with id
    getDocumentByID = (user_id, access_token, claim_id, partner_id, payment_line_id) => {

        let request = { 
            "user_id": parseInt(user_id),
            "access_token": access_token,
            "claim_id": parseInt(claim_id),
            "partner_id": parseInt(partner_id),
            "payment_line_id": parseInt(payment_line_id)
        }
        
        axios.post(api_url+'/api/v1/get/document/id', request, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data

            if(result.success === true){
                this.setState({ 
                    document_id: res.data.document_id
                })
                
            }else{
                this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                    setTimeout(() => this.setState({ errorMsg: false }), 5000);
                })
            }
        })
        .catch(err => {
            console.log(err)
        })
        
    }

    render() {

        if(this.state.goBack){
            return(
                <Redirect to="/claim-info" />
            )
        }

        if(this.state.goNext){
            return(
                <Redirect to="/respondent-info" />
            )
        }

        if(!localStorage.getItem('token')){
            return(
                <Redirect to="/" />
            )
        }

        let documents = this.state.master_data.documents;
        let documents_info = this.state.myclaim_detail_data;

        return(
           <div className="document-upload">
                <h1>Agreement</h1>
                <ul>
                    <li>
                        <label className='checkbox radiobox'>The Parties have a written agreement that includes an arbitration provision and the agreement will be uploaded below
                            <input type="radio" name='agreement_type_id' value={this.state.agreement_type_id ? this.state.agreement_type_id : '1'} defaultChecked/>
                            <span className="checkmark"></span>
                        </label>
                    </li>
                    <li>
                        <label className='checkbox radiobox'>The written agreement between the parties don not include an arbitration provision but the parties have subsquents agreed to submit their dispute tor binding artAration with Mediation and Civil Arbitration 
                            <input type="radio" name='agreement_type_id' value={this.state.agreement_type_id ? this.state.agreement_type_id : '2'} />
                            <span className="checkmark"></span>
                        </label>
                    </li>
                    <li>
                        <label className='checkbox radiobox'>The written agreement between the parties does not Include an arbitration provision and the parties have not agreed to submittheir dispute for binding arbitration with Mediation and Civil Arbitration
                            <input type="radio" name='agreement_type_id' value={this.state.agreement_type_id ? this.state.agreement_type_id : '3'} />
                            <span className="checkmark"></span>
                        </label>
                    </li>
                </ul>
                <div className='row'>
                    <div className="col-md-6 col-12">
                        <h2>Document Upload</h2>
                    </div>
                    <div className="col-md-6 col-12 text-right">
                        <Link onClick={e => e.preventDefault()} className="add-primary" onClick={this.openAddDocument}>Add Primary</Link>
                    </div>
                </div>

                <DocumentTable table_data={documents_info} />

               <div className="text-center">
                    <button type="button" className="back-btn" onClick={this.goBack}><img src={Arrow} alt='arrow'/>Back</button>
                    <button type="button" className="next-btn" onClick={this.fileclaimStep3Submit}>Next <img src={Arrow} alt='arrow' /></button>
                </div>

                <Modal className="add-document--modal white-background-modal" show={this.state.showModel} >
                    <Modal.Body>
                        <Button className="close" onClick={this.handleCloseAddDocument}>&times;</Button>
                        <h3 className="text-center">Add Document</h3>
                        {this.state.errorMsg &&
                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.errorMsg}</div>
                        }
                        <div className="d-none">
                            <h3 className="text-center mb-2">Upload Document</h3>
                            <i className="text-center d-block">Primary Document</i>
                        </div>
                        <form onSubmit = {this.docsUpload}>
                            <div className="row align-items-center mb-4">
                                <div className="col-md-4 col-12">
                                    <label>Document<sup>*</sup></label>
                                </div>
                                <div className="col-md-8 col-12">
                                    <select className="form-control" name="document_name" value={this.state.document_name} onChange={this.inputHandler}>
                                        {documents ? documents.map((document, i) =>
                                            <option value={document.name} key={i} >{document.name}</option>
                                        ) : null}
                                    </select>
                                    {this.state.formValidateError0 &&
                                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError0}</div>
                                    }
                                </div>
                            </div>
                            <div className="row align-items-center mb-4">
                                <div className="col-md-4 col-12">
                                    <label>Document Type<sup className="required">*</sup></label>
                                </div>
                                <div className="col-md-8 col-12">
                                    <select className="form-control" name="doc_type" value={this.state.document_name} onChange={this.inputHandler}>
                                        {documents ? documents.map((document, j) =>
                                            <option value={document.doc_type} key={j}>{document.doc_type}</option>
                                        ) : null}
                                    </select>
                                    {this.state.formValidateError1 &&
                                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError1}</div>
                                    }
                                </div>
                            </div>
                            <div className="row mb-4">
                                <div className="col-md-4 col-12">
                                    <label>Description</label>
                                </div>
                                <div className="col-md-8 col-12">
                                    <textarea className="form-control" cols="4" rows="4" name="description" value={this.state.description} onChange={this.inputHandler}></textarea>
                                </div>
                            </div>
                            <div className="row align-items-center mb-4">
                                <div className="col-md-4 col-12">
                                    <label>Upload Document</label>
                                </div>
                                <div className="col-md-8 col-12">
                                   <label className="file-attach" htmlFor='file'>
                                       <input type="file" id="file" name="document" onChange={this.onFileChange}/>
                                       <p className="text-uppercase">Choose file<img src={File} alt="Upload"/></p>
                                   </label>
                                   {this.state.formValidateError2 &&
                                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError2}</div>
                                    }
                                    {this.state.formValidateError3 &&
                                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError3}</div>
                                    }
                                </div>
                            </div>
                            <button type="submit" className="btn-signin">Submit 123</button>
                        </form>
                    </Modal.Body>
                </Modal>

           </div>
        )
    }
}
export default DocumentUpload;