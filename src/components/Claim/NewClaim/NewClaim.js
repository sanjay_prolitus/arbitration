import React from 'react';
import Arrow from './../../../assets/images/arrow-circle.png';
import './new-claim.scss';

const NewClaim = () =>{
    return(
       <div className="new-claim">
            <button type="button" className="new-claim-btn">File A New Claim <img src={Arrow} alt='arrow'/></button>
       </div>
    )
}
export default NewClaim;