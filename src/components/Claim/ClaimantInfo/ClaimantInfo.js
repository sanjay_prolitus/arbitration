import React, {Component, useEffect, useState} from 'react';
import axios from 'axios';
import { Redirect, match, matchPath } from 'react-router';
import Select from 'react-select';
import Arrow from './../../../assets/images/arrow.svg';
import AddNewClaim from '../AddNewClaim/AddNewClaim';
import './claimant-info.scss';
import Loader from 'react-loader'
import ReactTooltip from 'react-tooltip';

import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Mail from './../../../assets/images/mail.png';
import User from './../../../assets/images/user.png';

/*import ReactDOM from "react-dom";
import { BrowserRouter as Router, Route } from "react-router-dom";*/

let api_url = process.env.REACT_APP_API_URL1;

class ClaimantInfo extends Component{
    
    constructor(props) {
        super(props);
        this.state = {
            claim_id : 0,
            claimantInfoMaster : '',
            are_you_filing_for: 'arbitration',
            claimant_added: false,
            claimant_name: '',
            claimantname: '',
            claimant_id: '',
            claimant_email: '',
            claimant_address: '',
            representative_type: '',
            representative_type_other: '',
            SetOpen: false,
            errormessage: '',
            loaded: false,
            isDisabled: false,
            help: '',
            name: '',
            email: '',
            claimant_options: []
        };

        this.handleAddNew = this.handleAddNew.bind(this);
        this.handleCloseAddNew  = this.handleCloseAddNew .bind(this);
    }

    claimantChange = (selectedOption) => {

        let claimant_contact_id = selectedOption.id;
        let claimant_name       = selectedOption.value;
        let claimant_email      = selectedOption.email;
        this.setState({ 
            claimant_contact_id: claimant_contact_id,
            claimant_name: claimant_name,
            claimant_email: claimant_email

        });
    };

    componentDidMount(){
        
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        if(!this.props){
            return;
        }

        let claim_id = "";
        if( typeof this.props != undefined && this.props != "" && Object.keys(this.props).length !== 0 ){
            claim_id = parseInt(this.props.param.id);
        }

        //console.log("Props Data: " + this.getProps());

            // console.log("Claim ID", this.props.match.params.id);

        if(token !== '' && user_id !== ''){ 
            this.getClaimantInfo(user_id, token, claim_id);
            this.getClaimantContactInfo(user_id, token);


        }
    }

    getClaimantContactInfo = (user_id, token) => {
        if(user_id){
            let claimant_info = { 'user_id': parseInt(user_id), 'access_token': token }

            axios.post(api_url+'/api/v1/get_claimant_contact', claimant_info, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then(res => {
                
                if(res.data.success === true){
                    let claimant = res.data.Claimant;
                    let claimant_email;
                    let claimant_id;
                    let claimant_name;
                    if(claimant && claimant.length){
                        claimant_email  = claimant[0]['email'];
                        claimant_id     = claimant[0]['id'];
                        claimant_name   = claimant[0]['name'];
                    }

                    let claimant_options = [];
                    if(claimant !== undefined && claimant.length ){
                        //let claimant_list = Object.keys(claimant); 

                        claimant.forEach(function(data, key) {

                            claimant_options.push({ 
                                value: data.name, 
                                id: data.id, 
                                email: data.email, 
                                label: data.name + ' ( '+data.email + ' )'
                            })

                        });
                    }
                    this.setState({
                        claimantContactData: res.data.Claimant,
                        claimant_email: claimant_email,
                        claimant_id: claimant_id,
                        claimant_name: claimant_name,
                        claimant_options: claimant_options,
                        loading: true,
                        loaded: true
                    })
                }else{

                }
            })
        }
    }

    getProps(){

        const match = matchPath(this.props.location.pathname, {
            path: "/claimant-info/:id",
            exact: true,
            strict: false
        });

        return match;
    }

    handleAddNew(){
        this.setState(state => ({
            SetOpen: true
        }));
    }

    handleCloseAddNew(){
        this.setState(state => ({
            SetOpen: false
        }));
    }

    //  Claimant Information (Fileclaim Master)
    getClaimantInfo = (user_id, token, claim_id) => {
        let claimant_master_info = { 'user_id': parseInt(user_id), 'access_token': token }
        
        axios.post(api_url+'/api/v1/fileclaim/master', claimant_master_info, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then(result => {

            if(result.data.success === true){
                let representative_types = result.data.representative_types;
                if(representative_types){
                    representative_types = representative_types[0]['id']
                }
                this.setState({
                    claimantInfoMaster: result.data,
                    representative_type: representative_types,
                    help: result.data.help,
                    loading: true,
                    loaded: true
                })
            }else{

            }
        })

        if(claim_id){ 
            let claimant_info = { 
                'user_id': parseInt(user_id), 
                'access_token': token, 
                'claim_id': parseInt(claim_id) 
            }
            console.log("claimant_info: ", claimant_info)
            axios.post(api_url+'/api/v1/myclaim/detail', claimant_info, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then(res => {
                console.log("claimant_info res: ", res)
                if(res.data.success === true){
                    let label = res.data.claim_detail.claimant_name + ' ( '+res.data.claim_detail.claimant_email + ' )';
                    let claimant_name = {
                        value: res.data.claim_detail.claimant_name, 
                        id: res.data.claim_detail.claimant_id, 
                        email: res.data.claim_detail.claimant_email, 
                        label: label
                    }

                    this.setState({
                        are_you_filing_for: res.data.claim_detail.are_you_filing_for,
                        claimant_email: res.data.claim_detail.claimant_email,
                        representative_type: res.data.claim_detail.representative_type_id[0],
                        representative_type_other: res.data.claim_detail.representative_type_other,
                        claim_id: claim_id,
                        claimant_id: res.data.claim_detail.claimant_id,
                        claimant_name: res.data.claim_detail.claimant_name,
                        claimantname: res.data.claim_detail.claimant_name,
                        loading: true
                        
                    }, () => {
                        this.setState({ loaded: true })
                    })
                }else{

                }
            })   
        }
    }

    inputHandler = (event) =>{
        this.setState ({ [event.target.name] :event.target.value  })
    }

    updateSelectedOption = (event) => {
        event.preventDefault();

        let e = document.getElementById("claimant_name");
        let selected_option = e.options[e.selectedIndex].text;

        document.getElementById('selected_contact').innerHTML = selected_option;

    }

    addClaimantInfo = (event) => {
        event.preventDefault();

        //Getting title of selected option
        let e = document.getElementById("claimant_name");
        //let claimant_contact_id = e.options[e.selectedIndex].value;
        /*let claimant_contact    = e.options[e.selectedIndex].text;        
        var claimant_contact_id = e.options[e.selectedIndex].getAttribute("data-claimant_id");
        let claimant_name       = claimant_contact.split(" (")[0];
        let claimant_email      = claimant_contact.match(/\(([^)]+)\)/)[1];*/

        let claimant_contact_id = this.state.claimant_contact_id;
        let claimant_name = this.state.claimant_name;
        let claimant_email = this.state.claimant_email;

        this.setState({
            loaded: false
        })

        let claimantInfo = { 
            'user_id': parseInt(localStorage.getItem('user_id')), 
            'claim_id': parseInt(localStorage.getItem('claim_id')), 
            'access_token': localStorage.getItem('token'), 
            'are_you_filing_for': this.state.are_you_filing_for,
            'claimant_name' : claimant_name,
            'claimant_id' : parseInt(claimant_contact_id),
            'claimant_email' : claimant_email,
            'claimant_address' : "Delhi",
            'representative_type_id' : parseInt(this.state.representative_type),
            'representative_type_other' : this.state.representative_type_other
        };
        
        axios.post(api_url+'/api/v1/fileclaim/step1', claimantInfo, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data;

            if(result.success === true){
                var claim_id    = result.claim_id;
                var partner_id         = result.partner_id;

                localStorage.setItem('claim_id', claim_id);
                localStorage.setItem('partner_id', partner_id);

                this.setState({
                    claimant_added: true,
                    loaded: true
                })

                
            }else{
                this.setState({
                    errormessage: result.message,
                    loaded: true
                });
            }
            
        })
        .catch(err => {
            console.log(err);
        })
    }

    addClaimantContact = (event) => {
        event.preventDefault();

        this.setState({
            isDisabled: true
        });

        let claimantContactInfo = { 
            'user_id': parseInt(localStorage.getItem('user_id')),
            'access_token': localStorage.getItem('token'), 
            'name': this.state.name,
            'email' : this.state.email
        }

        axios.post(api_url+'/api/v1/add_claimant_contact', claimantContactInfo, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data
            
            if(result.success === true){
                var contact_id    = result.contact_id;
                this.setState({
                    SetOpen: false,
                    isDisabled: false,
                    name: '',
                    email: ''
                });

                this.componentDidMount()
            }else{
                this.setState({ errormessage: result.message, isDisabled: false }, () => {
                    setTimeout(() => this.setState({ errormessage: false }), 5000);
                })
            }
            
        })
        .catch(err => {
            console.log(err);
        })
    }

    render(){
       
        if(this.state.claimant_added){
            return(<Redirect to="/claim-info" />)
        }

        if (!localStorage.getItem('token')) {
            return (
                <Redirect to="/" />
            )
        }

        let claimant_contacts_data    = this.state.claimantContactData;
        let SetOpen                   = this.state.SetOpen;
        let representative_types      = this.state.claimantInfoMaster.representative_types;
        let claim_id                  = this.state.claim_id;
        let partner_id                = this.state.partner_id;
        let are_you_filing_for        = this.state.are_you_filing_for;
        let claimant_email            = this.state.claimant_email;
        let representative_type_id    = this.state.representative_type;
        let representative_type_other = this.state.representative_type_other;

        let claimant_options    = this.state.claimant_options;
        let find_index          = claimant_options.find(x => x.value === this.state.claimantname);
        let index_value         = claimant_options.indexOf(find_index);
        /*console.log("getBankAccounts", index_value)
        console.log("selectedOption", claimant_options[index_value])*/
        console.log("representative_type_id: ", representative_type_id)
        console.log("representative_type: ", this.state.representative_type)
        console.log("are_you_filing_for: ", are_you_filing_for)
        return(
            <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} radius={12} top="100%" color="#201e53">
                <div className="claimant-info"> 
                    <form onSubmit = {this.addClaimantInfo}>
                        <div className="row align-items-center mb-5">
                           <div className="col-md-4 col-12">
                               <label data-tip data-for='are_you_filing_for'>Are you filling for?</label>
                                {this.state.help.are_you_filing_for &&
                                    <ReactTooltip id='are_you_filing_for' >
                                        <span>{this.state.help.are_you_filing_for}</span>
                                    </ReactTooltip>
                                }
                           </div>
                           <div className="col-md-8 col-12">
                                <label className='checkbox radiobox'>Arbitration
                                    <input type="radio" name='are_you_filing_for' value="arbitration" onChange={this.inputHandler} checked={are_you_filing_for === "arbitration" ? "checked" : ""} />
                                    <span className="checkmark"></span>
                                </label>
                                <label className='checkbox radiobox'>Mediation
                                    <input type="radio" name='are_you_filing_for' value="mediation" onChange={this.inputHandler} checked={are_you_filing_for === "mediation" ? "checked" : ""} />
                                    <span className="checkmark"></span>
                                </label>
                           </div>
                        </div>
                        <div className="row align-items-center mb-5">
                           <div className="col-md-4 col-12">
                               <label data-tip data-for='claimant_name_tooltip'>Claimant Name</label>
                                {this.state.help.claimant_name &&
                                    <ReactTooltip id='claimant_name_tooltip' >
                                        <span>{this.state.help.claimant_name}</span>
                                    </ReactTooltip>
                                }
                           </div>
                           <div className="col-md-8 col-12">
                                {claimant_options && claimant_options.length ? (
                                    <Select 
                                        className="form-control w-75" 
                                        name="claimant_name" 
                                        id="claimant_name" 
                                        options={claimant_options} 
                                        defaultValue={claimant_options[index_value]} 
                                        onChange={this.claimantChange} 
                                    />
                                ): (
                                    <Select 
                                        className="form-control w-75" 
                                        name="claimant_name" 
                                        id="claimant_name" 
                                        options={claimant_options} 
                                        defaultValue={claimant_options[0]} 
                                        onChange={this.claimantChange} 
                                    />

                                )}
                                
                                {/*<select className="form-control w-75" name="claimant_name" id="claimant_name" value={this.state.claimant_name} onChange={this.inputHandler} required="required">
                                    {
                                        claimant_contacts_data !== undefined && claimant_contacts_data.length ? (
                                            <>
                                                {
                                                    claimant_contacts_data ? claimant_contacts_data.map((claimant_contact_data, index) => 
                                                        <option value={claimant_contact_data.name} data-claimant_id={claimant_contact_data.id} >{claimant_contact_data.name} ({claimant_contact_data.email})</option>
                                                    ) : null
                                                }
                                            </>
                                        ) : (
                                            <option value="" disabled selected>No record found.</option>
                                        )
                                    }
                                </select>*/}
                                {this.state.claimant_name &&
                                    <i id="selected_contact">{this.state.claimant_name} ({this.state.claimant_email})</i>
                                }
                                <button type="button" onClick={this.handleAddNew} className='add-new'>Add New</button>
                           </div>
                        </div>
                        <div className="row align-items-center mb-5">
                           <div className="col-md-4 col-12">
                               <label data-tip data-for='representative_type'>Respresentative Type</label>
                                {this.state.help.representative_type &&
                                    <ReactTooltip id='representative_type' >
                                        <span>{this.state.help.representative_type}</span>
                                    </ReactTooltip>
                                }
                           </div>
                           <div className="col-md-8 col-12">
                                <select className="form-control transparent" name="representative_type" onChange={this.inputHandler} required="required" value={this.state.representative_type}>
                                {representative_types !== undefined && representative_types.length ? (
                                <>
                                {representative_types ? representative_types.map((representative_type_item, index) =>
                                    <option value={representative_type_item.id}>{representative_type_item.name}</option>
                                    ) : null}  
                                </>  
                                ) : (
                                    <option value="" disabled selected> No record found</option>
                            )}
                                </select>
                           </div>
                        </div>
                        {representative_type_id === 2 &&
                            <div className="row align-items-center mb-5">
                               <div className="col-md-4 col-12">
                                   <label data-tip data-for='representative_type_other'>Other</label>
                                    {this.state.help.representative_type_other &&
                                        <ReactTooltip id='representative_type_other' >
                                            <span>{this.state.help.representative_type_other}</span>
                                        </ReactTooltip>
                                    }
                               </div>
                               <div className="col-md-8 col-12">
                                   <input type="text" className="form-control transparent" name="representative_type_other" value={this.state.representative_type_other} onChange={this.inputHandler} required="required"/>
                               </div>
                            </div>
                        }
                        <div className="text-center">
                            <button type="submit" className="next-btn">Next <img src={Arrow} alt='arrow'/></button>
                        </div>
                    </form>

                    {/*this.state.SetOpen === true ? <AddNewClaim openNewClaim={this.handleAddNew} closeNewClaim={this.handleCloseAddNew}/> : '' */}

                    <Modal className="new-claim--modal" show={this.state.SetOpen} onHide={this.state.SetOpen}>
                        <Modal.Body>
                            <Button className="close" onClick={this.handleCloseAddNew}>&times;</Button>
                            <h3 className="text-center">Add Claimant</h3>
                            {this.state.errormessage &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.errormessage}</div>
                            }
                            <form onSubmit = {this.addClaimantContact}>
                                <InputGroup className="mb-4">
                                    <InputGroup.Text id="Name"><img src={User} alt='user'/></InputGroup.Text>
                                    <FormControl
                                        placeholder="Name"
                                        aria-label="Enter Name"
                                        aria-describedby="Name"
                                        name="name"
                                        onChange={this.inputHandler}
                                        required="required"
                                    />
                                </InputGroup>
                                <InputGroup className="mb-4">
                                    <InputGroup.Text id="email"><img src={Mail} alt='mail'/></InputGroup.Text>
                                    <FormControl
                                        placeholder="E-mail"
                                        aria-label="Enter Email"
                                        aria-describedby="email"
                                        name="email"
                                        onChange={this.inputHandler}
                                        required="required"
                                    />
                                </InputGroup>
                                <div className="text-center">
                                    <button type="submit" className="btn-signin" disabled={this.state.isDisabled}>Submit</button>
                                </div>
                            </form>
                        </Modal.Body>
                    </Modal>

                </div>
            </Loader>
        )
    }
}
export default ClaimantInfo;