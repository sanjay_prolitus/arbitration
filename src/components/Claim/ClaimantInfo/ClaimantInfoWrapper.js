import React, { Component , useState } from 'react';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Accordion from 'react-bootstrap/Accordion';
import '../claim.scss';
import ClaimantInfo from './ClaimantInfo';
import { Redirect, match } from 'react-router';

class ClaimantInfoWrapper extends Component{

    constructor(props) {
        super(props);
        //debugger;

    }

    componentDidMount = (event) => {

    }


    claimantInfoWrapper= (event) => {

        let activeButton = false;
        let setActiveButton = false;
        localStorage.setItem('activeButton', false);
        localStorage.setItem('activeButton', false);

        const handleAccordion = (e) =>{
            if(e.target.value == "Claimant Information"){    
                //console.log(e.target.value);
                // setActiveButton(!activeButton)
            }
        }
    }

    render(){
       // debugger;
        //console.log("Wrapper Pops Data: " + this.props.match.params.id);

        return(        
            <div className="container claim-container">
           <div className="row">
               <div className="col-12 d-md-block d-none">
                   <Tabs className='claim-tabs' >
                        <Tab eventKey="claimant Information" title="Claimant Information">
                            <ClaimantInfo param={this.props.match.params}/>
                        </Tab>
                        <Tab eventKey="Claim Information" title="Claim Information" disabled="disabled">
                            
                        </Tab>
                        <Tab eventKey="Document Upload" title="Document Upload" disabled="disabled">
                            
                        </Tab>
                        <Tab eventKey="Respondent information" title="Respondent Information" disabled="disabled">
                            
                        </Tab>
                        <Tab eventKey="Claim Summary" title="Claim Summary" disabled="disabled">
                            
                        </Tab>
                    </Tabs>
               </div>
               <div className="col-12 d-md-none d-block">
                    <Accordion defaultActiveKey="0" className='claim-accordion'>
                        <Accordion.Toggle value="Claimant Information" className={this.activeButton === true ? 'accordion-button active-button' : 'accordion-button'} disabled="disabled" eventKey="0">Claimant Information</Accordion.Toggle>
                        <Accordion.Collapse eventKey="0">
                            <div className="tab-content">
                                <ClaimantInfo/>
                            </div>
                        </Accordion.Collapse>
                        
                    </Accordion>
               </div>
           </div>
       </div>
        )
    }

}
export default ClaimantInfoWrapper;