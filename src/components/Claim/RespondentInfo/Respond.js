import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Mail from './../../assets/images/mail.png';
import User from './../../assets/images/user.png';
import './respond.scss';

const Respond = () =>{  
    const [openRespond,setOpenRespond] = useState(false);
    const handleCloseRespond = () =>{
        setOpenRespond(false);
    }
    return(        
        <Modal className="respond--modal" show={openRespond} onHide={openRespond}>
            <Modal.Body>
                <Button className="close" onClick={handleCloseRespond}>&times;</Button>
                <h3 className="text-center">Add Respondent</h3>
                <InputGroup className="mb-4">
                    <InputGroup.Text id="rName"><img src={User} alt='respond'/></InputGroup.Text>
                    <FormControl
                        placeholder="Respondent Name"
                        aria-label="Enter Respondent"
                        aria-describedby="rName"
                    />
                </InputGroup>
                <InputGroup className="mb-4">
                    <InputGroup.Text id="email"><img src={Mail} alt='mail'/></InputGroup.Text>
                    <FormControl
                        placeholder="E-mail"
                        aria-label="Enter Email"
                        aria-describedby="email"
                    />
                </InputGroup>
                <InputGroup className="mb-4">
                    <InputGroup.Text id="cEmail"><img src={Mail} alt='mail'/></InputGroup.Text>
                    <FormControl
                        placeholder="Confirm E-mail"
                        aria-label="Enter confirm email"
                        aria-describedby="cEmail"
                    />
                </InputGroup>
                <div className="text-center">
                    <button type="submit" className="btn-signin">Submit</button>
                </div>
            </Modal.Body>
        </Modal>
    )
}

export default Respond;