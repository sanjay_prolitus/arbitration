import React, { useState } from 'react';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Accordion from 'react-bootstrap/Accordion';
import '../claim.scss';
import RespondentInfo from './RespondentInfo';


const RespondentInfoWrapper = () =>{

    const [activeButton,setActiveButton] = useState(false);

    const handleAccordion = (e) =>{
        if(e.target.value === "Respondent Information"){         
            setActiveButton(!activeButton)
        }
    }

    return(
       <div className="container claim-container">
           <div className="row">
               <div className="col-12 d-md-block d-none">
                   <Tabs className='claim-tabs' defaultActiveKey="Respondent information">
                        <Tab eventKey="claimant Information" title="Claimant Information" disabled="disabled">
                            
                        </Tab>
                        <Tab eventKey="Claim Information" title="Claim Information" disabled="disabled">
                            
                        </Tab>
                        <Tab eventKey="Document Upload" title="Document Upload" disabled="disabled">
                            
                        </Tab>
                        <Tab eventKey="Respondent information" title="Respondent Information">
                            <RespondentInfo/>
                        </Tab>
                        <Tab eventKey="Claim Summary" title="Claim Summary" disabled="disabled">
                            
                        </Tab>
                    </Tabs>
               </div>
               <div className="col-12 d-md-none d-block">
                    <Accordion defaultActiveKey="0" className='claim-accordion'>
                        <Accordion.Toggle value="Respondent Information" className={activeButton === true ? 'accordion-button active-button' : 'accordion-button'} disabled="disabled" eventKey="1">Respondent Information</Accordion.Toggle>
                        <Accordion.Collapse eventKey="0">
                            <div className="tab-content">
                                <RespondentInfo/>
                            </div>
                        </Accordion.Collapse>
                    </Accordion>
               </div>
           </div>
       </div>
    )
}
export default RespondentInfoWrapper;