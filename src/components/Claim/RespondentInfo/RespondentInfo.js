import React, { Component } from 'react';
import axios from 'axios';
import DataTable from 'react-data-table-component';
import { Link } from "react-router-dom";

import RespondentTable from './RespondentTable';
import Arrow from './../../../assets/images/arrow.svg';
import UserPlus from './../../../assets/images/user-plus.png';
import './respondent-info.scss';
import { Redirect } from 'react-router';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Mail from './../../../assets/images/mail.png';
import User from './../../../assets/images/user.png';
import Edit from './../../../assets/images/edit.png';
import Delete from './../../../assets/images/delete.png';
import './respond.scss';
import Loader from 'react-loader'
import RespondentAlert from '../../Alert/RespondentAlert';
import NoDataTable from '../../Common/NoDataTable';
import { ToastContainer, toast } from 'react-toastify';
import 'react-toastify/dist/ReactToastify.css';

let api_url = process.env.REACT_APP_API_URL1;

class RespondentInfo extends Component {
  constructor(props) {
    super(props);
    this.state = {
      showModel: false,
      goBack: false,
      goNext: false,
      showEditModel: false,
      errorMsg: '',
      step4_data: '',
      claim_id: '',
      name: '',
      email: '',
      edit_name: '',
      edit_email: '',
      confirm_email: '',
      formValidateError: '',
      formValidateError0: '',
      formValidateError1: '',
      formValidateError2: '',
      formValidateError3: '',
      respondent_lists: '',
      tables_data: '',
      selectedIndex: '',
      loaded: false,
      succmessage: false
    };

    this.openRespond        = this.openRespond.bind(this);
    this.handleCloseRespond = this.handleCloseRespond.bind(this);
    this.handleCloseEditRespondent = this.handleCloseEditRespondent.bind(this);

  }

    componentDidMount(){
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');

        if(user_id){
            let claim_id = '';
            if(localStorage.getItem("claim_id")){
                claim_id = localStorage.getItem("claim_id");
            }
            this.myClaimDetail(user_id, token, claim_id)
            this.respondentTableData(user_id, token, claim_id)
        }
    }

    // Go to previous screen
    goBack = (e) => {
        e.preventDefault();
        this.setState({
            goBack: true
        });
    }

    // Go to previous screen
    goNext = (e) => {
        e.preventDefault();

        this.fileclaimStep4Submit(e);
    }

    openRespond(e) {
        this.setState({
            showModel: true
        })
    }

    handleCloseRespondent(e) {
        this.setState({
            showModel: false
        })
    }

    handleCloseEditRespondent(e) {
        this.setState({
            showEditModel: false
        })
    }

    handleCloseRespond(e) {
        this.setState({
            showModel: false,
            name: '',
            email: '',
            confirm_email: ''
        })
    }

    inputHandler = (event) =>{

        this.setState ({ [event.target.name] : event.target.value })
    }

    fileclaimStep4Submit (e) {
        e.preventDefault();
        let access_token        = localStorage.getItem('token');
        let user_id             = localStorage.getItem('user_id');
        
        let claim_id            =  localStorage.getItem('claim_id');
        let name                =  this.state.name ? this.state.name : '';
        let email               =  this.state.email ? this.state.email : '';
        let confirm_email       =  this.state.confirm_email ? this.state.confirm_email : '';

        let case_respondent_ids = JSON.parse(localStorage.getItem('respondent_list'));
        
        /*let case_respondent_ids = [
          {
              "name": name,
              "email": email
          }
        ]*/

        let fileclaim_step4 = {
            "user_id": parseInt(user_id),
            "access_token": access_token,
            "claim_id": parseInt(claim_id),
            "case_respondent_ids": case_respondent_ids
        }

        if(case_respondent_ids[0]){
            this.setState({
              loaded: false
            })
              axios.post(api_url+'/api/v1/fileclaim/step4', fileclaim_step4, {
                  headers: {
                      'Content-Type': 'text/plain'
                  }
              })
              .then( res => {
                  let result = res.data
                  
                  if(result.success === true){
                      let fee_amount = res.data.fee_amount;
                      this.setState({
                          step4_data: res.data,
                          showModel: false,
                          goNext: true,
                          loaded: true
                      })

                      localStorage.setItem("fee_amount", fee_amount);   
                      localStorage.removeItem('respondent_list')
                  }else{
                      /*this.setState({ goNext: true, errorMsg: "Somthing went wrong!!" }, () => {
                          setTimeout(() => this.setState({ errorMsg: false }), 5000);
                      })*/
                      toast.error('Please add respondent.', {
                        position: "top-right",
                        autoClose: 3000,
                        hideProgressBar: false,
                        closeOnClick: true,
                        pauseOnHover: true,
                        draggable: true,
                        progress: undefined,
                      });
                  }
              })
              .catch(err => {
                  console.log(err)
              }) 
        }else {
            /*this.setState({ formValidateError: "Please add respondent." }, () => {
                setTimeout(() => this.setState({ formValidateError: false }), 5000);
            })*/

            toast.error('Please add respondent.', {
              position: "top-right",
              autoClose: 3000,
              hideProgressBar: false,
              closeOnClick: true,
              pauseOnHover: true,
              draggable: true,
              progress: undefined,
            });
          
        }
    }

    myClaimDetail = (user_id, token, claim_id) => {

        let request = {
            "user_id": parseInt(user_id),
            "access_token": token,
            "claim_id": parseInt(claim_id)
        }

        axios.post(api_url+'/api/v1/myclaim/detail', request, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data
            
            if(result.success === true){
              let respondents = res.data.respondent;
              
              let respo_data;
              let respo_grid = [];
              if(respondents !== undefined){ 
                let data_keys = Object.keys(respondents);
                data_keys.map( (key, i) => (

                  respo_data = {
                    name: respondents[key]['name'], 
                    email: respondents[key]['email'] 
                  },
                  respo_grid.push(respo_data)
                  ))

                localStorage.setItem('respondent_list', JSON.stringify(respo_grid));
              }

            }else{
                this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                    setTimeout(() => this.setState({ errorMsg: false }), 5000);
                })
            }
        })
        .catch(err => {
            console.log(err)
        })
    }

    // fileclaim step 4 submit
    addRespondent = (event) => {
        event.preventDefault();
        let access_token        = localStorage.getItem('token');
        let user_id             = localStorage.getItem('user_id');
        let user_email          = localStorage.getItem('user_email');
        
        let claim_id            =  localStorage.getItem('claim_id');
        let name                =  this.state.name ? this.state.name : '';
        let email               =  this.state.email ? this.state.email : '';
        let confirm_email       =  this.state.confirm_email ? this.state.confirm_email : '';
        
        let case_respondent_ids = [
          {
              "name": name,
              "email": email
          }
        ]

        let fileclaim_step4 = {
            "user_id": parseInt(user_id),
            "access_token": access_token,
            "claim_id": parseInt(claim_id),
            "case_respondent_ids": case_respondent_ids
        }

        if(name && email && confirm_email){
          if(email === confirm_email){
            if(email !== user_email){

                let respondent_list = JSON.parse(localStorage.getItem('respondent_list'));
                let respondentList = {
                    "name": name,
                    "email": email
                };
                if(respondent_list){                
                    respondent_list.push(respondentList);
                    localStorage.setItem('respondent_list', JSON.stringify(respondent_list));
                }else{
                    respondentList = [respondentList];
                    localStorage.setItem('respondent_list', JSON.stringify(respondentList));
                }


                this.setState({ name: '', email: '', confirm_email: '' }, () => {
                    setTimeout(() => this.setState({ showModel: false }), 1000);
                })

                /*this.setState({ success_message: "Successfully added." }, () => {
                    setTimeout(() => this.setState({ success_message: false, showModel: false }), 3000);
                })*/

                toast.success('Successfully added.', {
                  position: "top-right",
                  autoClose: 3000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });

            } else {
                toast.error('The login user cannot make himself a respondent, please use a different email address.', {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
            }
          } else {
            toast.error('Email not matched.', {
                position: "top-right",
                autoClose: 3000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
              });
          }
        }else {
          if(name == ''){
              this.setState({ formValidateError0: "This field is required." }, () => {
                  setTimeout(() => this.setState({ formValidateError0: false }), 5000);
              })
          }
          if(email == ''){
              this.setState({ formValidateError1: "This field is required." }, () => {
                  setTimeout(() => this.setState({ formValidateError1: false }), 5000);
              })
          }
          if(confirm_email == ''){
              this.setState({ formValidateError2: "This field is required." }, () => {
                  setTimeout(() => this.setState({ formValidateError2: false }), 5000);
              })
          }
        }
    }

    // Get Respondent table data
    respondentTableData = (user_id, token, claim_id) => {

        let respondent_list   = localStorage.getItem('respondent_list');
        respondent_list       = JSON.parse(respondent_list);
        
        if(respondent_list){
            this.setState({
                respondent_lists: respondent_list,
                loading: true,
                loaded: true
            })
        }else{
          this.setState({
            loaded: true
          })
        }

        // call respondentTableData() again in set time in seconds
        this.intervalID = setTimeout(this.respondentTableData.bind(this), 1000);
                  
    }

    // Edit Attorneylist
    submitEditRespondent = (event) => {
        event.preventDefault();
        
        let user_email          = localStorage.getItem('user_email');
        let edit_name           =  this.state.edit_name ? this.state.edit_name : '';
        let edit_email          =  this.state.edit_email ? this.state.edit_email : '';
        let selectedIndex       =  this.state.selectedIndex ? this.state.selectedIndex : 0;
                    
        if(edit_name && edit_email){
            if(edit_email !== user_email){
              let respondent_list = JSON.parse(localStorage.getItem('respondent_list'));
              let respondentList = {
                  "name": edit_name,
                  "email": edit_email
              };
              respondent_list[selectedIndex] = respondentList;
              localStorage.setItem('respondent_list', JSON.stringify(respondent_list));

              //this.componentDidMount();

              this.setState({ success_message: "Successfully updated." }, () => {
                setTimeout(() => this.setState({ showEditModel: false }), 100);
              })

              /*this.setState({ success_message: "Successfully updated." }, () => {
                setTimeout(() => this.setState({ success_message: false }), 3000);
              })*/

              toast.success('Successfully updated.', {
                position: "top-right",
                autoClose: 3000,
                hideProgressBar: false,
                closeOnClick: true,
                pauseOnHover: true,
                draggable: true,
                progress: undefined,
              });
            } else {
                toast.error('The login user cannot make himself a respondent, please use a different email address.', {
                  position: "top-right",
                  autoClose: 5000,
                  hideProgressBar: false,
                  closeOnClick: true,
                  pauseOnHover: true,
                  draggable: true,
                  progress: undefined,
                });
            }  

        }else {
          if(edit_name == ''){
              this.setState({ formValidateError0: "This field is required." }, () => {
                  setTimeout(() => this.setState({ formValidateError0: false }), 5000);
              })
          }
          if(edit_email == ''){
              this.setState({ formValidateError1: "This field is required." }, () => {
                  setTimeout(() => this.setState({ formValidateError1: false }), 3000);
              })
          }
        }
    }

    removeRespondent = (index) => {
        const getSelectedIndex = JSON.parse(localStorage.getItem("respondent_list"));
        getSelectedIndex.splice(index, 1);
        localStorage.setItem("respondent_list", JSON.stringify(getSelectedIndex));   

        //this.componentDidMount();

        /*this.setState({ succmessage: "Successfully removed." }, () => {
            setTimeout(() => this.setState({ succmessage: false }), 3000);
        })*/

        toast.success('Successfully removed.', {
          position: "top-right",
          autoClose: 3000,
          hideProgressBar: false,
          closeOnClick: true,
          pauseOnHover: true,
          draggable: true,
          progress: undefined,
        });
    }

    editRespondent = (index) => {
        let respondent_list    = JSON.parse(localStorage.getItem("respondent_list"));
        let respondent_data    = respondent_list[index];

        this.setState({
            edit_name: respondent_data.name,
            edit_email: respondent_data.email,
            showEditModel: true,
            selectedIndex: index
        })
    }

    render() {

        if(this.state.goBack){
            return(
                <Redirect to="/document-upload" />
            )
        }

        if(this.state.goNext){
            return(
                <Redirect to="/claim-summary" />
            )
        }

        if(!localStorage.getItem('token')){
            return(
                <Redirect to="/" />
            )
        }

      let respondent_info = this.state.tables_data;

      let respondent_lists = this.state.respondent_lists;

      let columns = [
            {
                name: 'Respondent Name',
                selector: row => row.name,
                sortable: true
            },
            {
                name: 'Email',
                selector: row => row.email,
                sortable: true
            },
            {
                name: 'Action',
                selector: row => row.action,
            }           
        ];

        let list_data = [];
        if(respondent_lists !== undefined){
            let respondent_list = Object.keys(respondent_lists); 
            let $this = this;
            respondent_list.forEach(function(key, i) {

                let respondent_name = respondent_lists[key].name;
                let action_col;
                if(respondent_name){
                    action_col = 
                    <>
                        <img src={Edit} alt='Edit' onClick={() => $this.editRespondent(i)}/>
                        <img src={Delete} alt='delete' onClick={() => $this.removeRespondent(i)}/>
                    </>
                }

                list_data.push({ 
                    name: respondent_lists[key].name, 
                    email: respondent_lists[key].email,
                    action: action_col
                })

            });
        }


      return(
        <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} radius={12} top="100%" color="#201e53">
         <div className="respondent-info">            
              <div className='row'>
                  <div className="col-12 text-right">
                      <Link onClick={e => e.preventDefault()} className="add-respondent" onClick={this.openRespond}><img src={UserPlus} alt='user'/>Add Respondent</Link>
                  </div>
              </div>

                {this.state.formValidateError &&
                    <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError}</div>
                }
                
                {this.state.success_message &&
                    <div className="success-message" style={{ color:"green"}}>{this.state.success_message}</div>
                }

                <>
                    
                    {list_data.length > 0 &&
                        <DataTable
                            columns={columns}
                            data={list_data}
                            fixedHeader
                        />
                    }

                    { list_data.length == 0 &&
                        <div className="document-table table-responsive">
                          <NoDataTable columns={columns} title="No Data Available" />
                        </div>
                    }  

                </>

             <div className="text-center">
                  <button type="button" className="back-btn" onClick={this.goBack}><img src={Arrow} alt='arrow'/>Back</button>
                  <button type="button" className="next-btn" onClick={this.goNext}>Next <img src={Arrow} alt='arrow'/></button>
              </div>

              <Modal className="respond--modal" show={this.state.showModel}>
                <Modal.Body>
                    <Button className="close" onClick={this.handleCloseRespond}>&times;</Button>
                    <h3 className="text-center">Add Respondent</h3>
                    {this.state.errorMsg &&
                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.errorMsg}</div>
                    }
                    <form onSubmit = {this.addRespondent}>
                      <InputGroup className="mb-4">
                          <InputGroup.Text id="rName"><img src={User} alt='respond'/></InputGroup.Text>
                          <FormControl
                              placeholder="Respondent Name"
                              aria-label="Enter Respondent"
                              aria-describedby="rName"
                              name="name"
                              type="text"
                              value={this.state.name}
                              onChange={this.inputHandler}
                              required="required"
                          />
                      </InputGroup>
                      {this.state.formValidateError0 &&
                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError0}</div>
                      }
                      <InputGroup className="mb-4">
                          <InputGroup.Text id="email"><img src={Mail} alt='mail'/></InputGroup.Text>
                          <FormControl
                              placeholder="E-mail"
                              aria-label="Enter Email"
                              aria-describedby="email"
                              name="email"
                              type="email"
                              value={this.state.email}
                              onChange={this.inputHandler}
                              required="required"
                          />
                      </InputGroup>
                      {this.state.formValidateError1 &&
                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError1}</div>
                      }
                      <InputGroup className="mb-4">
                          <InputGroup.Text id="cEmail"><img src={Mail} alt='mail'/></InputGroup.Text>
                          <FormControl
                              placeholder="Confirm E-mail"
                              aria-label="Enter confirm email"
                              aria-describedby="cEmail"
                              name="confirm_email"
                              type="confirm_email"
                              value={this.state.confirm_email}
                              onChange={this.inputHandler}
                              required="required"
                          />
                      </InputGroup>
                      {this.state.formValidateError2 &&
                        <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError2}</div>
                      }
                      {this.state.formValidateError3 &&
                          <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError3}</div>
                      }
                      {this.state.errorMsg &&
                          <div className="form-validate-error" style={{ color:"red"}}>{this.state.errorMsg}</div>
                      }
                      <div className="text-center">
                          <button type="submit" className="btn-signin">Submit</button>
                      </div>
                    </form>
                </Modal.Body>
              </Modal>

              <Modal className="respond--modal" show={this.state.showEditModel}>
                    <Modal.Body>
                        <Button className="close" onClick={this.handleCloseEditRespondent}>&times;</Button>
                        <h3 className="text-center">Edit Respondent</h3>
                        {this.state.errorMsg &&
                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.errorMsg}</div>
                        }
                          <InputGroup className="mb-4">
                              <InputGroup.Text id="rName"><img src={User} alt='respond'/></InputGroup.Text>
                              <FormControl
                                  placeholder="Respondent Name"
                                  aria-label="Enter Respondent"
                                  aria-describedby="rName"
                                  name="edit_name"
                                  type="text"
                                  value={this.state.edit_name}
                                  onChange={this.inputHandler}
                              />
                          </InputGroup>
                          {this.state.formValidateError0 &&
                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError0}</div>
                          }
                          <InputGroup className="mb-4">
                              <InputGroup.Text id="email"><img src={Mail} alt='mail'/></InputGroup.Text>
                              <FormControl
                                  placeholder="E-mail"
                                  aria-label="Enter Email"
                                  aria-describedby="email"
                                  name="edit_email"
                                  type="email"
                                  value={this.state.edit_email}
                                  onChange={this.inputHandler}
                              />
                          </InputGroup>
                          {this.state.formValidateError1 &&
                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError1}</div>
                          }
                          {this.state.formValidateError2 &&
                              <div className="form-validate-error" style={{ color:"red"}}>{this.state.formValidateError2}</div>
                          }
                          {this.state.errorMsg &&
                              <div className="form-validate-error" style={{ color:"red"}}>{this.state.errorMsg}</div>
                          }
                          <div className="text-center">
                              <button type="submit" className="btn-signin" onClick={this.submitEditRespondent}>Submit</button>
                          </div>
                        
                    </Modal.Body>
              </Modal>

              {this.state.succmessage &&
                  <RespondentAlert registerSucc={this.state.succmessage} />
              }
              <ToastContainer />
         </div>
        </Loader>
      )

    }
}
export default RespondentInfo;