import React, { useState } from 'react';
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Accordion from 'react-bootstrap/Accordion';
import './claim.scss';
import ClaimantInfo from './ClaimantInfo/ClaimantInfo';
import ClaimInfo from './ClaimInfo/ClaimInfo';
import DocumentUpload from './DocumentUpload/DocumentUpload';
import RespondentInfo from './RespondentInfo/RespondentInfo';
import Summary from './Summary/Summary';

const Claim = () =>{

    const [activeButton,setActiveButton] = useState(false);

    const handleAccordion = (e) =>{
        if(e.target.value == "Claimant Information"){    
            console.log(e.target.value);
            setActiveButton(!activeButton)
        }
        else if(e.target.value == "Claim Information"){         
            setActiveButton(!activeButton)
        }
        else if(e.target.value == "Document Upload"){         
            setActiveButton(!activeButton)
        }
        else if(e.target.value === "Respondent Information"){         
            setActiveButton(!activeButton)
        }
        else if(e.target.value === "Claim Summary"){         
            setActiveButton(!activeButton)
        }
    }

    return(
       <div className="container claim-container">
           <div className="row">
               <div className="col-12 d-md-block d-none">
                   <Tabs className='claim-tabs' >
                        <Tab eventKey="claimant Information" title="Claimant Information">
                            <ClaimantInfo/>
                        </Tab>
                        <Tab eventKey="Claim Information" title="Claim Information">
                            <ClaimInfo/>
                        </Tab>
                        <Tab eventKey="Document Upload" title="Document Upload">
                            <DocumentUpload/>
                        </Tab>
                        <Tab eventKey="Respondent information" title="Respondent Information">
                            <RespondentInfo/>
                        </Tab>
                        <Tab eventKey="Claim Summary" title="Claim Summary">
                            <Summary />
                        </Tab>
                    </Tabs>
               </div>
               <div className="col-12 d-md-none d-block">
                    <Accordion defaultActiveKey="0" className='claim-accordion'>
                        <Accordion.Toggle value="Claimant Information" className={activeButton === true ? 'accordion-button active-button' : 'accordion-button'} onClick={handleAccordion} eventKey="0">Claimant Information</Accordion.Toggle>
                        <Accordion.Collapse eventKey="0">
                            <div className="tab-content">
                                <ClaimantInfo/>
                            </div>
                        </Accordion.Collapse>
                        <Accordion.Toggle value="Claim Information" eventKey="1" className={activeButton === true ? 'accordion-button active-button' : 'accordion-button'}>Claim Information</Accordion.Toggle>
                        <Accordion.Collapse eventKey="1">
                            <div className="tab-content">
                                <ClaimInfo/>
                            </div>
                        </Accordion.Collapse>
                        <Accordion.Toggle value="Document Upload" eventKey="3" className={activeButton === true ? 'accordion-button active-button' : 'accordion-button'}>Document Upload</Accordion.Toggle>
                        <Accordion.Collapse eventKey="3">
                            <div className="tab-content">
                                <DocumentUpload/>
                            </div>
                        </Accordion.Collapse>
                        <Accordion.Toggle value="Respondent Information" eventKey="4" className={activeButton === true ? 'accordion-button active-button' : 'accordion-button'}>Respondent Information</Accordion.Toggle>
                        <Accordion.Collapse eventKey="4">
                            <div className="tab-content">
                                <RespondentInfo/>
                            </div>
                        </Accordion.Collapse>
                        <Accordion.Toggle value="Claim Summary" eventKey="5" className={activeButton === true ? 'accordion-button active-button' : 'accordion-button'}>Claim Summary</Accordion.Toggle>
                        <Accordion.Collapse eventKey="5">
                            <div className="tab-content">
                                <Summary/>
                            </div>
                        </Accordion.Collapse>
                    </Accordion>
               </div>
           </div>
       </div>
    )
}
export default Claim;