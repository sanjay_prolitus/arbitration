import React, { Component } from 'react';
import DataTable from 'react-data-table-component';
import Excel from './../../../assets/images/excel.png';
import Pdf from './../../../assets/images/pdf.png';
import './document-table.scss';
import axios from 'axios';
import { Link } from "react-router-dom";
import NoDataTable from '../../Common/NoDataTable';

let api_url = process.env.REACT_APP_API_URL1;

class DocumentTable extends Component {
    constructor(props) {
        super(props);
        this.state = {
            document_url: ''
        };
    }

    // Download Docs 
    downloadDocs = (claim_id, document_id, payment_line_id, doc_attachment_type ) => {
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');

        let get_file_detail = doc_attachment_type.split('/');
        let type = get_file_detail[0];
        let ext  = get_file_detail[1];

        let file_request    = { 
            'user_id': parseInt(user_id), 
            'access_token': token, 
            'claim_id': claim_id, 
            'document_id': document_id, 
            'payment_line_id': payment_line_id
        }

        axios.post(api_url+'/api/v1/document/download', file_request, {
            //responseType: 'blob',
            headers: {
                'Content-Type': 'application/'+type+'/*'
            }
        })
        .then(res => {   

            if(res.data.success === true){
                let document_url = res.data.document_url;
                window.open(api_url+document_url, '_blank');
            }
        })
        
    }

    render() {

        let document_data   = this.props.document_data;
        let loading         = this.props.loading;
        let claim_id        = this.props.claim_id;
        let document_url    = this.state.document_url;
        
        
        let columns = [
            {
                name: 'Docket#',
                selector: row => row.docket,
                sortable: true,
                minWidth: "110px"
            },
            {
                name: 'Document',
                selector: row => row.document,
                sortable: true,
                minWidth: "150px"
            },
            {
                name: 'Document Type',
                selector: row => row.document_type,
                sortable: true,
                minWidth: "200px"
            },
            {
                name: 'Description',
                selector: row => row.description,
                sortable: true,
                minWidth: "150px"
            },
            {
                name: 'Filled Date',
                selector: row => row.filled_date,
                sortable: true,
                minWidth: "150px"
            },
            {
                name: 'Filled By',
                selector: row => row.filled_by,
                sortable: true,
                minWidth: "150px"
            } ,
            {
                name: 'Status',
                selector: row => row.status,
                minWidth: "150px"
            },
            {
                name: 'Download',
                selector: row => row.download,
                minWidth: "150px"
            }           
        ];

        let list_data = [];
        if(document_data !== undefined){
            let document_list = Object.keys(document_data); 
            let $this = this;
            document_list.forEach(function(key, i) {

                let document_name = document_data[key].docket_no;
                let action_col;
                if(document_name){
                    action_col = 
                    <>
                        <span className="filter-field" style={{ backgroundColor: "unset",boxShadow: "none" }}>
                            <Link onClick={e => e.preventDefault()} className="clear-btn" onClick={() => $this.downloadDocs(claim_id, document_data[key].id, document_data[key].payment_line_id, document_data[key].doc_attachment_type)}>Download</Link>
                        </span>
                    </>
                }

                list_data.push({ 
                    docket: document_data[key].docket_no, 
                    document: document_data[key].doc_type,
                    document_type: document_data[key].document_configuration_id[1],
                    description: document_data[key].description,
                    filled_date: document_data[key].filed_date,
                    filled_by: document_data[key].filed_by[1],
                    status: document_data[key].status,
                    download: action_col
                })

            });
        }

        return(
            <>
                {list_data.length > 0 &&
                    <DataTable
                        columns={columns}
                        data={list_data}
                        fixedHeader
                    />
                }

                { list_data.length == 0 &&
                    <div className="document-table table-responsive">
                        <NoDataTable columns={columns} title="No Data Available" />
                    </div>
                }  
           </>
        )
    }
}
export default DocumentTable;