import React, {Component} from 'react';
import DocumentTable from './DocumentTable';
import Attorney from './Attorney/Attorney';
import AttorneyTable from './Attorney/AttorneyTable';
import Filter from './Filter/Filter';
import Invoice from './Invoice/Invoice';
import './file-details.scss';
import axios from 'axios';
import Loader from 'react-loader'
import { Redirect } from 'react-router';
import DatePicker from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";
import Breadcrumb from 'react-bootstrap/Breadcrumb'

let api_url = process.env.REACT_APP_API_URL1;

class FileDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
          claim_id: '',
          tables_data: '',
          search_data: '',
          loading: false,
          loaded: false,
          doc_type: '',
          name: '',
          filed_from: '',
          filed_to: '',
          filed_froms: '',
          filed_tos: '',
          status: '',
          filed_by: '',
          sort_by: '',
          clear_filter: false,
          date: new Date()
      };

      this.dateChangedFrom  = this.dateChangedFrom.bind(this);
      this.dateChangedTo    = this.dateChangedTo.bind(this);

    }

    componentDidMount(){

        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        let claim_id        = this.props.match.params.id;

        if(token !== '' && user_id !== '' && claim_id !== ''){
            this.claimDetailsData(user_id, token, claim_id)
        }
    }

    dateChangedFrom(d){
        //let date = d.toLocaleDateString();
        //let dates = d.toISOString().slice(0,10);
        //alert(dates)
        
        let date    = d.getDate();
        let month   = d.getMonth();
        let year    = d.getFullYear();
        let dateString = year + "-" +(month + 1) + "-" + date;
        /*var result = d.toISOString().split('T')[0];
        alert(result)*/
        this.setState({
            filed_froms: dateString,
            filed_from: d
        });
    }

    dateChangedTo(d){
        //let dates = d.toISOString().slice(0,10);
        let date    = d.getDate();
        let month   = d.getMonth();
        let year    = d.getFullYear();
        let dateString = year + "-" +(month + 1) + "-" + date;
        this.setState({
            filed_to: d,
            filed_tos: dateString
        });
    }

    inputHandler = (event) =>{

        this.setState ({ [event.target.name] : event.target.value })
    }

    // Get claim deatil data
    claimDetailsData = (user_id, token, claim_id) => {
        
        let request = {
            "user_id": parseInt(user_id),
            "access_token": token,
            "claim_id": parseInt(claim_id)
        }

        axios.post(api_url+'/api/v1/myclaim/detail', request, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data
            
            if(result.success === true){
                this.setState({
                    tables_data: res.data,
                    loading: true,
                    loaded: true
                })

            }else{
                this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                    setTimeout(() => this.setState({ errorMsg: false }), 5000);
                })
            }
        })
        .catch(err => {
            console.log(err)
        })
          
    }

    // Search filter
    submitFilter = (e) => {
        e.preventDefault();
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        let claim_id        = this.props.match.params.id;
        let doc_type        = this.state.doc_type;
        let name            = this.state.name;
        let filed_from      = this.state.filed_froms;
        let filed_to        = this.state.filed_tos;
        let filed_by        = this.state.filed_by;
        let status          = this.state.status;
        let sort_by         = this.state.sort_by;

        let request = {
            "user_id": parseInt(user_id),
            "access_token": token,
            "claim_id": parseInt(claim_id),
            "name": name,
            "doc_type": doc_type,
            "filed_from": filed_from,
            "filed_to": filed_to,
            "filed_by": filed_by,
            "sort_by": sort_by,
            "status": status
        }

        axios.post(api_url+'/api/v1/filter/document', request, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data

            if(result.success === true){
                this.setState({
                    search_data: res.data,
                    loading: true,
                    loaded: true,
                    clear_filter: false
                })

            }else{
                this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                    setTimeout(() => this.setState({ errorMsg: false }), 5000);
                })
            }
        })
        .catch(err => {
            console.log(err)
        })
          
    }

    clearFilter = (e) => {
        e.preventDefault();
        this.setState({
            clear_filter: true,
            doc_type: '',
            name: '',
            filed_from: '',
            filed_to: '',
            sort_by: '',
            filed_by: '',
            status: '',
            filledby: ''
        }, () => {
            let user_id         = localStorage.getItem('user_id');
            let token           = localStorage.getItem('token');
            let claim_id        = this.props.match.params.id;
            this.claimDetailsData(user_id, token, claim_id);
        })
    }

    render() {

        if ( !localStorage.getItem('user_id') ) {
            return (
                <Redirect to="/" />
            )
        }

        let get_all_data            = this.state.tables_data;
        let get_invoice_data        = get_all_data.claim_detail;
        let get_respondent_data     = get_all_data.respondent;
        let document_data           = get_all_data.docket;
        let file_claim_applicable   = get_all_data.file_claim_applicable;
        let expenses_count          = get_all_data.expenses_count;
        let timesheet_count         = get_all_data.timesheet_count;
        let invoice_count           = get_all_data.invoice_count;

        let search_data         = this.state.search_data;
        let get_document_data;

        if(search_data && !this.state.clear_filter){
            get_document_data = search_data.docket;
        }else{
            get_document_data = document_data;
        }
        let claim_id;
        if(get_invoice_data){
            claim_id = get_invoice_data.id;
        }

        return(
            <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} radius={12} color="#201e53">
                <Breadcrumb>
                    <div className="container">
                        <div className="row">
                            <Breadcrumb.Item href="/dashboard">My Claims</Breadcrumb.Item>
                            <Breadcrumb.Item href="#">Claim Detail</Breadcrumb.Item>
                        </div>
                    </div>
                </Breadcrumb>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="file-details">
                                <Invoice invoice_data={get_invoice_data} file_claim_applicable={file_claim_applicable} expenses_count={expenses_count} timesheet_count={timesheet_count} invoice_count={invoice_count} />
                                <AttorneyTable respondent_data={get_respondent_data} loading={this.state.loading}/>
                                
                                <div className='row mobile-heading'>
                                    <div className="col-md-6 col-12">
                                        <h2>Docket</h2>
                                    </div>
                                </div>

                                <div className="row filters">
                                    <form className="w-100" onSubmit={this.submitFilter}>
                                        <div className="col-lg-12 col-12">                                        
                                            <div className='filter-field'>
                                                <div className="filter-titles">
                                                    <label>Filter</label>
                                                    <input type="text" className="form-control" name="name" value={this.state.name} onChange={this.inputHandler} />
                                                </div>
                                                <div className="filter-titles">
                                                    <select className="form-control" name="doc_type" value={this.state.doc_type} onChange={this.inputHandler}>
                                                        <option value="">Type</option>
                                                        <option value="primary">Primary</option>
                                                        <option value="supporting">Supporting</option>
                                                    </select>
                                                </div>
                                                <div className="filter-titles">
                                                    <input type="text" className="form-control" name="filed_by" Placeholder="Filled By" autoComplete="off" value={this.state.filed_by} onChange={this.inputHandler} />
                                                </div>
                                                <div className="filter-titles">
                                                    <DatePicker className="form-control" name="filed_from" selected={this.state.filed_from} dateFormat="yyyy-MM-dd" autoComplete="off" placeholderText="Date From" onChange={this.dateChangedFrom} />
                                                </div>
                                                <div className="filter-titles">
                                                    <DatePicker className="form-control" name="filed_to" selected={this.state.filed_to} placeholderText="Date To" autoComplete="off" dateFormat="yyyy-MM-dd" minDate={new Date(this.state.filed_from)} onChange={this.dateChangedTo} />
                                                </div>
                                                <div className="filter-titles">
                                                    <select className="form-control" name="sort_by" value={this.state.sort_by} onChange={this.inputHandler}>
                                                        <option value="">Sort By</option>
                                                        <option value="asc">ASC</option>
                                                        <option value="desc">DESC</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <div className="col-lg-12 col-12 text-right">
                                            <div className='filter-field filter-button'>
                                                <h2 className="inline-heading desktop-heading">Docket</h2>
                                                <button type="submit" className="submit-btn">Submit</button>
                                                <button type="button" className="clear-btn" onClick={this.clearFilter}>Clear</button>
                                            </div>
                                        </div>
                                    </form>
                                </div>

                                <DocumentTable document_data={get_document_data} claim_id={claim_id} loading={this.state.loading}/>
                            </div>
                       </div>
                   </div>
               </div>
            </Loader>
        )
    }
}
export default FileDetails;