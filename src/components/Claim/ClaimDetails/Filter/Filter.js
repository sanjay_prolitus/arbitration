import React from 'react';
import './filter.scss';

const Filter = () =>{
    return(
        <div className="row mb-5">
            <div className="col-lg-10 col-12">
                <div className='filter-field'>
                    <label>Filter1</label>
                    <input type="text" className="form-control"/>
                    <select className="form-control">
                        <option value="type">Type</option>
                    </select>
                    <select className="form-control">
                        <option value="filledby">Filled By</option>
                    </select>
                    <input type="text" className="form-control" placeholder="Filled Date"/> 
                    <button type="button" className="submit-btn">Submit</button>
                    <button type="button" className="clear-btn">Clear</button>
                </div>
            </div>
            <div className="col-lg-2 col-12 text-right">
                <select className="form-control sort-by">
                    <option value="sortby">Sort By</option>
                </select>
            </div>
        </div>
    )
}
export default Filter;