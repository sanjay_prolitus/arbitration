import React, { Component } from 'react';
import DataTable from 'react-data-table-component';
import './attorney-table.scss';
import NoDataTable from '../../../Common/NoDataTable';

class RespondentTable extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {

        let respondent_data = this.props.respondent_data;
        let loading         = this.props.loading;

        let columns = [
            {
                name: 'Respondent Name',
                selector: row => row.name,
                sortable: true
            },
            {
                name: 'Email',
                selector: row => row.email,
                sortable: true,
                minWidth: "150px"
            }         
        ];

        let list_data = [];
        if(respondent_data !== undefined){
            let respondent_list = Object.keys(respondent_data); 
            respondent_list.forEach(function(key, i) {
                list_data.push({ 
                    name: respondent_data[key].name, 
                    email: respondent_data[key].email
                })

            });
        }

        return(
           <div className="document-table table-responsive">
                {list_data.length > 0 &&
                    <DataTable
                        columns={columns}
                        data={list_data}
                        fixedHeader
                    />
                }

                { list_data.length == 0 &&
                    <NoDataTable columns={columns} title="No Data Available" />
                }    
           </div>
        )
    }
}
export default RespondentTable;