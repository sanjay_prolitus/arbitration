import React, { Component } from 'react';
import Search from './../../../../assets/images/search-plus.png';
import File from './../../../../assets/images/file.png';
import TimeSheet from './../../../../assets/images/timesheet.png';
import Expense from './../../../../assets/images/expense.png';
import './invoice.scss';
import { Link } from "react-router-dom";
import axios from 'axios';
import { Redirect } from 'react-router';
import ReactPaginate from 'react-paginate';
import Loader from 'react-loader'
import NoDataTable from '../../../Common/NoDataTable';
import DataTable from 'react-data-table-component';
import Breadcrumb from 'react-bootstrap/Breadcrumb'

let api_url = process.env.REACT_APP_API_URL1;

class InvoiceList extends Component {

    constructor(props) {
        super(props);
        this.state = {
            invoice_list : '',
            loaded: false,
            page_no: false
        };

    }

    componentDidMount(){
        let user_id     = localStorage.getItem('user_id');
        let token       = localStorage.getItem('token');
        let claim_id    = this.props.match.params.id;

        if(token && user_id ){
            this.getInvoiceList(user_id, token, claim_id)
        }
    }

    // get Invoice List
    getInvoiceList = (user_id, token, claim_id) => {
        if(user_id){
            let invoice_request    = { 
                'user_id': parseInt(user_id), 
                'access_token': token, 
                'page_no': (this.state.page_no) ? this.state.page_no : 1, 
                'claim_id': parseInt(claim_id)
            }
            axios.post(api_url+'/api/v1/invoice/list', invoice_request, {
                headers: { 
                    'Content-Type': 'text/plain'
                }
            })
            .then(res => {
                
                if(res.data.status !== 'true'){
                    this.setState({
                        invoice_list: res.data,
                        loading: true,
                        loaded: true
                    })
                }else{

                }
            })
        }
    }

    handlePageClick = (data) => {
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');

        let page_no = data.selected;

        this.setState({ page_no: page_no+1 }, () => {
          this.getInvoiceList(user_id, token);
      });
    };

    render() {

        if (!localStorage.getItem('token')) {
            return (
                <Redirect to="/" />
            )
        }

        let invoice_list_data   = this.state.invoice_list.invoice_list;
        let total_count         = this.state.invoice_list.total_count;
        let records_per_page    = this.state.invoice_list.records_per_page;
        let page_count          = total_count / records_per_page;

        let columns = [
            {
                name: 'Claim#',
                selector: row => row.claim,
                sortable: true,
                minWidth: "170px"
            },
            {
                name: 'Invoice#',
                selector: row => row.invoice_number,
                sortable: true,
                minWidth: "170px"
            },
            {
                name: 'Claimant/ Respondent',
                selector: row => row.claimaint_or_respondent,
                sortable: true,
                minWidth: "200px"
            },
            {
                name: 'Date',
                selector: row => row.invoice_date,
                sortable: true
            },
            {
                name: 'Total',
                selector: row => row.total,
                sortable: true
            },
            {
                name: 'Status',
                selector: row => row.state,
                sortable: true
            },
            {
                name: 'Invoice',
                selector: row => row.invoice,
                sortable: true,
                minWidth: "160px"
            }         
        ];

        let list_data = [];
        if(invoice_list_data !== undefined){
            let invoice_list = Object.keys(invoice_list_data); 
            invoice_list.forEach(function(key, i) {
                list_data.push({ 
                    claim: invoice_list_data[key].claim, 
                    invoice_number: invoice_list_data[key].invoice_number,
                    claimaint_or_respondent: invoice_list_data[key].claimaint_or_respondent, 
                    invoice_date: invoice_list_data[key].invoice_date,
                    total: invoice_list_data[key].total +" USD", 
                    state: invoice_list_data[key].state,
                    invoice: <Link className="add-primary" to={ "/invoice-detail/" + invoice_list_data[key].invoice_id}>View</Link>
                })

            });
        }

        return(
            <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} radius={12} color="#201e53">
                <Breadcrumb>
                    <div className="container">
                        <div className="row">
                            <Breadcrumb.Item href="/dashboard">My Claims</Breadcrumb.Item>
                            <Breadcrumb.Item href="#">Invoice Claim List</Breadcrumb.Item>
                        </div>
                    </div>
                </Breadcrumb>
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="invoice claim-list">
                                <div className="claim-list-table table-responsive">
                                    
                                    {list_data.length > 0 &&
                                        <DataTable
                                            columns={columns}
                                            data={list_data}
                                            fixedHeader
                                        />
                                    }

                                    { list_data.length == 0 &&
                                        <NoDataTable columns={columns} title="No Data Available" />
                                    }  
                                    {invoice_list_data !== undefined && invoice_list_data.length ? (
                                        <ReactPaginate
                                            previousLabel={'Prev'}
                                            nextLabel={'Next'}
                                            breakLabel={'...'}
                                            breakClassName={'break-me'}
                                            pageCount={page_count}
                                            marginPagesDisplayed={2}
                                            pageRangeDisplayed={5}
                                            onPageChange={this.handlePageClick}
                                            containerClassName={'pagination'}
                                            activeClassName={'active'}
                                            pageClassName={'page-item'}
                                            previousClassName={'page-item'}
                                            nextClassName={'page-item'}
                                            nextLinkClassName={'page-item'}
                                            pageLinkClassName={'page-link'}
                                            previousLinkClassName={'page-link'}
                                            nextLinkClassName={'page-link'}
                                        />
                                    ) : null}
                               </div>
                            </div>
                        </div>
                    </div>
                </div>
            </Loader>    
        )
    };
}
export default InvoiceList;