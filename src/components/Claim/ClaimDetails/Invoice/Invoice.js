import React, { Component } from 'react';
import Search from './../../../../assets/images/search-plus.png';
import File from './../../../../assets/images/file.png';
import TimeSheet from './../../../../assets/images/timesheet.png';
import Expense from './../../../../assets/images/expense.png';
import './invoice.scss';
import { Link } from "react-router-dom";

class Invoice extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {

        let invoice_data            = this.props.invoice_data ? this.props.invoice_data : '';
        let file_claim_applicable   = this.props.file_claim_applicable ? this.props.file_claim_applicable : '';
        let expenses_count          = this.props.expenses_count ? this.props.expenses_count : 0;
        let timesheet_count         = this.props.timesheet_count ? this.props.timesheet_count : 0;
        let invoice_count           = this.props.invoice_count ? this.props.invoice_count : 0;
        let invoices = invoice_data.invoices;
        let claim_status = invoice_data.status;
        let invoices_id;
        if(invoices[0]){
            invoices_id = invoices[0].id;
        }else{
            invoices_id = '';
        }
        
        return(
            <div className="invoice claim-types">
                <div className="text-right mb-4">
                    {file_claim_applicable === "yes" ? (
                        <>
                            <button type="button" className="print-btn"><Link to={`/invoice-claim-list/${ invoice_data.id }`}><img src={Search} alt='print'/>View Invoice ({invoice_count})</Link></button>
                            <button type="button" className="file-btn"><Link to={`/timesheet/${ invoice_data.id }`}><img src={TimeSheet} alt='File'/>Timesheet ({timesheet_count})</Link></button>
                            <button type="button" className="file-btn expense"><Link to={`/expenses/${ invoice_data.id }`}><img src={Expense} alt='File'/>Expenses ({expenses_count})</Link></button>
                            <button type="button" className="file-btn"><Link to={`/claim-file-detail/${ invoice_data.id }`}><img src={File} alt='File'/>File Document</Link></button>
                        </>
                    ):null}
                    {claim_status === "Disposed" &&
                        <>
                            <button type="button" className="print-btn"><Link to={`/invoice-claim-list/${ invoice_data.id }`}><img src={Search} alt='print'/>View Invoice ({invoice_count})</Link></button>
                            <button type="button" className="file-btn"><Link to={`/timesheet/${ invoice_data.id }`}><img src={TimeSheet} alt='File'/>Timesheet ({timesheet_count})</Link></button>
                            <button type="button" className="file-btn expense"><Link to={`/expenses/${ invoice_data.id }`}><img src={Expense} alt='File'/>Expenses ({expenses_count})</Link></button>
                        </>
                    }
                    {claim_status === "Draft" &&
                        <button type="button" className="file-btn"><Link to={`/claimant-info/${ invoice_data.id }`}> File Claim</Link></button>
                    }
                </div>
                <div className="row">
                    <div className="col-md-6 col-12">
                        <ul>
                            <li>
                                <label>Claim#</label>
                                <p>{invoice_data.name}</p>
                            </li>
                            <li>
                                <label>Claimant Name</label>
                                <p>{invoice_data.claimant_name}</p>
                            </li>
                            <li>
                                <label>Date</label>
                                <p>{invoice_data.date}</p>
                            </li>
                            {invoice_data.claimant_email &&
                                <li>
                                    <label>Claimant E-mail</label>
                                    <p>{invoice_data.claimant_email}</p>
                                </li>
                            }
                        </ul>
                    </div>
                    <div className="col-md-6 col-12">
                        <ul>
                            {invoice_data.claimant_mobile &&
                                <li className='no-margin'>
                                    <label>Claimant Contact</label>
                                    <p>{invoice_data.claimant_mobile}</p>
                                </li>
                            }
                            <li className='no-margin'>
                                <label>Amount of Claim</label>
                                <p>{invoice_data.claim_amount} USD</p>
                            </li>
                            {invoice_data.counter_claim_amount > 0 ? (
                                <li className='no-margin'>
                                    <label>Counter Claim Amount</label>
                                    <p>{invoice_data.counter_claim_amount} USD</p>
                                </li>
                            ): null}
                            <li className='no-margin'>
                                <label>Status</label>
                                <p>{invoice_data.status}</p>
                            </li>
                            {invoice_data.invoice &&
                                <li className='no-margin'>
                                    <label>Invoice</label>
                                    <p>{invoice_data.invoice}</p>
                                </li>
                            }
                        </ul>
                    </div>
                    {invoice_data.claim_description &&
                        <div className="col-md-12 col-12 claim-description">
                            <ul>
                                    <li className='no-margin'>
                                        <label>Claim Description</label>
                                        <p>{invoice_data.claim_description}</p>
                                    </li>
                            </ul>
                        </div>
                    }
                </div>
            </div>
        )
    }
}
export default Invoice;