import React from 'react';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import ClaimListTable from './ClaimListTable';
import Search from './../../../assets/images/search.png';
import './claim-list.scss';

const ClaimList = () =>{
    return(
       <div className="claim-list">
            <div className='row'>
                <div className="col-md-6 col-12">
                    <a href="javascript" className="add-primary">Draft <span>(2)</span></a>
                    <a href="javascript" className="add-primary">In Process <span>(2)</span></a>
                    <a href="javascript" className="add-primary">Disposed <span>(1)</span></a>
                </div>
                <div className="col-md-6 col-12">
                    <InputGroup>
                    <FormControl
                        placeholder="Search"
                        aria-label="Enter value for searching"
                        aria-describedby="search"
                    />
                    <InputGroup.Text id="search"><img src={Search} alt='search'/></InputGroup.Text>
                </InputGroup>
                </div>
            </div>
            <ClaimListTable/>
       </div>
    )
}
export default ClaimList;