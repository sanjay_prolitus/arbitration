import React, { Component } from "react";
import axios from 'axios';
import { Redirect } from 'react-router';
import './claim-list-table.scss';

let api_url = process.env.REACT_APP_API_URL1;

class ClaimListTable extends Component{

    constructor(props) {
        super(props);
        this.state = {
            claim_list : ''
        };

    }

    componentDidMount(){
        
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        
        if(token !== '' && user_id !== ''){
            this.getClaimList(user_id, token)
        }
    }

    // get Claim List
    getClaimList = (user_id, token) => {
        if(user_id){
            let claim_request    = { 'user_id': parseInt(user_id), 'access_token': token, 'page_no': (this.state.page_no) ? this.state.page_no : 0 }
            
            axios.post(api_url+'/api/v1/myclaim/list', claim_request, {
                headers: { 
                    'Content-Type': 'text/plain'
                }
            })
            .then(res => {
                if(res.data.status !== 'true'){
                    console.log(res.data)
                    this.setState({
                        claim_list: res.data,
                        loading: true
                    })
                }else{

                }
            })
        }
    }


    render() {

        if (!localStorage.getItem('token')) {
            return (
                <Redirect to="/" />
            )
        }

        let claim_list_data = this.state.claim_list.claims;

        return(
           <div className="claim-list-table table-responsive">
                <table class="table table-bordered">
                    <thead>
                        <tr>
                            <th>Claim Numb</th>
                            <th>Date</th>
                            <th>Claimant Name</th>
                            <th>Claimant Email</th>
                            <th>Claimant Contact</th>
                            <th>Claimant Amount</th>
                            <th>Status</th>
                            <th>Invoice</th>
                        </tr>
                    </thead>
                    <tbody>
                        {claim_list_data !== undefined && claim_list_data.length ? (
                            <>
                            {claim_list_data ? claim_list_data.map((claim_list_item, index) =>

                                <tr>
                                    <td>{claim_list_item.name}</td>
                                    <td>2020/2/2</td>
                                    <td>{claim_list_item.claimant_name}</td>
                                    <td>{claim_list_item.claimant_email}</td>
                                    <td>{claim_list_item.claimant_mobile}</td>
                                    <td>{claim_list_item.claim_amount} USD</td>
                                    <td>{claim_list_item.status}</td>
                                    <td>
                                        {
                                            claim_list_item.invoices !== undefined && claim_list_item.invoices.length ? (
                                                <>
                                                    {claim_list_item.invoices ? claim_list_item.invoices.map((invoice_list, index) =>
                                                        {invoice_list.name}
                                                    )}
                                                </>
                                            )
                                        }
                                    </td>
                                </tr>

                            ) : null}  
                            </>  
                            ) : (
                                <tr>
                                    <td colSpan="8">No Record found</td>
                                </tr>
                        )} 

                    </tbody>
                </table>
                <ul className="pagination">
                    <li className="page-item active"><a className="page-link" href="#">1</a></li>
                    <li className="page-item"><a className="page-link" href="#">2</a></li>
                    <li className="page-item"><a className="page-link" href="#">3</a></li>
                    <li className="page-item"><a className="page-link" href="#">5</a></li>
                </ul>
           </div>
        )
    };
}
export default ClaimListTable;