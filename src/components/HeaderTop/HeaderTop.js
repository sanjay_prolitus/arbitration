import React, { Component } from 'react';
import './headertop.scss';
import axios from 'axios';
import Loader from 'react-loader'
import "slick-carousel/slick/slick.css";  
import "slick-carousel/slick/slick-theme.css";
import Slider from "react-slick";

let api_url = process.env.REACT_APP_API_URL1;

class HeaderTop extends Component{
    constructor(props) {
        super(props);
        this.state = {
            promotion_strip: '',
            loaded: false
        };
    }

    componentDidMount(){
        
       this.getTopHeaderdata()
        
    }

    // get Top header data
    getTopHeaderdata = () => {
        
        axios.get(api_url+'/api/v1/header', {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then(res => {

            if(res.data.success === true){
                let promotion_strip = res.data.promotion_strip;
                /*if(promotion_strip[0]){
                    promotion_strip = promotion_strip[0]['strip_name']
                }*/
                this.setState({
                    promotion_strip: promotion_strip,
                    loaded: true
                })
            }else{

            }
        })        
    }

    render() {

        let promotion_strip = this.state.promotion_strip;
        var slider = {
            slidesToShow: 1,
            autoplay: true,
            autoplaySpeed: 5000,
            infinite: true,
            slidesToScroll: 1,
            arrows:false
        };
        
        return(
            <>
                {promotion_strip && promotion_strip.length ? (
                    <section className='header--top'>
                        <div className="container">
                            <div className="row">
                                <div className="text-center col-12">
                                    <Slider {...slider} >
                                        { promotion_strip ? promotion_strip.map((promotion_strip, pro_key) =>
                                            <p key={pro_key}>{ promotion_strip.strip_name }</p>
                                        ) : null}
                                    </Slider>
                                </div>
                            </div>
                        </div>
                    </section>
                ): null }
            </>
        )
    }
}
export default HeaderTop;