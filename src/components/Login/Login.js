import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import axios from 'axios';
import Mobile from './../../assets/images/mobile.png';
import Mail from './../../assets/images/mail.png';
import Lock from './../../assets/images/lock.png';
import Google from './../../assets/images/google.png';
import './login.scss';

import { facebookProvider, googleProvider } from "../../config/authMethods";
import socialMediaAuth from "../../services/auth";

const Login = (props) =>{ 

    const [succmessage, setSuccmessage] = useState('');
    const [errormessage, setErrormessage] = useState('');

    let emailInput = React.createRef();  // React use ref to get input value
    let passwordInput = React.createRef();  // React use ref to get input value

    function submitForm(e) {
        e.preventDefault();
        let loginDetail = { 'login': emailInput.current.value, 'password': passwordInput.current.value }
        axios.post('/api/v1/auth/login', loginDetail, {
            headers: { 
                'Content-Type': 'text/plain'
            }
        })
        .then( response => {
            let result = response.data;
            console.log(result);
            if(result.success === true){
                var access_token    = result.access_token;
                var user_id         = result.user_id;
                var user_name       = result.user_name;
                var user_email      = result.user_email;
                localStorage.setItem('token', access_token);
                localStorage.setItem('user_id', user_id);
                localStorage.setItem('user_name', user_name);
                localStorage.setItem('user_email', user_email);
                setSuccmessage(result.message);
            }else{
                setErrormessage(result.message);
            }
            
        })
        .catch(err => {
            console.log(err);
        })
    }

    function handleOnClick () {
        const res       = socialMediaAuth(googleProvider);
        console.log("Google Res", res)
        if(res.credential){
            let id_token    = res.credential.idToken;
            let accessToken = res.credential.accessToken;
            let provider    = res.credential.providerId;
            let displayName;
            let email;
            let phone;
            if(res.user){
                displayName = res.user.displayName;
                email       = res.user.email;
                phone       = res.user.phoneNumber;
            }else{
                displayName = '';
                email       = res.email;
                phone       = '';
            }
            // console.log(res)
            let socialData = {
                "id_token": id_token,
                "accessToken": accessToken,
                "provider": provider,
                "name": displayName,
                "phone": phone,
                "email": email,
            }
            console.log("SocialData: ", socialData)
            axios.post('/social/media/login', socialData, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then( res => {
                let result = res.data.result
                // console.log('Result', result)
                if(result.status !== 0){
                    var access_token    = result.access_token;
                    var user_id         = result.user_id;

                    localStorage.setItem('token', access_token);
                    localStorage.setItem('user_id', user_id);

                }else{
                    
                }
                
            })
            .catch(err => {
                console.log(err);
            })
        }
    }

        return(   

            <Modal className="login--modal" show={props.openLogin} onHide={props.closeLogin}>
                <Modal.Body>
                    <Button className="close" onClick={props.closeLogin}>&times;</Button>
                    <h3 className="text-center">Login</h3>
                     
                    <InputGroup className="mb-4">
                        <InputGroup.Text id="email"><img src={Mail} alt='email'/></InputGroup.Text>
                        <FormControl
                            placeholder="E-mail"
                            aria-label="Enter email"
                            aria-describedby="email"
                            name="email"
                            ref={emailInput}
                        />
                    </InputGroup>
                    <InputGroup className="mb-4">
                        <InputGroup.Text id="password"><img src={Lock} alt='password'/></InputGroup.Text>
                        <FormControl
                            placeholder="Password"
                            aria-label="Enter password"
                            aria-describedby="password"
                            name="password"
                            ref={passwordInput}
                        />
                    </InputGroup>
                    <InputGroup className="mb-4 d-none">
                        <InputGroup.Text id="otp"><img src={Lock} alt='otp'/></InputGroup.Text>
                        <FormControl
                            placeholder="Enter OTP"
                            aria-label="Enter otp"
                            aria-describedby="otp"
                        />
                    </InputGroup>
                    <div className="row">
                        <div className="col-6">
                            <label className='checkbox'>Remember
                                <input type="checkbox"/>
                                <span className="checkmark"></span>
                            </label>
                        </div>
                        <div className="col-6 text-right">
                            <a href="#" className="forgot-link">Forget Password</a>
                        </div>
                    </div>
                    <div className="text-center otp-button-group d-none">
                        <button type="button" className="btn-signin">Get OTP</button>
                        <button type="button" className="btn-signin">Sign In Using Password</button>
                    </div>
                    <button onClick={submitForm} type="submit" className="btn-signin">Sign In</button>
                    
                    <div className="text-center">
                        <button type="button" className="google-btn"><span><img src={Mobile} alt='password'/></span>Sign in Using OTP</button>
                        <div className="or">OR</div>
                        <h4 className="text-center text-uppercase">Sign up with</h4>
                        <button type="button" className="google-btn google-btn__2 mt-4" onClick={() => handleOnClick(googleProvider)}><span><img src={Google} alt='password'/></span>Sign in with Google</button>
                    </div>
                </Modal.Body>
            </Modal>
        )
}

export default Login;