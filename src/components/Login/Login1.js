import React,{useState, Component} from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import axios from 'axios';
import RegisterAlert from '../Alert/RegisterAlert';
import Mobile from './../../assets/images/mobile.png';
import OTP from './../../assets/images/otp.png';
import Mail from './../../assets/images/mail.png';
import Lock from './../../assets/images/lock.png';
import Google from './../../assets/images/google.png';
import './login.scss';

import { facebookProvider, googleProvider } from "../../config/authMethods";
import socialMediaAuth from "../../services/auth";
import { Redirect } from 'react-router';

import { instanceOf } from "prop-types";
import { withCookies, Cookies } from "react-cookie";

let api_url = process.env.REACT_APP_API_URL1;

class Login extends Component {
    static propTypes = {
        cookies: instanceOf(Cookies).isRequired
    };
    constructor(props) {
        super(props);
        this.state = {
            succmessage: false,
            errormessage: false,
            email: this.props.cookies.get("arb_email") || "",
            loginemail: '',
            login_email: '',
            password: this.props.cookies.get("arb_password") || "",
            login_otp: '',
            openOtpLogin: '',
            closeOtpLogin: '',
            isLoggedIn: false,
            formEmailErr: false,
            formEmailErr1: false,
            formPassErr: false,
            formEmailErr2: false,
            formOtpErr: false,
            loginemailMsg: false,
            loginemailMsgErr: false,
            openOtpVerify: false,
            closeOtpVerify: false,
            reset_email: '',
            reset_password: '',
            reset_otp: '',
            openResetForm: false,
            closeResetForm: false,
            formEmailErr0: false,
            formEmailErr3: false,
            succ_message: false,
            loading: false,
            format_error: false,
            openLogin: this.props.openLogin,
            closeModel: this.props.closeLogin,
            rememberme: this.props.cookies.get("arb_password") || false
        };

        this.otpLogin          = this.otpLogin.bind(this);
        this.closeOtpLogin     = this.closeOtpLogin.bind(this);
        this.openResetForm      = this.openResetForm.bind(this);
        this.closeResetForm     = this.closeResetForm.bind(this);
        this.openOtpForm        = this.openOtpForm.bind(this);
        this.closeOtpForm       = this.closeOtpForm.bind(this);
        this.openLoginForm       = this.openLoginForm.bind(this);

        this.emailInput = React.createRef();
    }

    componentDidMount() {
        this.emailInput.current.focus();
    }

    inputHandler = (event) =>{
        this.setState ({ [event.target.name] :event.target.value  })
    }

    checkedHandler = (event) =>{
        this.setState ({ [event.target.name] :event.target.checked  })
    }

    submitForm = (e) => {
        e.preventDefault();

        let email      = this.state.email ? this.state.email : ''; 
        let password   = this.state.password ? this.state.password : '';
        let rememberme = this.state.rememberme ? this.state.rememberme : '';

        let loginDetail = { 'login': email, 'password': password }
        
        if(email && password ){
            
            this.setState({
              loading: true
            });

            axios.post(api_url+'/api/v1/auth/login', loginDetail, {
                headers: { 
                    'Content-Type': 'text/plain'
                }
            })
            .then( response => {
                let result = response.data;
                
                if(result.success === true){
                    var access_token    = result.access_token;
                    var user_id         = result.user_id;
                    var user_name       = result.user_name;
                    var user_email      = result.user_email;

                    localStorage.setItem('token', access_token);
                    localStorage.setItem('user_id', user_id);
                    localStorage.setItem('user_name', user_name);
                    localStorage.setItem('user_email', user_email);

                    this.setState({ succmessage: result.message, loading:false }, () => {
                        setTimeout(() => this.setState({ succmessage: false }), 10000);
                    })

                    const { cookies } = this.props;

                    if(rememberme){
                        cookies.set("arb_email", email, { path: "/", maxAge: 2592000 });
                        cookies.set("arb_password", password, { path: "/", maxAge: 2592000 });
                        cookies.set("arb_remember_me", rememberme, { path: "/", maxAge: 86400000 });
                    }else{
                        cookies.remove("arb_email");
                        cookies.remove("arb_password");
                        cookies.set("arb_remember_me", rememberme, { path: "/", maxAge: 86400000 });
                    }

                    this.setState({ 
                        email: cookies.get("arb_email"),
                        password: cookies.get("arb_password") 
                    });

                    window.location.reload(true);
                }else{
                    this.setState({ errormessage: result.message, loading:false }, () => {
                        setTimeout(() => this.setState({ errormessage: false }), 10000);
                    })
                }
                
            })
            .catch(err => {
                console.log(err);
            })
            
        }else{
            if(email === ''){
                this.setState({ formEmailErr1: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formEmailErr1: false }), 5000);
                })
            }
            if(password === ''){
                this.setState({ formPassErr: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formPassErr: false }), 5000);
                })
            }
        }
    }

    socialLogin = async (provider) => {
        const res       = await socialMediaAuth(provider);

        if(res.credential){
            let id_token    = res.credential.idToken;
            let accessToken = res.credential.accessToken;
            let provider    = res.credential.providerId;
            let displayName;
            let email;
            let phone;
            if(res.user){
                displayName = res.user.displayName;
                email       = res.user.email;
                phone       = res.user.phoneNumber;
            }else{
                displayName = '';
                email       = res.email;
                phone       = '';
            }
            
            let socialData = {
                "id_token": id_token,
                "accessToken": accessToken,
                "provider": provider,
                "name": displayName,
                "mobile": phone,
                "email": email,
            }

            axios.post(api_url+'/api/v1/social/media/login', socialData, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then( res => {
                let result = res.data

                if(result.status === 1){
                    var access_token    = result.access_token;
                    var user_id         = result.user_id;
                    var user_name       = result.name;
                    var user_email      = result.email;

                    localStorage.setItem('token', access_token);
                    localStorage.setItem('user_id', user_id);
                    localStorage.setItem('user_name', user_name);
                    localStorage.setItem('user_email', user_email);

                    this.setState({ succmessage: result.message }, () => {
                        setTimeout(() => this.setState({ succmessage: false }), 3000);
                    })

                    window.location.reload(true);
                    // this.getProfileData(user_id, access_token)
                    
                }else{
                    this.setState({ errormessage: result.message }, () => {
                        setTimeout(() => this.setState({ errormessage: false }), 3000);
                    })
                }
                
            })
            .catch(err => {
                console.log(err);
            })
        }
    }

    otpLogin = (e) => {
        e.preventDefault();
        this.setState({
            openOtpLogin: true,
            openLogin: false
        })
    }

    openLoginForm = (e) => {
        e.preventDefault();
        this.setState({
            openOtpLogin: false,
            openLogin: true
        })
    }

    closeOtpLogin = (e) => {
        e.preventDefault();
        this.setState({
            openOtpLogin: false
        })
    }
    closeOtpVerify = (e) => {
        e.preventDefault();
        this.setState({
            openOtpVerify: false
        })
    }

    loginOtpCreation = (event) => {
        event.preventDefault();
        let loginDetail = { 'email': this.state.loginemail }
        if(this.state.loginemail){
            axios.post(api_url+'/api/v1/login/otp/creation', loginDetail, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then( res => {
                let result = res.data
                if(result.success == false){
                    this.setState({
                        loginemailMsgErr: result.message
                    })
                }else{

                    this.setState({
                        loginemailMsg: result.email.message,
                        login_email: result.email.email,
                        otp_time: result.email.otp_time,
                        openOtpVerify: true
                    }, () => {
                        setTimeout(() => this.setState({ loginemailMsg: false }), 5000);
                    })
                }
                
            })
            .catch(err => {
                console.log(err);
            })
        }else{
            this.setState({ formEmailErr: "This field is required." }, () => {
                setTimeout(() => this.setState({ formEmailErr: false }), 5000);
            })
        }
    }

    resendOtp = (login_email) => {

        let loginDetail = { 'email': login_email }
        axios.post(api_url+'/api/v1/login/otp/creation', loginDetail, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data
            if(result.success == false){
                this.setState({
                    loginemailMsgErr: result.message
                })
            }else{
                this.setState({
                    loginemailMsg: result.email.message,
                    login_email: result.email.email,
                    otp_time: result.email.otp_time,
                    openOtpVerify: true
                })
            }
            
        })
        .catch(err => {
            console.log(err);
        })
    }

    verifyLoginOTP = (event) => {
        event.preventDefault();
        let registerDetail = { "email": this.state.login_email, "otp": this.state.login_otp }
        if(this.state.login_email && this.state.login_otp){
            axios.post(api_url+'/api/v1/login/otp/submit', registerDetail, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then( res => {
                let result = res.data

                if(result.success === true){
                    var access_token    = result.access_token;
                    var user_id         = result.user_id;
                    var user_name       = result.user_name;
                    var user_email      = result.user_email;

                    localStorage.setItem('token', access_token);
                    localStorage.setItem('user_id', user_id);
                    localStorage.setItem('user_name', user_name);
                    localStorage.setItem('user_email', user_email);

                    this.setState({
                        openOtpVerify: false,
                        openOtpLogin: false
                    })

                    window.location.reload(true);

                }else{
                    this.setState({ loginVarifyOtpErr: result.message }, () => {
                        setTimeout(() => this.setState({ loginVarifyOtpErr: false }), 5000);
                    })
                }
                
            })
            .catch(err => {
                console.log(err)
            })
        } else {
            if(this.state.login_email === ''){
                this.setState({ formEmailErr2: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formEmailErr2: false }), 5000);
                })
            }
            if(this.state.login_otp === ''){
                this.setState({ formOtpErr: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formOtpErr: false }), 5000);
                })    
            }
        }
    }

    openResetForm = (e) => {
        e.preventDefault();
        //this.props.closeLogin();
        this.setState({
            openResetForm: true,
            openLogin: false
        });
    }

    closeResetForm = (e) => {
        e.preventDefault();
        this.setState({
            openResetForm: false
        })
    }

    openOtpForm = (e) => {
        e.preventDefault();
        this.setState({
            openResetForm: true
        })
    }

    closeOtpForm = (e) => {
        e.preventDefault();
        this.setState({
            openOtpForm: false
        })
    }

    submitResetForm = (e) => {
        e.preventDefault();

        let email      = this.state.reset_email ? this.state.reset_email : '';

        let request = { 'email': email}
        
        if(email){
            axios.post(api_url+'/api/v1/generate/otp', request, {
                headers: { 
                    'Content-Type': 'text/plain'
                }
            })
            .then( response => {
                let result = response.data;
                
                if(result.success === true){

                    this.setState({ succ_message: result.message, email: email }, () => {
                        setTimeout(() => this.setState({ succ_message: false, openOtpForm: true }), 3000);
                    })

                }else{
                    this.setState({ errormessage: "Somthing went wrong." }, () => {
                        setTimeout(() => this.setState({ errormessage: false }), 3000);
                    })
                }
                
            })
            .catch(err => {
                console.log(err);
            })
        }else{
            
            this.setState({ formEmailErr0: "This field is required." }, () => {
                setTimeout(() => this.setState({ formEmailErr0: false }), 3000);
            })
            
        }
    }

    verifyResetForm = (e) => {
        e.preventDefault();

        let email       = this.state.reset_email ? this.state.reset_email : ''; 
        let password    = this.state.reset_password ? this.state.reset_password : '';
        let otp         = this.state.reset_otp ? this.state.reset_otp : '';

        let request = { 
            'email': email, 
            'confirm_password': password, 
            'otp': otp 
        }
        
        if(email && password && otp ){
            var regex = /^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[^\w\s]).{8,}$/
            if(password.match(regex)){
                axios.post(api_url+'/api/v1/set_password', request, {
                    headers: { 
                        'Content-Type': 'text/plain'
                    }
                })
                .then( response => {
                    let result = response.data;
                    
                    if(result.success === true){
                        var access_token    = result.access_token;
                        var user_id         = result.user_id;
                        var user_name       = result.user_name;
                        var user_email      = result.user_email;

                        localStorage.setItem('token', access_token);
                        localStorage.setItem('user_id', user_id);
                        localStorage.setItem('user_name', user_name);
                        localStorage.setItem('user_email', user_email);

                        this.setState({ succmessage: result.message }, () => {
                            setTimeout(() => this.setState({ succmessage: false, openOtpForm: false }), 3000);
                        })
                        window.location.reload(true);
                    }else{
                        this.setState({ errormessage: result.message }, () => {
                            setTimeout(() => this.setState({ errormessage: false }), 3000);
                        })
                    }
                    
                })
                .catch(err => {
                    console.log(err);
                })
            } else {
                this.setState({ format_error: "Minimum eight characters, at least one uppercase letter, one lowercase letter, one number and one special character" }, () => {
                    setTimeout(() => this.setState({ format_error: false }), 5000);
                })
            } 
        }else{
            if(email === ''){
                this.setState({ formEmailErr1: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formEmailErr1: false }), 3000);
                })
            }
            if(password === ''){
                this.setState({ formEmailErr2: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formEmailErr2: false }), 3000);
                })
            }
            if(otp === ''){
                this.setState({ formEmailErr3: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formEmailErr3: false }), 3000);
                })
            }
        }
    }

    render() {

        if ( localStorage.getItem('user_id') ) {
            return (
                <Redirect to="/dashboard" />
            )
        }

        let openOtpLogin    = this.state.openOtpLogin;
        let closeOtpLogin   = this.state.closeOtpLogin;
        let openResetForm   = this.state.openResetForm;
        let openLogin       = this.state.openLogin;
        let closeModel      = this.state.closeModel;
        let openOtpVerify   = this.props.openOtpVerify;
        let rememberme      = this.state.rememberme ? this.state.rememberme : '';

        return(   
            <>
                <Modal className="login--modal" show={openLogin} >
                    <Modal.Body>
                        <form onSubmit={this.submitForm}>
                            <Button className="close" onClick={closeModel}>&times;</Button>
                            <h3 className="text-center">Sign In</h3>
                            {this.state.succmessage &&
                                <div className="success-message" style={{ color:"green"}}>{this.state.succmessage}</div>
                            }
                            {this.state.errormessage &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.errormessage}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="email"><img src={Mail} alt='email'/></InputGroup.Text>
                                <FormControl
                                    placeholder="E-mail"
                                    aria-label="Enter email"
                                    aria-describedby="email"
                                    name="email"
                                    type="email"
                                    ref={this.emailInput}
                                    value={this.state.email}
                                    onChange={this.inputHandler}
                                    required="required"
                                />
                            </InputGroup>
                            {this.state.formEmailErr1 &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.formEmailErr1}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="password"><img src={Lock} alt='password'/></InputGroup.Text>
                                <FormControl
                                    type="password"
                                    placeholder="Password"
                                    aria-label="Enter password"
                                    aria-describedby="password"
                                    name="password"
                                    value={this.state.password}
                                    onChange={this.inputHandler}
                                    required="required"
                                />
                            </InputGroup>
                            {this.state.formPassErr &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.formPassErr}</div>
                            }
                            <div className="row">
                                <div className="col-6">
                                    <label className='checkbox'>Remember me
                                        <input type="checkbox" name="rememberme" deafultValue={this.state.rememberme} onChange={this.checkedHandler} />
                                        <span className={"checkmark "+(this.state.rememberme ? "checked" : "")}></span>
                                    </label>
                                </div>
                                <div className="col-6 text-right">
                                    <a href="#" className="forgot-link" onClick={this.openResetForm}>Forgot Password?</a>
                                </div>
                            </div>
                            <div className="text-center otp-button-group d-none">
                                <button type="button" className="btn-signin">Get OTP</button>
                                <button type="button" className="btn-signin">Sign In Using Password</button>
                            </div>
                            <button type="submit" className="btn-signin">{this.state.loading ? "Loading..." : "Sign In"} </button>
                            
                            <div className="text-center">
                                <button type="button" className="google-btn" onClick={this.otpLogin}><span><img src={Mobile} alt='OTP' /></span>Sign in Using OTP</button>
                                <div className="or">OR</div>
                                <button type="button" className="google-btn google-btn__2 mt-0" onClick={() => this.socialLogin(googleProvider)}><span><img src={Google} alt='password'/></span>Sign in with Google</button>
                            </div>
                        </form>
                    </Modal.Body>
                </Modal>

                <Modal className="login--modal" show={this.state.openOtpLogin}>
                    <Modal.Body>
                        <form onSubmit={this.loginOtpCreation}>
                            <Button className="close" onClick={closeModel}>&times;</Button>
                            <h3 className="text-center">Sign In</h3>
                            {this.state.loginemailMsgErr &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.loginemailMsgErr}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="loginemail"><img src={Mail} alt='email'/></InputGroup.Text>
                                <FormControl
                                    placeholder="E-mail"
                                    aria-label="Enter email"
                                    aria-describedby="email"
                                    name="loginemail"
                                    required="required"
                                    value={this.state.loginemail}
                                    onChange={this.inputHandler}
                                />
                            </InputGroup>
                            {this.state.formEmailErr &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.formEmailErr}</div>
                            }
                            <div className="text-center otp-button-group">
                                <button type="submit" className="btn-signin" >Get OTP</button>
                                <button type="button" className="btn-signin" onClick={this.openLoginForm}>Sign In Using Password</button>
                            </div>

                            <div className="text-center">
                                <div className="or">OR</div>
                                <button type="button" className="google-btn google-btn__2 mt-0" onClick={() => this.socialLogin(googleProvider)}><span><img src={Google} alt='password'/></span>Sign in with Google</button>
                            </div>
                        </form>
                    </Modal.Body>
                </Modal>

                <Modal className="login--modal" show={this.state.openOtpVerify}>
                    <Modal.Body>
                        <form onSubmit={this.verifyLoginOTP}>
                            <Button className="close" onClick={closeModel}>&times;</Button>
                            <h3 className="text-center">Sign In</h3>                        
                            {this.state.loginemailMsg &&
                                <div className="success-message" style={{ color:"green"}}>{this.state.loginemailMsg}</div>
                            }
                            {this.state.loginVarifyOtpErr &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.loginVarifyOtpErr}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="login_email"><img src={Mail} alt='email'/></InputGroup.Text>
                                <FormControl
                                    placeholder="E-mail"
                                    aria-label="Enter email"
                                    aria-describedby="email"
                                    name="login_email"
                                    required="required"
                                    value={this.state.login_email}
                                    onChange={this.inputHandler}
                                    readonly
                                />
                            </InputGroup>
                            {this.state.formEmailErr2 &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.formEmailErr2}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="OTP"><img src={OTP} alt='OTP'/></InputGroup.Text>
                                <FormControl
                                    placeholder="Enter OTP"
                                    aria-label="Enter OTP"
                                    aria-describedby="otp"
                                    name="login_otp"
                                    required="required"
                                    value={this.state.login_otp}
                                    onChange={this.inputHandler}
                                />
                            </InputGroup>
                            {this.state.formOtpErr &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.formOtpErr}</div>
                            }
                            <div className="row">
                                <div className="col-12 text-right">
                                    <a href="#" className="forgot-link">Forget Password</a>
                                </div>
                            </div>
                            <div className="text-center otp-button-group">
                                <button type="submit" className="btn-signin" >Verify OTP</button>
                                <button type="button" className="btn-signin" onClick={() => this.resendOtp(this.state.login_email)}>Resend OTP</button>
                            </div>

                            <div className="text-center">
                                <div className="or">OR</div>
                                <button type="button" className="google-btn google-btn__2 mt-0" onClick={() => this.handleOnClick(googleProvider)}><span><img src={Google} alt='password'/></span>Sign in with Google</button>
                            </div>
                        </form>
                    </Modal.Body>
                </Modal>

                <Modal className="login--modal" show={this.state.openResetForm}>
                    <Modal.Body>
                        <form onSubmit={this.submitResetForm}>
                            <Button className="close" onClick={closeModel}>&times;</Button>
                            <h3 className="text-center">Reset Password</h3>
                            {this.state.succ_message &&
                                <div className="success-message" style={{ color:"green"}}>{this.state.succ_message}</div>
                            }
                            {this.state.errormessage &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.errormessage}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="reset_email1"><img src={Mail} alt='reset_email'/></InputGroup.Text>
                                <FormControl
                                    placeholder="E-mail"
                                    aria-label="Enter email"
                                    aria-describedby="email"
                                    name="reset_email"
                                    required="required"
                                    value={this.state.reset_email}
                                    onChange={this.inputHandler}
                                />
                            </InputGroup>
                            {this.state.formEmailErr0 &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.formEmailErr0}</div>
                            }
                            <div className="text-center otp-button-group">
                                <button type="submit" className="btn-signin">Get OTP</button>
                            </div>
                        </form>
                    </Modal.Body>
                </Modal>

                <Modal className="login--modal" show={this.state.openOtpForm}>
                    <Modal.Body>
                        <form onSubmit={this.verifyResetForm}>
                            <Button className="close" onClick={closeModel}>&times;</Button>
                            <h3 className="text-center">Verify OTP</h3>
                            {this.state.errormessage &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.errormessage}</div>
                            }
                            {this.state.succmessage &&
                                <div className="success-message" style={{ color:"green"}}>{this.state.succmessage}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="reset_email"><img src={Mail} alt='reset_email'/></InputGroup.Text>
                                <FormControl
                                    placeholder="E-mail"
                                    aria-label="Enter email"
                                    aria-describedby="email"
                                    name="reset_email"
                                    readonly="readonly"
                                    required="required"
                                    value={this.state.reset_email}
                                    onChange={this.inputHandler}
                                />
                            </InputGroup>
                            {this.state.formEmailErr1 &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.formEmailErr1}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="password"><img src={Lock} alt='password'/></InputGroup.Text>
                                <FormControl
                                    placeholder="Enter Password"
                                    aria-label="Enter Password"
                                    aria-describedby="password"
                                    name="reset_password"
                                    type="password"
                                    required="required"
                                    value={this.state.reset_password}
                                    onChange={this.inputHandler}
                                />
                            </InputGroup>
                            {this.state.formEmailErr2 &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.formEmailErr2}</div>
                            }
                            {this.state.format_error &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.format_error}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="otp"><img src={OTP} alt='otp'/></InputGroup.Text>
                                <FormControl
                                    placeholder="Enter OTP"
                                    aria-label="Enter Otp"
                                    aria-describedby="otp"
                                    name="reset_otp"
                                    required="required"
                                    value={this.state.reset_otp}
                                    onChange={this.inputHandler}
                                />
                            </InputGroup>
                            {this.state.formEmailErr3 &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.formEmailErr3}</div>
                            }
                            <div className="text-center otp-button-group">
                                <button type="submit" className="btn-signin">Submit</button>
                            </div>
                        </form>
                    </Modal.Body>
                </Modal>

                {this.state.succmessage &&
                    <RegisterAlert registerSucc={this.state.succmessage} />
                }
            </>
        )
    }
}

export default withCookies(Login);