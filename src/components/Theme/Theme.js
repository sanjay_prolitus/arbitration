import React,{Component} from 'react';
import axios from 'axios';
import Logo from './../../assets/images/logo.png';
// import './theme.scss';
require('dotenv').config()
let api_url = process.env.REACT_APP_API_URL1;

class Theme extends Component{

     constructor(props) {
        super(props);

         this.state = {
            color: {
                primary: "#1e1666",
                text: "#333333",
                button_background_colour: "",
                button_colour: "",
                button_hover_colour: "",
                error_bg_colour: "",
                error_message_colour: "",
                input_background: "",
                input_colour: "",
                logo_height: 25,
                logo_width: 25,
                msg_color: "#7FC18A",
                popup_background: "",
                popup_header: "#fff",
                white_popup_header: "#2b2b45",
                popup_label: "#fff",
                white_popup_label: "#2b2b45",
                popup_input: "#2b2b45",
                white_popup_input: "#2b2b45",
                popup_button: "#fff",
                popup_button_background: "#2b2b45",
                white_popup_button: "#fff",
                white_popup_button_background: "#2b2b45",
                popup_button_hover: "#2b2b45",
                popup_button_hover_background: "#fff",
                white_popup_button_hover: "#2b2b45",
                white_popup_button_hover_background: "#fff",
                success_bgcolour: "",
                success_message_colour: "",
                theme_logo: "/api/v1/theme/logo/content/454"
            },
            logo: "/static/media/logo.80a325a0.png"

         }

    }

    componentDidMount(){

       const root = document.documentElement;
       root?.style.setProperty( "--primary-color", this.state.color.primary );
       root?.style.setProperty( "--primary-text-color", this.state.color.primary );
       root?.style.setProperty("--popup-heading-color", this.state.color.popup_header );
       root?.style.setProperty("--white-popup-heading-color", this.state.color.white_popup_header );
       root?.style.setProperty("--popup-label-color", this.state.color.popup_label );
       root?.style.setProperty("--white-popup-label-color", this.state.color.white_popup_label );
       root?.style.setProperty("--popup-input-color", this.state.color.popup_input );
       root?.style.setProperty("--white-popup-input-color", this.state.color.white_popup_input );
       root?.style.setProperty("--popup-button-color", this.state.color.popup_button );
       root?.style.setProperty("--white-popup-button-color", this.state.color.white_popup_button );
       root?.style.setProperty("--popup-button-background", this.state.color.popup_button_background );
       root?.style.setProperty("--white-popup-button-background", this.state.color.white_popup_button_background );
       root?.style.setProperty("--popup-button-hover-color", this.state.color.popup_button_hover );
       root?.style.setProperty("--white-popup-button-hover-color", this.state.color.white_popup_button_hover );
       root?.style.setProperty("--popup-button-hover-background", this.state.color.popup_button_hover_background );
       root?.style.setProperty("--white-popup-button-hover-background", this.state.color.white_popup_button_hover_background );
       root?.style.setProperty( "--primary-color", this.state.color.button_background_colour );
       root?.style.setProperty("--text-color", this.state.color.button_colour );
       root?.style.setProperty( "--primary-color", this.state.color.button_hover_colour );
       root?.style.setProperty("--text-color", this.state.color.error_bg_colour );
       root?.style.setProperty( "--primary-color", this.state.color.error_message_colour );
       root?.style.setProperty("--text-color", this.state.color.input_background );
       root?.style.setProperty( "--primary-color", this.state.color.input_colour );
       root?.style.setProperty("--text-color", this.state.color.logo_height );
       root?.style.setProperty( "--primary-color", this.state.color.logo_width );
       root?.style.setProperty("--text-color", this.state.color.msg_color);
       root?.style.setProperty( "--primary-color", this.state.color.popup_background );
       root?.style.setProperty("--text-color", this.state.color.popup_header );
       root?.style.setProperty( "--primary-color", this.state.color.success_bgcolour );
       root?.style.setProperty("--text-color", this.state.color.success_message_colour );

       this.getThemeData(root);
    }

    getThemeData (root){

        axios.get(api_url+'/api/v1/backend/theme')
            .then(res => {
                if(res.data.success === true){
                    
                    this.setState({
                        logo: res.data.theme_logo
                    });
                    root?.style.setProperty("--primary-color", res.data.theme_color );
                    root?.style.setProperty("--button_background_colour", this.state.color.button_background_colour );
                    root?.style.setProperty("--button_colour", this.state.color.button_colour );
                    root?.style.setProperty("--button_hover_colour", this.state.color.button_hover_colour );
                    root?.style.setProperty("--error_bg_colour", this.state.color.error_bg_colour );
                    root?.style.setProperty("--error_message_colour", this.state.color.error_message_colour );
                    root?.style.setProperty("--input_background", this.state.color.input_background );
                    root?.style.setProperty("--input_colour", this.state.color.input_colour );
                    root?.style.setProperty("--logo_height", this.state.color.logo_height );
                    root?.style.setProperty("--logo_width", this.state.color.logo_width );
                    root?.style.setProperty("--msg_colo", this.state.color.msg_color);
                    root?.style.setProperty("--popup_background", this.state.color.popup_background );
                    root?.style.setProperty("--popup_header", this.state.color.popup_header );
                    root?.style.setProperty("--success_bgcolour", this.state.color.success_bgcolour );
                    root?.style.setProperty("--success_message_colour", this.state.color.success_message_colour );
                }else{

                }
            })
    }

    render () {
        let img_url = process.env.REACT_APP_API_URL + this.state.logo;
        return(
            /*<img src={img_url} alt='logo'/>*/
            <img src={Logo} alt='logo'/>
        );
    }
}

export default Theme;