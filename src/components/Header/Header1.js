import React,{useState} from 'react';
import Dropdown from 'react-bootstrap/Dropdown'
import Logo from './../../assets/images/logo.png';
import SignUp from './../../assets/images/signup.png';
import SignIn from './../../assets/images/signin.png';
import { Link } from 'react-router-dom';
import Login from './../Login/Login1';
import Register from './../Register/Register1';
import Reset from './../Reset/Reset';
import Respond from './../Respond/Respond';
import SuccessAlert from './../Alert/Alert';
import AddDocument from './../AddDocument/AddDocument';
import './header.scss';
import Theme from './../Theme/Theme';
import { Redirect } from 'react-router';
import axios from 'axios';
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faUser } from '@fortawesome/free-solid-svg-icons'
import { faSignOutAlt } from '@fortawesome/free-solid-svg-icons'
import { faSignInAlt } from '@fortawesome/free-solid-svg-icons'
import { faUserPlus } from '@fortawesome/free-solid-svg-icons'
import { faUserLock } from '@fortawesome/free-solid-svg-icons'

let api_url = process.env.REACT_APP_API_URL1;

const Header = () =>{
    const [openLogin,setOpenLogin]          = useState();
    const [openRegister,setOpenRegister]    = useState();
    const [toggle,setToggle]                = useState(false);
    const [megaMenu, setMegaMenu]           = React.useState('');

    const handleLogin = () =>{
        setOpenLogin(true)
    }

    const handleCloseLogin = () =>{
        setOpenLogin(false)
    }

    const handleRegister = () =>{
        setOpenRegister(true)
    }

    const handleCloseRegister = () =>{
        setOpenRegister(false)
    }

    const toogleHandle = () =>{
        setToggle(!toggle)
    }

    const handleLogout = () =>{

        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');

        let request    = { 
            'user_id': parseInt(user_id), 
            'access_token': token 
        }
        axios.post(api_url+'/api/v1/auth/logout', request, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then(res => {

            if(res.data.success === true){

            }
        })

        localStorage.removeItem('token');
        localStorage.removeItem('user_id');
        localStorage.removeItem('user_name');
        localStorage.removeItem('user_email');
        localStorage.removeItem('partner_id');
        localStorage.removeItem('payment_line_id');
        localStorage.removeItem('claim_id');
        localStorage.removeItem('fee_amount');
        localStorage.removeItem('respondent_list');
        localStorage.removeItem('attorney_list');
        localStorage.removeItem('claimant_name');

        window.location.reload(true);
        return(<Redirect to="/" />)
    }

    const getMegaMenu = React.useCallback(() => {
        
        axios.get(api_url+'/api/v1/megamenu', {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then((response) => {
            setMegaMenu(response.data.menu_list)
        })
        .catch((error) => {
            console.log(error)
        })
        
    }, [])

    React.useEffect(() => {
        getMegaMenu()
    }, [getMegaMenu])

    //let lastelement = megaMenu[megaMenu.length - 1];
    //console.log("lastelement: ", lastelement.name)
    return(
        <header>
            
            <div className="container">
                <div className="row">
                    <div className="col-md-2 col-lg-4 col-12">
                        <Link to='/' className="logo"><Theme /></Link>
                    </div>
                    <div onClick={toogleHandle} className={`${toggle === true ? 'open-menu toogle':'toogle'}`}>
                        <span></span>
                    </div>
                    <div className="col-md-10 col-lg-8 col-12">
                        <nav className={`${toggle === true ? 'open-menu navbar':'navbar'}`}>                     
                            <ul className="navbar-nav">
                                <Dropdown className="nav-item dropdown">
                                    <Dropdown.Toggle id="dropdown-basic" className="nav-link dropdown-toggle">Help</Dropdown.Toggle>
                                    <Dropdown.Menu>
                                        <div className="container">
                                            <div className="row">
                                                {megaMenu ? megaMenu.map((megamenu, index) =>
                                                    <>
                                                        {megamenu.column != '' ? (
                                                            <div className={"col-md-3 col-12 "+(megamenu.name === "Document" ? "" : "border-right")} key={index}>
                                                                <h4 >{megamenu.name}</h4>
                                                                {megamenu.content_type === "csm_page" ? (
                                                                    <>
                                                                        {megamenu.column ? megamenu.column.map((columns, j) =>
                                                                            <Dropdown.Item href={`/cms-page/${ columns.page_type_name }`} key={j}>{columns.name}</Dropdown.Item>
                                                                        ) : null}
                                                                    </>
                                                                ) : (
                                                                    <>
                                                                        {megamenu.column ? megamenu.column.map((columns, j) =>
                                                                            <Dropdown.Item href={api_url+columns.document} key={j} target="_blank">{columns.name}</Dropdown.Item>
                                                                        ) : null}
                                                                    </>
                                                                )} 
                                                            </div>
                                                        ) : (
                                                            <></>
                                                        )}    
                                                    </>
                                                ) : null}

                                            </div>
                                        </div>
                                    </Dropdown.Menu>
                                </Dropdown>
                                <li className="nav-item"><Link to='/cms-page/pma' className="nav-link">Rules and forms</Link></li>
                                {localStorage.getItem('user_name') &&
                                    <li className="nav-item"><Link to='/dashboard' className="nav-link">My Claims</Link></li>
                                }
                                {!localStorage.getItem('user_id') ? (
                                    <li className="nav-item"><button onClick={handleLogin} type="button" className="nav-link text-uppercase cred-btn"><FontAwesomeIcon icon={faSignInAlt} /> SignIn </button></li>
                                ) : (
                                    <li className="nav-item"><Link to="/dashboard"><button type="button" className="nav-link text-uppercase cred-btn"><FontAwesomeIcon icon={faUser} /> {localStorage.getItem('user_name')}</button></Link></li>
                                )}
                                {!localStorage.getItem('user_id') ? (
                                    <li className="nav-item"><button onClick={handleRegister} type="button" className="nav-link text-uppercase cred-btn"><FontAwesomeIcon icon={faUserLock} /> Signup</button></li>
                                ) : (
                                    <li className="nav-item"><button onClick={handleLogout} type="button" className="nav-link text-uppercase cred-btn"><FontAwesomeIcon icon={faSignOutAlt} /> Logout</button></li>
                                )}
                            </ul>                      
                        </nav>
                    </div>
                </div>
            </div>
            {openLogin === true ? <Login openLogin={openLogin} closeLogin={handleCloseLogin}/> : ''}
            {openRegister === true ? <Register openRegister={openRegister} closeRegister={handleCloseRegister}/> : ''}
            <Reset/>
            <Respond/>
            <SuccessAlert/>
            <AddDocument/>
        </header>
    )
}
export default Header;