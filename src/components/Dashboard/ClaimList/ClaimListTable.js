import React, { Component } from "react";
import axios from 'axios';
import { Redirect } from 'react-router';
import DataTable from 'react-data-table-component';
import './claim-list-table.scss';
import { Link } from "react-router-dom";

import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Search from './../../../assets/images/search.png';

import ReactPaginate from 'react-paginate';
import Loader from 'react-loader'
import NoDataTable from '../../Common/NoDataTable';

let ajaxRequest = null; 
let api_url = process.env.REACT_APP_API_URL1;

class ClaimListTable extends Component{

    constructor(props) {
        super(props);
        this.state = {
            claim_list : '',
            name : '',
            list_data : [],
            status : this.props.status ? this.props.status : "in_process",
            loading : false,
            loaded: false
        };

        this.keyUpSearch        = this.keyUpSearch.bind(this);
    }

    componentDidMount(){
        
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        
        if(token !== '' && user_id !== ''){
            this.getClaimList(user_id, token)
        }
    }

    keyUpSearch(event) {
        
        this.setState({
            [event.target.name] : event.target.value
        }, () => {
            this.claimListSearch(event);
        });
    }

    inputHandler = (event) =>{

        this.setState ({ [event.target.name] : event.target.value })
    }

    // get asset details
    getClaimList = (user_id, token) => {
        if(user_id){
            let claim_request    = { 
                'user_id': parseInt(user_id), 
                'access_token': token, 
                'page_no': (this.state.page_no) ? this.state.page_no : 0, 
                'status': (this.state.status) ? this.state.status : "in_process",
            }
            axios.post(api_url+'/api/v1/myclaim/list', claim_request, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then(res => {

                if(res.data.success === true){

                    this.setState({
                        claim_list: res.data,
                        loading: true,
                        loaded: true
                    })
                }else{

                }
            })
        }
    }

    // get asset details
    claimListSearch = (e) => {
        
        if (ajaxRequest ) {
            ajaxRequest.cancel(); 
        }

        ajaxRequest = axios.CancelToken.source();

        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        let filter_request    = { 
            'user_id': parseInt(user_id), 
            'access_token': token, 
            'name': (this.state.name) ? this.state.name : '',
            'status': this.state.status
        }
        
        this.setState({
            loading: true
        })
        axios.post(api_url+'/api/v1/search/claim', filter_request, {
            headers: {
                'Content-Type': 'text/plain'
            },
            cancelToken: ajaxRequest.token
        })
        .then(res => {

            if(res.data.success === true){

                this.setState({
                    claim_list: res.data,
                    loading: true,
                    loaded: true
                })
            }else{

            }
        })
        
    }

    searchStatus = (type) => {
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        this.setState({
            status: type,
            name: '',
            loaded : false
        }, () => {
            this.getClaimList(user_id, token)
        })
    }

    handlePageClick = (data) => {
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');

        let page_no = data.selected;

        this.setState({ page_no: page_no+1 }, () => {
          this.getClaimList(user_id, token);
      });
    };

    render() {

        if (!localStorage.getItem('token')) {
            return (
                <Redirect to="/" />
            )
        }

        let claim_list_data = this.state.claim_list.claims;

        let total_count = this.state.claim_list.total_count;
        let records_per_page = this.state.claim_list.records_per_page;
        let page_count = total_count / records_per_page;
        
        let columns = [
            {
                name: 'Claim#',
                selector: row => row.claim,
                sortable: true,
                minWidth: "160px"
            },
            {
                name: 'Date',
                selector: row => row.date,
                sortable: true
            },
            {
                name: 'Claimant Name',
                selector: row => row.claimant,
                sortable: true,
                minWidth: "200px"
            },
            {
                name: 'Respondent',
                selector: row => row.case_respondent,
                wrap: true,
                minWidth: "200px"
            },
            {
                name: 'Claim Amount',
                selector: row => row.amount,
            },{
                name: 'Status',
                selector: row => row.status,
                sortable: true,
                minWidth: "100px"
            },
            {
                name: 'Action',
                selector: row => row.invoice,
                minWidth: "170px"
            }            
        ];

        let list_data = [];
        if(claim_list_data !== undefined){
            let claim_list = Object.keys(claim_list_data); 
            claim_list.forEach(function(key, i) {

                let invoice = claim_list_data[key]['invoices'][0];
                let case_respondent = claim_list_data[key]['case_respondent'];
                let respondent_name;
                if(case_respondent){
                    respondent_name = case_respondent.map((item) => { return item.name }).join(', ')
                }
                let invoice_url;
                if(invoice){
                    let count_invoice = claim_list_data[key]['invoices'].length;
                    invoice_url = 
                    <>
                        <div className="claimlist-btn">
                            <Link className="add-primary" to={`/invoice-claim-list/${ claim_list_data[key].id }`}>Invoice({count_invoice})</Link>
                        </div>
                    </>
                }else{
                    invoice_url = 
                    <div className="claimlist-btn">
                        <Link className="add-primary" onClick={e => e.preventDefault()}>Invoice</Link>
                    </div>
                }
                
                list_data.push({ 
                    claim: <Link to={`/claim-detail/${ claim_list_data[key].id }`}> {claim_list_data[key].name} </Link>, 
                    date: <Link to={`/claim-detail/${ claim_list_data[key].id }`}> {claim_list_data[key].date} </Link>,
                    claimant: <Link to={`/claim-detail/${ claim_list_data[key].id }`}> {claim_list_data[key].claimant_name} </Link>,
                    case_respondent: <Link to={`/claim-detail/${ claim_list_data[key].id }`} title={respondent_name}> {respondent_name} </Link>,
                    amount: <Link to={`/claim-detail/${ claim_list_data[key].id }`}> ${claim_list_data[key].claim_amount} </Link>,
                    status: <Link to={`/claim-detail/${ claim_list_data[key].id }`}> {claim_list_data[key].status} </Link>, 
                    invoice: invoice_url
                })

            });
        }

        
        return(
            
            <>
                <div className='row filter-options'>
                    <div className="col-md-6 col-12">
                        <Link onClick={e => e.preventDefault()} className={"add-primary "+(this.state.status === "draft" ? "active" : '')} onClick={() => this.searchStatus("draft")} > Draft { this.state.claim_list.total_draft ? ( ' ('+(this.state.claim_list.total_draft)+')' ): <>(0)</> } </Link>
                        <Link onClick={e => e.preventDefault()} className={"add-primary "+(this.state.status === "in_process" ? "active" : '')} onClick={() => this.searchStatus("in_process")} >In Process { this.state.claim_list.total_inpogress ? ( ' ('+(this.state.claim_list.total_inpogress)+')' ): <>(0)</> } </Link>
                        <Link onClick={e => e.preventDefault()} className={"add-primary "+(this.state.status === "disposed" ? "active" : '')} onClick={() => this.searchStatus("disposed")} >Disposed { this.state.claim_list.total_disposed ? ( ' ('+(this.state.claim_list.total_disposed)+')' ): <>(0)</>} </Link>
                    </div>
                    <div className="col-md-6 col-12">
                        <InputGroup>
                        <FormControl
                            placeholder="Search"
                            aria-label="Enter value for searching"
                            aria-describedby="search"
                            name="name"
                            value={this.state.name} 
                            onKeyUp={this.keyUpSearch}
                            onChange={this.inputHandler}
                        />
                        <InputGroup.Text id="search" onClick={this.claimListSearch}><img src={Search} alt='search'/></InputGroup.Text>
                    </InputGroup>
                    </div>
                </div>
                <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} radius={12} top="100%" color="#201e53">
                    {list_data.length > 0 &&
                        <DataTable
                            columns={columns}
                            data={list_data}
                            fixedHeader
                        />
                    }

                    { list_data.length == 0 &&
                        <div className="claim-list-table table-responsive">
                            <NoDataTable columns={columns} title="No Data Available" />
                        </div>
                    }
                    {list_data !== undefined && list_data.length ? (
                        <ReactPaginate
                            previousLabel={'Prev'}
                            nextLabel={'Next'}
                            breakLabel={'...'}
                            breakClassName={'break-me'}
                            pageCount={page_count}
                            marginPagesDisplayed={2}
                            pageRangeDisplayed={5}
                            onPageChange={this.handlePageClick}
                            containerClassName={'pagination'}
                            activeClassName={'active'}
                            pageClassName={'page-item'}
                            previousClassName={'page-item'}
                            nextClassName={'page-item'}
                            nextLinkClassName={'page-item'}
                            pageLinkClassName={'page-link'}
                            previousLinkClassName={'page-link'}
                            nextLinkClassName={'page-link'}
                        />
                    ) : null}
                </Loader>
            </>
        )
    };
}
export default ClaimListTable;