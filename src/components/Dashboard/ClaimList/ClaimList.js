import React, {Component} from 'react';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import ClaimListTable from './ClaimListTable';
import Search from './../../../assets/images/search.png';
import './claim-list.scss';
import { Link } from "react-router-dom";

class ClaimList extends Component{
    constructor(props) {
        super(props);
        this.state = {
            status: ''
        };
    }

    searchStatus = (type) => {
        this.setState({
            status: type
        })
    }

    render() {

        return(
           <div className="claim-list claim-list-table ">
                <ClaimListTable status={this.state.status}/>
           </div>
        )
    }
}
export default ClaimList;