import React from 'react';
import { Link } from "react-router-dom";
import Arrow from './../../../assets/images/arrow-circle.png';
import './new-claim.scss';

const NewClaim = () =>{
    return(
       <div className="new-claim">
            <Link to="/new-claimant-info"><button type="button" className="new-claim-btn">File A New Claim <img src={Arrow} alt='arrow'/></button></Link>
       </div>
    )
}
export default NewClaim;