import React, { Component } from 'react';
import ClaimList from '../../ClaimList/ClaimList';
import NewClaim from  '../../NewClaim/NewClaim';
import ViewInvoice from  '../../Invoice/ViewInvoice';
import axios from 'axios';
import Search from '../../../../assets/images/search-plus.png';
import './invoicedetails.scss';
import Loader from 'react-loader'
import DataTable from 'react-data-table-component';
import { Link } from "react-router-dom";
import { Redirect } from 'react-router';
import NoDataTable from '../../../Common/NoDataTable';
import Breadcrumb from 'react-bootstrap/Breadcrumb'

let api_url = process.env.REACT_APP_API_URL1;

class Invoice extends Component {

    constructor(props) {
        super(props);
        this.state = {
            invoice_id: '',
            claim: '',
            claimaint_or_respondent: '',
            amount_total: '',
            date_order: '',
            invoice_number: '',
            state: '',
            pdf_url: '',
            invoice_detail: '',
            product_list: '',
            billing_address: '',
            loading: false,
            loaded: false
        };
    }
    componentDidMount(){

        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        let invoice_id      = this.props.match.params.id;
        
        if(token !== '' && user_id !== '' && invoice_id !== ''){
            this.invoiceDetailsData(user_id, token, invoice_id)
        }
    }

    //Get Invoice Details data
    
    invoiceDetailsData = (user_id, token, invoice_id) => {
        
        let request = {
            "user_id": parseInt(user_id),
            "access_token": token,
            "invoice_id": parseInt(invoice_id)
        }

        axios.post(api_url+'/api/v1/invoice/detail', request, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data;
            if(result.status === true){
                this.setState({
                    invoice_detail: res.data,
                    claim: res.data.invoice_detail.claim,
                    claimaint_or_respondent: res.data.invoice_detail.claimaint_or_respondent,
                    amount_total: res.data.invoice_detail.amount_total,
                    date_order: res.data.invoice_detail.date_order,
                    invoice_number: res.data.invoice_detail.invoice_number,
                    state: res.data.invoice_detail.state,
                    pdf_url: res.data.invoice_detail.pdf_url,
                    product_list: res.data.invoice_detail.product_list,
                    billing_address: res.data.invoice_detail.billing_address,
                    loading: true,
                    loaded: true
                })
            }else{
                this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                    setTimeout(() => this.setState({ errorMsg: false }), 5000);
                })
            }
        })
        .catch(err => {
            console.log(err)
        })
          
    }

    render() {
        if ( !localStorage.getItem('user_id') ) {
            return (
                <Redirect to="/" />
            )
        }
        let invoice_detail               = this.state.invoice_detail;
        let claim_data                   = this.state.claim;
        let claimaint_or_respondent_data = this.state.claimaint_or_respondent;
        let amount_total_data            = this.state.amount_total;
        let date_order_data              = this.state.date_order;
        let invoice_number_data          = this.state.invoice_number;
        let state_data                   = this.state.state;
        let pdf_url_data                 = this.state.pdf_url;
        let product_lists                = this.state.product_list;
        let billing_address              = this.state.billing_address;
        
        let columns = [
            {
                name: 'Description',
                selector: row => row.description,
                sortable: true
            },
            {
                name: 'Quantity',
                selector: row => row.quantity,
                sortable: true
            },
            {
                name: 'Amount',
                selector: row => row.amount,
                sortable: true
            }            
        ];

        let list_data = [];
        if(product_lists !== undefined){
            let product_list = Object.keys(product_lists); 
            product_list.forEach(function(key, i) {

                list_data.push({ 
                    description: <Link onClick={e => e.preventDefault()}> {product_lists[key].description} </Link>, 
                    quantity: <Link onClick={e => e.preventDefault()}> {product_lists[key].quantity.toLocaleString(undefined, {minimumFractionDigits:2})} Units </Link>,
                    amount: <Link onClick={e => e.preventDefault()}> {product_lists[key].amount.toLocaleString(undefined, {minimumFractionDigits:2})} USD </Link>,
                })

            });
        }

            return(
                <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} radius={12} color="#201e53">
                    <Breadcrumb>
                        <div className="container">
                            <div className="row">
                                <Breadcrumb.Item href="/dashboard">My Claims</Breadcrumb.Item>
                                <Breadcrumb.Item href="#">Invoice Detail</Breadcrumb.Item>
                            </div>
                        </div>
                    </Breadcrumb>
                    <div className="container">
                        <div className="row">
                            <div className="col-12">
                                <div className="invoice">
                                {invoice_detail.status === true ? (
                                    <>
                                    <div className="text-right mb-4">
                                        <a href={api_url+pdf_url_data} target="blank"><button type="button" className="print-btn"><img src={Search} alt='print'/>Download Invoice</button></a>
                                    </div>
                                    <Link to={`/claim-detail/${ invoice_detail.claim_id }`}><h5>Claim#: {claim_data}</h5> </Link>
                                    <div className="row">
                                        <div className="col-md-6 col-12">
                                            <ul>
                                                <li>
                                                    <label>Claimant Name</label>
                                                    <p>{claimaint_or_respondent_data}</p>
                                                </li>
                                                <li>
                                                    <label>Date</label>
                                                    <p>{date_order_data}</p>
                                                </li>
                                                <li>
                                                    <label>Invoice#</label>
                                                    <p>{invoice_number_data}</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="col-md-6 col-12">
                                            <ul>
                                                <li className='no-margin'>
                                                    <label>Total Amount</label>
                                                    <p>{amount_total_data} USD</p>
                                                </li>
                                                <li className='no-margin'>
                                                    <label>Status</label>
                                                    <p>{state_data}</p>
                                                </li>
                                            </ul>
                                        </div>

                                        <div className="col-md-12 col-12">
                                            <ul>
                                                <li>
                                                    <h6>Billing Address: </h6>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="col-md-6 col-12">
                                            <ul>
                                                <li className='no-margin'>
                                                    <label>Contact Name</label>
                                                    <p>{billing_address[0].contact_name}</p>
                                                </li>
                                                <li className='no-margin'>
                                                    <label>Street2</label>
                                                    <p>{billing_address[0].street2}</p>
                                                </li>
                                                <li className='no-margin'>
                                                    <label>City</label>
                                                    <p>{billing_address[0].city}</p>
                                                </li>
                                                <li className='no-margin'>
                                                    <label>Country</label>
                                                    <p>{billing_address[0].country_name}</p>
                                                </li>
                                            </ul>
                                        </div>
                                        <div className="col-md-6 col-12">
                                            <ul>
                                                <li className='no-margin'>
                                                    <label>Street</label>
                                                    <p>{billing_address[0].street}</p>
                                                </li>
                                                <li className='no-margin'>
                                                    <label>Zip</label>
                                                    <p>{billing_address[0].zip}</p>
                                                </li>
                                                <li className='no-margin'>
                                                    <label>State</label>
                                                    <p>{billing_address[0].state_name}</p>
                                                </li>
                                            </ul>
                                        </div>

                                        {product_lists !== undefined && product_lists.length ? (
                                            <>
                                                {list_data.length > 0 &&
                                                    <DataTable
                                                        columns={columns}
                                                        data={list_data}
                                                        fixedHeader
                                                    />
                                                }

                                                { list_data.length == 0 &&
                                                    <div className="claim-list-table table-responsive">
                                                        <NoDataTable columns={columns} title="No Data Available" />
                                                    </div>
                                                }     
                                            </>
                                        ) : null}
                                    </div>
                                    </>
                                ) : (
                                    <div className="row">
                                        <div className="col-md-12 col-12">
                                            <p>No invoice data found.</p>
                                        </div>
                                    </div>
                                )}
                                </div>
                            </div>
                        </div>
                    </div>
                </Loader>        
            )
    };
}
export default Invoice;