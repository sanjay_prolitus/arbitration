import React, { useState } from "react";
import Tabs from 'react-bootstrap/Tabs';
import Tab from 'react-bootstrap/Tab';
import Accordion from 'react-bootstrap/Accordion';
import { Redirect } from 'react-router';
import './dashboard.scss';
import ClaimList from './ClaimList/ClaimList';
import NewClaim from  './NewClaim/NewClaim';
import RulesAndForms from  './RulesAndForms/RulesAndForms';
import Help from  './Help/Help';
import ViewInvoice from  './Invoice/ViewInvoice';
import Myaccount from  './Myaccount/Myaccount';
import axios from 'axios';

const Dashboard = () =>{

    const [activeId,setActiveId] = useState('0');
    let [responseData, setResponseData] = React.useState('');
    const [key, setKey] = useState('file_new_claims');

    const handleAccordion = (id) =>{
        if (activeId === id) {
            setActiveId(null);
        } else {
            setActiveId(id);
        }
    }

    const api_url = process.env.REACT_APP_API_URL1;

    // get asset details
    
    const getClaimList = React.useCallback(() => {
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        if(user_id){
            let claim_request    = { 
                'user_id': parseInt(user_id), 
                'access_token': token, 
                'page_no': 1, 
                'status': "",
            }
            axios.post(api_url+'/api/v1/myclaim/list', claim_request, {
                headers: {
                    'Content-Type': 'text/plain'
                }
            })
            .then((response) => {
                setResponseData(response.data)
                if(responseData.claims && responseData.claims.length){
                    setKey("my_claims")
                }
                
            })
            .catch((error) => {
                console.log(error)
            })
        }
    }, [])

    React.useEffect(() => {
        getClaimList()
    }, [getClaimList])

    let default_active_key;
    
    if(responseData.claims && responseData.claims.length){
        default_active_key = "my_claims";
    }else{
        default_active_key = "file_new_claims";
    }

    return(
        <div className="container">
            <div className="row">
                <div className="col-12 d-md-block d-none">
                    <Tabs className='claim-tabs' >
                        <Tab eventKey="my_claims" title="My Claims">
                            <ClaimList/>
                        </Tab>
                        <Tab eventKey="file_new_claims" title="File a new claims">
                            <NewClaim/>
                        </Tab>
                        <Tab eventKey="VIEW INVOICE" title="View Invoice">
                            <ViewInvoice/>
                        </Tab>
                        <Tab eventKey="MY ACCOUNT" title="My Account">
                            <Myaccount/>
                        </Tab>
                        <Tab eventKey="RULES AND FORMS" title="Rules and Forms">
                            <RulesAndForms/>
                        </Tab>
                        <Tab eventKey="HELP" title="Help">
                            <Help/>
                        </Tab>
                    </Tabs>
                </div>
                <div className="col-12 d-md-none d-block">
                    <Accordion defaultActiveKey="0" className='claim-accordion'>
                    <div className={activeId === '0' ? 'active-button' : ''}>
                        <Accordion.Toggle value="My Claims" className='accordion-button' onClick={()=>handleAccordion('0')} eventKey="0">My Claims</Accordion.Toggle>
                        <Accordion.Collapse eventKey="0">
                            <div className="tab-content">
                                <ClaimList/>
                            </div>
                        </Accordion.Collapse>
                    </div>
                    <div className={activeId === '1' ? 'active-button' : ''}>
                        <Accordion.Toggle value="File a new claims" eventKey="1" className='accordion-button' onClick={()=>handleAccordion('1')}>File a new claims</Accordion.Toggle>
                        <Accordion.Collapse eventKey="1">
                            <div className="tab-content">
                                <NewClaim/>
                            </div>
                        </Accordion.Collapse>
                    </div>
                    <div className={activeId === '2' ? 'active-button' : ''}>
                        <Accordion.Toggle value="View Invoice" eventKey="2" className='accordion-button' onClick={()=>handleAccordion('2')}>View Invoice</Accordion.Toggle>
                        <Accordion.Collapse eventKey="2">
                            <div className="tab-content">
                                <ViewInvoice/>
                            </div>
                        </Accordion.Collapse>
                    </div>
                    <div className={activeId === '3' ? 'active-button' : ''}>
                        <Accordion.Toggle value="My Account" eventKey="3" className='accordion-button' onClick={()=>handleAccordion('3')}>My Account</Accordion.Toggle>
                        <Accordion.Collapse eventKey="3">
                            <div className="tab-content">
                                <Myaccount/>
                            </div>
                        </Accordion.Collapse>
                    </div>
                    <div className={activeId === '4' ? 'active-button' : ''}>
                        <Accordion.Toggle value="Rules and Forms" eventKey="4" className='accordion-button' onClick={()=>handleAccordion('4')}>Rules and Forms</Accordion.Toggle>
                        <Accordion.Collapse eventKey="4">
                            <div className="tab-content">
                                <RulesAndForms/>
                            </div>
                        </Accordion.Collapse>
                    </div>
                    </Accordion>
                </div>
            </div>
        </div>
    )
    
}
export default Dashboard;