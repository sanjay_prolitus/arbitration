import React from 'react';
import { Link } from "react-router-dom";
import Arrow from './../../../assets/images/arrow-circle.png';

const Help = () =>{
    return(
    	<div className="new-claim">
            <Link to="/cms-page/help"><button type="button" className="new-claim-btn">View <img src={Arrow} alt='arrow'/></button></Link>
       </div>
    )
}
export default Help;