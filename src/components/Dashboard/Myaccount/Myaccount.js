import React, { Component } from 'react';
import axios from 'axios';
import './myaccount.scss';
import Loader from 'react-loader'
import Modal from 'react-bootstrap/Modal';
import Row from 'react-bootstrap/Row';
import Col from 'react-bootstrap/Col';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import { Link } from "react-router-dom";
import Mail from './../../../assets/images/mail.png';

let api_url = process.env.REACT_APP_API_URL1;

class Myaccount extends Component {

    constructor(props) {
        super(props);
        this.state = {
            email: localStorage.getItem('user_email'),
            password: '',
            otp: '',
            openResetForm: false,
            closeResetForm: false,
            formEmailErr0: false,
            formEmailErr1: false,
            formPassErr: false,
            formOtpErr: false,
            loaded: false,
            succmessage: '',
            errormessage: '',
            account_detail: '',
            editAccountForm: '',
            name_err: '',
            email_err: '',
            states: '',
            name: localStorage.getItem('user_name'),
            phone: '',
            mobile: '',
            street1: '',
            street2: '',
            zip: '',
            city: '',
            state_id: '',
            country_id: ''
        };

        this.openResetForm      = this.openResetForm.bind(this);
        this.closeResetForm     = this.closeResetForm.bind(this);
        this.openOtpForm        = this.openOtpForm.bind(this);
        this.closeOtpForm       = this.closeOtpForm.bind(this);
        this.openAccountForm    = this.openAccountForm.bind(this);
        this.closeAccountForm   = this.closeAccountForm.bind(this);
    }

    componentDidMount(){

        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');

        if(token !== '' && user_id !== ''){
            this.myAccountData(user_id, token)
        }
    }

    inputHandler = (event) =>{
        this.setState ({ [event.target.name] :event.target.value  })
    }

    openResetForm = (e) => {
        e.preventDefault();
        this.setState({
            openResetForm: true
        })
    }

    closeResetForm = (e) => {
        e.preventDefault();
        this.setState({
            openResetForm: false
        })
    }

    openOtpForm = (e) => {
        e.preventDefault();
        this.setState({
            openResetForm: true
        })
    }

    closeOtpForm = (e) => {
        e.preventDefault();
        this.setState({
            openOtpForm: false
        })
    }

    openAccountForm = (e) => {
        e.preventDefault();
        this.setState({
            editAccountForm: true
        })
    }

    closeAccountForm = (e) => {
        e.preventDefault();
        this.setState({
            editAccountForm: false
        })
    }

    myAccountData = (user_id, token) => {
        let request = { 
            "user_id": parseInt(user_id),
            "access_token": token
        }
        
        if(user_id){
            axios.post(api_url+'/api/v1/get/my_account/detail', request, {
                headers: { 
                    'Content-Type': 'text/plain'
                }
            })
            .then( response => {
                let result = response.data;

                if(result.success === true){
                    let account_detail = result.account_detail;
                    let country = result.country;
                    let country_id;
                    if(account_detail){
                       account_detail = account_detail[0]; 
                    }
                    if(country){
                       country_id = country[0].id; 
                    } 

                    this.setState({
                        account_detail: account_detail,
                        name: account_detail.name,
                        street1: account_detail.street1,
                        street2: account_detail.street2,
                        zip: account_detail.zip,
                        city: account_detail.city_id,
                        state_id: account_detail.state_id,
                        phone: account_detail.phone,
                        mobile: account_detail.mobile,
                        states: result.state,
                        country_id: country_id,
                        loaded: true
                    })

                }else{
                    this.setState({ errormessage: "Somthing went wrong." }, () => {
                        setTimeout(() => this.setState({ errormessage: false }), 3000);
                    })
                }
                
            })
            .catch(err => {
                console.log(err);
            })
        }
    }

    submitForm = (e) => {
        e.preventDefault();

        let email      = this.state.email ? this.state.email : '';

        let request = { 'email': email}
        
        if(email){
            axios.post(api_url+'/api/v1/generate/otp', request, {
                headers: { 
                    'Content-Type': 'text/plain'
                }
            })
            .then( response => {
                let result = response.data;
                
                if(result.success === true){

                    this.setState({ succmessage: result.message, openResetForm: false}, () => {
                        setTimeout(() => this.setState({ succmessage: false  }), 5000);
                    })

                    this.setState({ openOtpForm: true }, () => {
                        setTimeout(() => this.setState({ editAccountForm: false }), 500);
                    })

                    //localStorage.removeItem('user_name')
                    //localStorage.setItem('user_name', name)
                    this.componentDidMount();

                }else{
                    this.setState({ errormessage: "Somthing went wrong." }, () => {
                        setTimeout(() => this.setState({ errormessage: false }), 3000);
                    })
                }
                
            })
            .catch(err => {
                console.log(err);
            })
        }else{
            
            this.setState({ formEmailErr0: "This field is required." }, () => {
                setTimeout(() => this.setState({ formEmailErr0: false }), 3000);
            })
            
        }
    }

    verifyForm = (e) => {
        e.preventDefault();

        let email       = this.state.email ? this.state.email : ''; 
        let password    = this.state.password ? this.state.password : '';
        let otp         = this.state.otp ? this.state.otp : '';

        let request = { 
            'email': email, 
            'confirm_password': password, 
            'otp': otp 
        }
        
        if(email && password && otp ){
            axios.post(api_url+'/api/v1/set_password', request, {
                headers: { 
                    'Content-Type': 'text/plain'
                }
            })
            .then( response => {
                let result = response.data;
                
                if(result.success === true){
                    var access_token    = result.access_token;
                    var user_id         = result.user_id;
                    var user_name       = result.user_name;
                    var user_email      = result.user_email;

                    /*localStorage.setItem('token', access_token);
                    localStorage.setItem('user_id', user_id);
                    localStorage.setItem('user_name', user_name);
                    localStorage.setItem('user_email', user_email);*/

                    this.setState({ 
                        succmessage: "Password updated successfully.", 
                        openOtpForm: false,
                        otp: '', 
                        password: '' 
                    }, () => {
                        setTimeout(() => this.setState({ succmessage: false }), 3000);
                    })
                    //window.location.reload(true);
                }else{
                    this.setState({ errormessage: result.message }, () => {
                        setTimeout(() => this.setState({ errormessage: false }), 3000);
                    })
                }
                
            })
            .catch(err => {
                console.log(err);
            })
        }else{
            if(email === ''){
                this.setState({ formEmailErr1: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formEmailErr1: false }), 3000);
                })
            }
            if(password === ''){
                this.setState({ formPassErr: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formPassErr: false }), 3000);
                })
            }
            if(otp === ''){
                this.setState({ formOtpErr: "This field is required." }, () => {
                    setTimeout(() => this.setState({ formOtpErr: false }), 3000);
                })
            }
        }
    }

    submitAccountForm = (e) => {
        e.preventDefault();
        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');

        let name            = this.state.name ? this.state.name : '';
        let street1          = this.state.street1 ? this.state.street1 : '';
        let street2         = this.state.street2 ? this.state.street2 : '';
        let zip             = this.state.zip ? this.state.zip : '';
        let city            = this.state.city ? this.state.city : '';
        let state_id        = this.state.state_id ? this.state.state_id : '';
        let phone           = this.state.phone ? this.state.phone : '';
        let email           = this.state.email ? this.state.email : '';
        let mobile          = this.state.mobile ? this.state.mobile : '';
        let country_id      = this.state.country_id ? this.state.country_id : '';


        let request = { 
            "user_id": parseInt(user_id),
            "access_token": token,
            "name": name,
            "street": street1,
            "street2": street2,
            "zip": zip,
            "city": city,
            "state_id": parseInt(state_id),
            "country_id": country_id,
            "phone": phone,
            "mobile": mobile
        }

        if(name){
            
            axios.post(api_url+'/api/v1/update/my_account', request, {
                headers: { 
                    'Content-Type': 'text/plain'
                }
            })
            .then( response => {
                let result = response.data;
                
                if(result.success === true){

                    this.setState({ succmessage: result.message }, () => {
                        setTimeout(() => this.setState({ succmessage: false  }), 5000);
                    })

                    this.setState({ loaded: true }, () => {
                        setTimeout(() => this.setState({ editAccountForm: false }), 500);
                    })

                    localStorage.removeItem('user_name')
                    localStorage.setItem('user_name', name)
                    this.componentDidMount();

                }else{
                    this.setState({ errormessage: "Somthing went wrong." }, () => {
                        setTimeout(() => this.setState({ errormessage: false }), 3000);
                    })
                }
                
            })
            .catch(err => {
                console.log(err);
            })
        }else{
            if(name){
                this.setState({ name_err: "This field is required." }, () => {
                    setTimeout(() => this.setState({ name_err: false }), 3000);
                })
            }
            
        }
    }

    render() {
        let openResetForm    = this.state.openResetForm;
        let closeResetForm   = this.state.closeResetForm;
        let account_detail   = this.state.account_detail;
        let showModel;
        if(closeResetForm){
            showModel       = false;
        }else{
            showModel   = openResetForm;
        }

        return(
            <Loader loaded={this.state.loaded} className="loader" lines={13} length={12} width={6} top="100%" radius={12} color="#201e53">
                <div className="container">
                    <div className="row">
                        <div className="col-12">
                            <div className="invoice claim-list">                                
                                {this.state.succmessage &&
                                    <div className="success-message" style={{ color:"green"}}>{this.state.succmessage}</div>
                                }
                                <div className="row">
                                    <div className="col-md-6 col-12">
                                        <ul>
                                            <li>
                                                <label>Name</label>
                                                <p>{account_detail.name}</p>
                                            </li>
                                            <li>
                                                <label>Phone</label>
                                                <p>{account_detail.phone}</p>
                                            </li>
                                            <li>
                                                <label>Street1</label>
                                                <p>{account_detail.street1}</p>
                                            </li>
                                            <li>
                                                <label>Zip</label>
                                                <p>{account_detail.zip}</p>
                                            </li>
                                            <li>
                                                <label>State</label>
                                                <p>{account_detail.state_name}</p>
                                            </li>
                                            <li>
                                                <label>Country</label>
                                                <p>{account_detail.country_name}</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-md-6 col-12">
                                        <ul>
                                            <li>
                                                <label>Email</label>
                                                <p>{account_detail.email}</p>
                                            </li>
                                            <li>
                                                <label>Mobile</label>
                                                <p>{account_detail.mobile}</p>
                                            </li>
                                            <li>
                                                <label>Street2</label>
                                                <p>{account_detail.street2}</p>
                                            </li>
                                            <li>
                                                <label>City</label>
                                                <p>{account_detail.city_id}</p>
                                            </li>
                                        </ul>
                                    </div>
                                    <div className="col-md-12 col-12">
                                        <ul>
                                            <li>
                                                <Link className="add-primary" onClick={this.openResetForm}>Reset Password </Link>
                                                <Link className="add-primary" onClick={this.openAccountForm}>Update Profile </Link>
                                            </li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <Modal className="login--modal" show={showModel}>
                    <Modal.Body>
                        <form onSubmit={this.submitForm}>
                            <Button className="close" onClick={this.closeResetForm}>&times;</Button>
                            <h3 className="text-center">Reset Password</h3>
                            {this.state.succmessage &&
                                <div className="success-message" style={{ color:"green"}}>{this.state.succmessage}</div>
                            }
                            {this.state.errormessage &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.errormessage}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="email"><img src={Mail} alt='email'/></InputGroup.Text>
                                <FormControl
                                    placeholder="E-mail"
                                    aria-label="Enter email"
                                    aria-describedby="email"
                                    name="email"
                                    readonly="readonly"
                                    value={this.state.email}
                                    onChange={this.inputHandler}
                                />
                            </InputGroup>
                            {this.state.formEmailErr0 &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.formEmailErr0}</div>
                            }
                            <div className="text-center otp-button-group">
                                <button type="submit" className="btn-signin">Get OTP</button>
                            </div>
                        </form>
                    </Modal.Body>
                </Modal>

                <Modal className="login--modal" show={this.state.openOtpForm}>
                    <Modal.Body>
                        <form onSubmit={this.verifyForm}>
                            <Button className="close" onClick={this.closeOtpForm}>&times;</Button>
                            <h3 className="text-center">Verify OTP</h3>
                            {this.state.errormessage &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.errormessage}</div>
                            }
                            {this.state.succmessage &&
                                <div className="success-message" style={{ color:"green"}}>{this.state.succmessage}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="email1"><img src={Mail} alt='email'/></InputGroup.Text>
                                <FormControl
                                    placeholder="E-mail"
                                    aria-label="Enter email"
                                    aria-describedby="email"
                                    name="email"
                                    readonly="readonly"
                                    value={this.state.email}
                                    onChange={this.inputHandler}
                                />
                            </InputGroup>
                            {this.state.formEmailErr1 &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.formEmailErr1}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="password"><img src={Mail} alt='password'/></InputGroup.Text>
                                <FormControl
                                    placeholder="Enter Password"
                                    aria-label="Enter Password"
                                    aria-describedby="password"
                                    name="password"
                                    type="password"
                                    value={this.state.password}
                                    onChange={this.inputHandler}
                                />
                            </InputGroup>
                            {this.state.formPassErr &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.formPassErr}</div>
                            }
                            <InputGroup className="mb-4">
                                <InputGroup.Text id="otp"><img src={Mail} alt='otp'/></InputGroup.Text>
                                <FormControl
                                    placeholder="Enter OTP"
                                    aria-label="Enter Otp"
                                    aria-describedby="otp"
                                    name="otp"
                                    value={this.state.otp}
                                    onChange={this.inputHandler}
                                />
                            </InputGroup>
                            {this.state.formOtpErr &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.formOtpErr}</div>
                            }
                            <div className="text-center otp-button-group">
                                <button type="submit" className="btn-signin">Submit</button>
                            </div>
                        </form>
                    </Modal.Body>
                </Modal>

                <Modal className="login--modal" show={this.state.editAccountForm} size="lg">
                    <Modal.Body>
                        <form onSubmit={this.submitAccountForm}>
                            <Button className="close" onClick={this.closeAccountForm}>&times;</Button>
                            <h3 className="text-center">Update Profile</h3>
                            {this.state.errormessage &&
                                <div className="form-validate-error" style={{ color:"red"}}>{this.state.errormessage}</div>
                            }
                            <Row>
                                <Col xs={12} md={6}>
                                    <InputGroup className="mb-4">
                                        <FormControl
                                            placeholder="Name"
                                            aria-label="Enter name"
                                            aria-describedby="name"
                                            name="name"
                                            value={this.state.name}
                                            onChange={this.inputHandler}
                                        />
                                        {this.state.name_err &&
                                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.name_err}</div>
                                        }
                                    </InputGroup>
                                </Col>
                                <Col xs={12} md={6}>
                                    <InputGroup className="mb-4">
                                        <FormControl
                                            placeholder="Email"
                                            aria-label="Enter email"
                                            aria-describedby="email"
                                            name="email"
                                            type="email"
                                            readOnly="readonly"
                                            value={this.state.email}
                                            onChange={this.inputHandler}
                                        />
                                        {this.state.email_err &&
                                            <div className="form-validate-error" style={{ color:"red"}}>{this.state.email_err}</div>
                                        }
                                    </InputGroup>
                                </Col>
                            </Row>
                            <Row>
                                <Col xs={12} md={6}>
                                    <InputGroup className="mb-4">
                                        <FormControl
                                            placeholder="Phone"
                                            aria-label="Enter phone"
                                            aria-describedby="phone"
                                            name="phone"
                                            value={this.state.phone}
                                            onChange={this.inputHandler}
                                        />
                                    </InputGroup>
                                </Col>
                                <Col xs={12} md={6}>    
                                    <InputGroup className="mb-4">
                                        <FormControl
                                            placeholder="Mobile"
                                            aria-label="Enter mobile"
                                            aria-describedby="mobile"
                                            name="mobile"
                                            value={this.state.mobile}
                                            onChange={this.inputHandler}
                                        />
                                    </InputGroup>
                                </Col>  
                            </Row>     
                            <Row>
                                <Col xs={12} md={6}>
                                    <InputGroup className="mb-4">
                                        <FormControl
                                            placeholder="Enter Street1"
                                            aria-label="Enter Street1"
                                            aria-describedby="Street1"
                                            name="street1"
                                            type="text"
                                            value={this.state.street1}
                                            onChange={this.inputHandler}
                                        />
                                    </InputGroup>
                                </Col>
                                <Col xs={12} md={6}>    
                                    <InputGroup className="mb-4">
                                        <FormControl
                                            placeholder="Street2"
                                            aria-label="Enter Street2"
                                            aria-describedby="street2"
                                            name="street2"
                                            value={this.state.street2}
                                            onChange={this.inputHandler}
                                        />
                                    </InputGroup>
                                </Col>
                            </Row>    
                            <Row>
                                <Col xs={12} md={6}>
                                    <InputGroup className="mb-4">
                                        <FormControl
                                            placeholder="Zip"
                                            aria-label="Enter zip"
                                            aria-describedby="zip"
                                            name="zip"
                                            value={this.state.zip}
                                            onChange={this.inputHandler}
                                        />
                                    </InputGroup>
                                </Col>
                                <Col xs={12} md={6}>
                                    <InputGroup className="mb-4">
                                        <FormControl
                                            placeholder="Enter city"
                                            aria-label="Enter city"
                                            aria-describedby="city"
                                            name="city"
                                            type="text"
                                            value={this.state.city}
                                            onChange={this.inputHandler}
                                        />
                                    </InputGroup>
                                </Col> 
                            </Row>       
                            <Row>
                                <Col xs={12} md={6}>   
                                    <InputGroup className="mb-4">
                                        <select className="form-control" name="state_id" id="state_id" value={this.state.state_id} onChange={this.inputHandler}>
                                            <option value="">Please select state</option>
                                            {this.state.states ? this.state.states.map((state, index) =>
                                                <>
                                                <option value={state.id} key={index}>{state.name}</option>
                                                </>
                                            ) : null}                                  
                                        </select>
                                    </InputGroup>
                                    
                                </Col>
                            </Row>    
                            <div className="text-center otp-button-group">
                                <button type="submit" className="btn-signin">Submit</button>
                            </div>
                        </form>
                    </Modal.Body>
                </Modal>
            </Loader>
        )
    };
}
export default Myaccount;