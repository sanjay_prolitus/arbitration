import React from 'react';
import { Link } from "react-router-dom";
import Arrow from './../../../assets/images/arrow-circle.png';

const RulesAndForms = () =>{
    return(
    	<div className="new-claim">
            <Link to="/cms-page/pma"><button type="button" className="new-claim-btn">View <img src={Arrow} alt='arrow'/></button></Link>
       </div>
    )
}
export default RulesAndForms;