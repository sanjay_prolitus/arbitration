import React, {Component} from 'react';
import DocumentTable from './DocumentTable';
import Attorney from './Attorney/Attorney';
import AttorneyTable from './Attorney/AttorneyTable';
import Filter from './Filter/Filter';
import Invoice from './Invoice/Invoice';
import './file-details.scss';
import axios from 'axios';

let api_url = process.env.REACT_APP_API_URL1;

class FileDetails extends Component {

    constructor(props) {
        super(props);
        this.state = {
          claim_id: '',
          tables_data: '',
          loading: false
      };

    }

    componentDidMount(){

        let user_id         = localStorage.getItem('user_id');
        let token           = localStorage.getItem('token');
        let claim_id        = this.props.match.params.id;

        if(token !== '' && user_id !== '' && claim_id !== ''){
            this.claimDetailsData(user_id, token, claim_id)
        }
    }

    // Get claim deatil data
    claimDetailsData = (user_id, token, claim_id) => {
        
        let request = {
            "user_id": parseInt(user_id),
            "access_token": token,
            "claim_id": parseInt(claim_id)
        }

        axios.post(api_url+'/api/v1/myclaim/detail', request, {
            headers: {
                'Content-Type': 'text/plain'
            }
        })
        .then( res => {
            let result = res.data
            console.log(result)
            if(result.success === true){
                this.setState({
                    tables_data: res.data,
                    loading: true
                })

            }else{
                this.setState({ errorMsg: "Somthing went wrong!!" }, () => {
                    setTimeout(() => this.setState({ errorMsg: false }), 5000);
                })
            }
        })
        .catch(err => {
            console.log(err)
        })
          
    }

    render() {

        let get_all_data        = this.state.tables_data;
        let get_invoice_data    = get_all_data.claim_detail;
        let get_respondent_data = get_all_data.respondent;
        let get_document_data   = get_all_data.documents;
        let claim_id;
        if(get_invoice_data){
            claim_id = get_invoice_data.id;
        }

        return(
           <div className="file-details">
                <Invoice invoice_data={get_invoice_data} />
                <AttorneyTable respondent_data={get_respondent_data} loading={this.state.loading}/>
                <Filter />
                <DocumentTable document_data={get_document_data} claim_id={claim_id} loading={this.state.loading}/>
           </div>
        )
    }
}
export default FileDetails;