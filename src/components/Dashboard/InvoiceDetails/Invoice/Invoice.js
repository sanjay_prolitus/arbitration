import React, { Component } from 'react';
import Search from './../../../../assets/images/search-plus.png';
import File from './../../../../assets/images/file.png';
import TimeSheet from './../../../../assets/images/timesheet.png';
import Expense from './../../../../assets/images/expense.png';
import './invoice.scss';

class Invoice extends Component {

    constructor(props) {
        super(props);
        this.state = {};
    }

    render() {

        let invoice_data = this.props.invoice_data ? this.props.invoice_data : '';

        return(
            <div className="invoice">
                <div className="text-right mb-4">
                    <button type="button" className="print-btn"><img src={Search} alt='print'/>View Invoice</button>
                    <button type="button" className="file-btn"><img src={TimeSheet} alt='File'/>Timesheet</button>
                    <button type="button" className="file-btn"><img src={Expense} alt='File'/>Expenses</button>
                    <button type="button" className="file-btn"><img src={File} alt='File'/>File Document To This Case</button>
                </div>
                <div className="row">
                    <div className="col-md-6 col-12">
                        <ul>
                            <li>
                                <label>Claim#</label>
                                <p>{invoice_data.name}</p>
                            </li>
                            <li>
                                <label>Claimant Name</label>
                                <p>{invoice_data.claimant_name}</p>
                            </li>
                            <li>
                                <label>Date</label>
                                <p>{invoice_data.date}</p>
                            </li>
                            <li>
                                <label>Claimant E-mail</label>
                                <p>{invoice_data.claimant_email}</p>
                            </li>
                        </ul>
                    </div>
                    <div className="col-md-6 col-12">
                        <ul>
                            <li className='no-margin'>
                                <label>Claimant Contact</label>
                                <p>{invoice_data.claimant_mobile}</p>
                            </li>
                            <li className='no-margin'>
                                <label>Amount of Claim</label>
                                <p>{invoice_data.claim_amount} USD</p>
                            </li>
                            <li className='no-margin'>
                                <label>Status</label>
                                <p>{invoice_data.status}</p>
                            </li>
                            <li className='no-margin'>
                                <label>Invoice</label>
                                <p>{invoice_data.invoice}</p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        )
    }
}
export default Invoice;