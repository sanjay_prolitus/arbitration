import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import InputGroup from 'react-bootstrap/InputGroup';
import FormControl from 'react-bootstrap/FormControl';
import Lock from './../../assets/images/lock.png';
import OTP from './../../assets/images/otp.png';
import './reset.scss';

const Reset = (props) =>{  
    const [openReset,setOpenReset] = useState(false);
    const handleCloseReset = () =>{
        setOpenReset(false);
    }
    return(        
        <Modal className="reset--modal" show={openReset} onHide={openReset}>
            <Modal.Body>
                <Button className="close" onClick={handleCloseReset}>&times;</Button>
                <h3 className="text-center">Reset Password</h3>
                <h6 className="text-center">John123@gmail.com</h6>
                <InputGroup className="mb-4">
                    <InputGroup.Text id="password"><img src={Lock} alt='password'/></InputGroup.Text>
                    <FormControl
                        placeholder="Password"
                        aria-label="Enter Password"
                        aria-describedby="password"
                    />
                </InputGroup>
                <InputGroup className="mb-4">
                    <InputGroup.Text id="cPassword"><img src={Lock} alt='password'/></InputGroup.Text>
                    <FormControl
                        placeholder="Confirm Password"
                        aria-label="Enter Confirm password"
                        aria-describedby="cPassword"
                    />
                </InputGroup>
                <InputGroup className="mb-4">
                    <InputGroup.Text id="otp"><img src={OTP} alt='password'/></InputGroup.Text>
                    <FormControl
                        placeholder="Enter OTP"
                        aria-label="Enter OTP"
                        aria-describedby="otp"
                    />
                </InputGroup>
                <div className="text-center">
                    <button type="submit" className="btn-signin">Get New Password</button>
                </div>
            </Modal.Body>
        </Modal>
    )
}

export default Reset;