import React, {useState, Component} from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import Check from './../../assets/images/check.png';
import './alert.scss';

class RespondentAlert extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openAlert: false
        };

        this.handleCloseAlert     = this.handleCloseAlert.bind(this);
    }

    handleCloseAlert = (e) => {
        e.preventDefault();
        this.setState({
            openAlert: false
        })
    }

    render() {
        let registerSucc = this.props.registerSucc;
        let openAlert;
        if(registerSucc){
            openAlert = true
        }else{
            openAlert = this.state.openAlert
        }
        return(        
            <Modal className="alert--modal respondent-alert" show={openAlert} onHide={openAlert }>
                <Alert variant="success">
                    <Button className="close" onClick={this.handleCloseAlert}>&times;</Button>
                    <div className="d-flex align-items-start">
                        <img className="mr-3" src={Check} alt='alert'/>
                        <div>
                            <h5 className="mb-2">Successfully removed</h5>
                        </div>
                    </div>
                </Alert>    
            </Modal>
        )
    }
}

export default RespondentAlert;