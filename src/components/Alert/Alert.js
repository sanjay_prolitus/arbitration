import React,{useState} from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import Check from './../../assets/images/check.png';
import './alert.scss';

const SuccessAlert = () =>{  
    const [openAlert,setOpenAlert] = useState(false);
    const handleCloseAlert = () =>{
        setOpenAlert(false);
    }
    return(        
        <Modal className="alert--modal" show={openAlert } onHide={openAlert }>
            <Alert variant="success">
                <Button className="close" onClick={handleCloseAlert}>&times;</Button>
                <p className="text-center d-none"><img src={Check} alt='alert'/>Your Payment Of 500.00 USD We Done Successfully. Your Claim Number <strong>#20202001</strong></p>
                <div className="d-flex align-items-start">
                    <img className="mr-3" src={Check} alt='alert'/>
                    <div>
                        <h5 className="mb-2">Register Successfully</h5>
                        <p>Please check your inbox to verify Your Account</p>
                    </div>
                </div>
            </Alert>    
        </Modal>
    )
}

export default SuccessAlert;