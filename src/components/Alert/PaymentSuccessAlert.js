import React, {useState, Component} from 'react';
import Modal from 'react-bootstrap/Modal';
import Button from 'react-bootstrap/Button';
import Alert from 'react-bootstrap/Alert';
import Check from './../../assets/images/check.png';
import './alert.scss';

class PaymentSuccessAlert extends Component {
    constructor(props) {
        super(props);
        this.state = {
            openAlert: false
        };

        this.handleCloseAlert     = this.handleCloseAlert.bind(this);
    }

    handleCloseAlert = (e) => {
        e.preventDefault();
        this.setState({
            openAlert: false
        })

        window.location.href="/dashboard";
    }

    render() {
        let paymentsuccess = this.props.paymentsuccess;
        let openAlert;
        if(paymentsuccess){
            openAlert = true
        }else{
            openAlert = this.state.openAlert
        }
        return(        
            <Modal className="alert--modal" show={openAlert} onHide={openAlert }>
                <Alert variant="success">
                    <Button className="close" onClick={this.handleCloseAlert}>&times;</Button>
                    <div className="d-flex align-items-start">
                        <img className="mr-3" src={Check} alt='alert'/>
                        <div>
                            <p>{paymentsuccess}</p>
                        </div>
                    </div>
                </Alert>    
            </Modal>
        )
    }
}

export default PaymentSuccessAlert;