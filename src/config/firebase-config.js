import firebase from "firebase";

var firebaseConfig = {
    apiKey: "AIzaSyAXj5BCz1ibzRpR8lT56SIB3mPbHX-d27U",
    authDomain: "axia-6278c.firebaseapp.com",
    projectId: "axia-6278c",
    storageBucket: "axia-6278c.appspot.com",
    messagingSenderId: "54166462133",
    appId: "1:54166462133:web:c6914586cc48cbff2e28fc",
    measurementId: "G-KQXCHXR34N"
  };

// Initialize Firebase
firebase.initializeApp(firebaseConfig);
firebase.analytics();

export default firebase; 